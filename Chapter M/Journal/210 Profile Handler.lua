-- |[ ===================================== Profile Handler ==================================== ]|
--Whenever the journal is opened to profiles mode, generates profiles.

-- |[Listing]|
--Setup.
gzaChMProfileEntries = {}

--Shorthand.
local zPE = gzaChMProfileEntries

--Debug flag.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")

-- |[ ======================================== Functions ======================================= ]|
local function fnGetTopicState(psTopicName, psCharacterName)
    
    --Arg check.
    if(psTopicName     == nil) then return 0 end
    if(psCharacterName == nil) then return 0 end
    
    --Iterate across all topics.
    local iTopics = WD_GetProperty("Total Topics")
    for i = 0, iTopics-1, 1 do
        
        --Check if the topic matches:
        if(WD_GetProperty("Topic Name", i) == psTopicName) then
            
            --Iterate across all NPCs in this topic:
            local iNPCs = WD_GetProperty("Topic NPCs Total", i)
            for p = 0, iNPCs-1, 1 do
                
                --Check if this NPC name matches:
                if(WD_GetProperty("Topic NPC Name", i, p) == psCharacterName) then
                    
                    --Return the topic level.
                    return WD_GetProperty("Topic NPC Level", i, p)
                end
            end
        end
    end

    --Not found.
    return 0
end

-- |[ ========================== Common Variables ========================== ]|

-- |[ =========================== Nina =========================== ]|
--Nina. The profile only shifts by form a bit.
local sNinaForm = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")

--Basic profile. A lot of forms and cases have these in common.
local sBaseProfile = "A hunter from Tserro, on the eastern\\ncoast of Arulenta. Like all elves, she\\nlooks like she's in her early 20's but is\\nactually 45. She has been hunting for a\\nlong time but seems to have been \\nhaving rotten luck."

--Normal:
if(sNinaForm == "Human") then
    fnCreateProfileStorage(zPE,  "Nina", "Nina", "Root/Images/Portraits/NinaDialogue/Human|Neutral", 0, 0)
    fnSetProfileStorageDescription(zPE, "Nina", sBaseProfile)

--Zombie:
elseif(sNinaForm == "Zombie") then
        fnCreateProfileStorage(zPE,  "Nina", "Nina", "Root/Images/Portraits/NinaDialogue/Zombie|Smirk", 0, 0)
        fnSetProfileStorageDescription(zPE, "Nina", sBaseProfile .. 
        "\\n\\nShe died in battle against some\\nskeletons, but was returned to life in a\\nmoldy tomb as a banshee. She feels no\\nallegiance to the skeletons, however.")
    
--Mannequin.
elseif(sNinaForm == "Mannequin") then
    fnCreateProfileStorage(zPE,  "Nina", "Nina", "Root/Images/Portraits/NinaDialogue/Mannequin|Neutral", 0, 0)
    fnSetProfileStorageDescription(zPE, "Nina", sBaseProfile .. 
        "\\n\\nTransformed into a mannequin by a\\ntrap, she now serves the master of the\\nestate while repeating a manta in her\\nhead to ensure total loyalty.")
end
    
-- |[ ====================================== Assemble List ===================================== ]|
--Upload the information from the list.
for i = 1, #gzaChMProfileEntries, 1 do
    fnCreateProfileFromList(gzaChMProfileEntries, gzaChMProfileEntries[i].sInternalName)
end
