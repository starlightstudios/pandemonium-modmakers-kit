-- |[ ==================================== Bestiary Handler ==================================== ]|
--Whenever the journal is opened to bestiary mode, run this script to populate bestiary entries.

-- |[Stat Table Check]|
if(gczaNinaAdventureStats == nil) then return end

-- |[Listing]|
--If the listing doesn't already exist, create it.
gzaChMBestiaryEntries = nil
if(gzaChMBestiaryEntries == nil) then
    
    --Setup.
    gzaChMBestiaryEntries = {}
    local zL = gczaNinaAdventureStats
    local zB = gzaChMBestiaryEntries
    local fnCBSC = fnCreateBestiaryStorageChart
    local fnSBSD = fnSetBestiaryStorageDescription
    
    --List.
    fnCBSC(zL, zB, "Brigand Warrior", "Brigand Warrior", false, "Brigand Warrior", 685, 148, false)
    fnSBSD(    zB, "Brigand Warrior", "Some lowlife thug who happens to enjoy breaking into other people's\\nhouses and taking their things. Tough, but not smart.\\nWeak to Terrify attacks.")

    fnCBSC(zL, zB, "Brigand Thief", "Brigand Thief", false, "Brigand Thief", 688, 131, false)
    fnSBSD(    zB, "Brigand Thief", "A thief without the good sense to at least be fairly tough in battle.\\nWeak to Terrify attacks.")

    fnCBSC(zL, zB, "Skeleton Archer", "Skeleton Archer", false, "Skeleton Archer", 747, 160, false)
    fnSBSD(    zB, "Skeleton Archer", "An archer, but a skeleton. Right there in the name.\\nVery resistant to pierce and bleed attacks.")
    
    fnCBSC(zL, zB, "Skeleton Warrior", "Skeleton Warrior", false, "Skeleton Warrior", 747, 160, false)
    fnSBSD(    zB, "Skeleton Warrior", "If you saw this enemy and assumed they were tough, good at close\\ncombat, and a skeleton, you assumed correctly!")
    
    fnCBSC(zL, zB, "Sproutling", "Sproutling", false, "Sproutling", 821, 226, false)
    fnSBSD(    zB, "Sproutling", "A tiny baby plant! These cute motile plants mostly stumble around\\nas they scrounge for insects to eat. The alraunes protect them and\\nhelp them grow into the " ..
                                 "very helpful pest-controlling Sprout Peckers.\\n\\n\\n\\n")

    fnCBSC(zL, zB, "Sproutpecker", "Sproutpecker", false, "Sproutpecker", 637, 153, false)
    fnSBSD(    zB, "Sproutpecker", "In the same genus as the venus fly trap, the Sprout Pecker, for lack\\nof a better term, is a motile plant that consumes insects near marshy\\nlocations and consumes " .. 
                                    "parasites that burrow into tree bark.\\nThey are often seen with alraunes, who shepherd and protect them.\\nThis is the adult form of a Sproutling.\\n\\n")

    fnCBSC(zL, zB, "Jelli", "Jelli", false, "Jelli", 675, 270, false)
    fnSBSD(    zB, "Jelli", "A slimegirl who is not currently taking human form. Has considerably\\nless health as a result, but retains the vulnerability to cold." ..
                            "\\n\\n\\n\\n\\n")

end

-- |[Assemble List]|
for i = 1, #gzaChMBestiaryEntries, 1 do
    fnCreateBestiaryEntryList(gczaNinaAdventureStats, gzaChMBestiaryEntries[i])
end
