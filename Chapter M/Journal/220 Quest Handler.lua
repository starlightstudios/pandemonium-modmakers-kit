-- |[ ====================================== Quest Handler ===================================== ]|
--Whenever the journal is opened to quest mode, generates quest entries.

-- |[Listing]|
--The list is always generated dynamically.

-- |[ ======================================= Main Quest ======================================= ]|
--Find a way home. Always generated.
if(true) then
    
    -- |[ ============================== Setup ============================= ]|
    --Setup.
    local sInternalName = "Finish the modkit!"
    local sObjective = ""
    local sDescription = ""
    AM_SetPropertyJournal("Add Quest Entry", sInternalName, sInternalName)
    
    -- |[ =========================== Description ========================== ]|
    --Objective.
    sObjective = "Beat the game!"
    
    --Description.
    sDescription = "This quest is so very short that it'd be almost insulting to remind you\\nof the objectives. That, and I don't want to. -Salty"
    
    -- |[ ============================= Upload ============================= ]|
    --Upload objective.
    AM_SetPropertyJournal("Set Quest Objective", sInternalName, sObjective)
    
    --Parse and upload description.
    local saDescription = fnParseDescriptionToArray(sDescription)
    fnUploadDescriptionArray(sInternalName, saDescription)
    
end