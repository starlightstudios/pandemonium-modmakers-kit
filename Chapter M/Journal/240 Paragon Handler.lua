-- |[ ===================================== Paragon Handler ==================================== ]|
--Whenever the journal is opened to paragons mode, generates entries and toggles.

--Note: Paragons are not tested for this mod. RELY ON NOTHING.

-- |[ ===================================== Database Entry ===================================== ]|
--Create a list of all paragons in this chapter and put them in a database.
local zaParagonDatabase = {}

-- |[Function]|
local function fnAddToDatabase(psInternalName, psDisplayName, psGroupingName)
    
    --Arg check.
    if(psInternalName == nil) then return end
    if(psDisplayName  == nil) then return end
    if(psGroupingName == nil) then return end
    
    --Create.
    local i = #zaParagonDatabase + 1
    zaParagonDatabase[i] = {}
    
    --Set.
    zaParagonDatabase[i].sInternalName = psInternalName
    zaParagonDatabase[i].sDisplayName  = psDisplayName
    
    --Default values
    zaParagonDatabase[i].bIsEnabled = false
    zaParagonDatabase[i].iKOCurrent = 0
    zaParagonDatabase[i].iKORequired = gciParagon_KO_Needed_To_Spawn
    zaParagonDatabase[i].bDefeatedActivator = false
    zaParagonDatabase[i].sEnabledPath = "Null"
    
    --Get variables.
    local iIsEnabled          = VM_GetVar("Root/Variables/Paragons/"..psGroupingName.."/iIsEnabled", "N")
    local iKillCount          = VM_GetVar("Root/Variables/Paragons/"..psGroupingName.."/iKillCount", "N")
    local iKillsNeeded        = VM_GetVar("Root/Variables/Paragons/"..psGroupingName.."/iKillsNeeded", "N")
    local iHasDefeatedParagon = VM_GetVar("Root/Variables/Paragons/"..psGroupingName.."/iHasDefeatedParagon", "N")
    
    --Set.
    if(iIsEnabled == 1.0) then zaParagonDatabase[i].bIsEnabled = true end
    zaParagonDatabase[i].iKOCurrent = iKillCount
    zaParagonDatabase[i].iKORequired = iKillsNeeded
    if(iHasDefeatedParagon == 1.0) then zaParagonDatabase[i].bDefeatedActivator = true end
    zaParagonDatabase[i].sEnabledPath = "Root/Variables/Paragons/"..psGroupingName.."/iIsEnabled"
    
end

-- |[List Creation]|
--              |[Internal Name]|   |[Display Name]|               |[SLF Paragon Grouping Name]|
fnAddToDatabase("The Estate",     "The Estate",                "The Estate")  --Nope

-- |[ ========================================= Upload ========================================= ]|
--Upload all the data to the journal.

-- |[Uploader Function]|
local function fnCreateParagonEntry(pzEntry)
    
    --Arg check.
    if(pzEntry == nil) then return end
    
    --If the KO count was zero, add a blank entry.
    if(pzEntry.iKOCurrent < 1) then
        AM_SetPropertyJournal("Add Paragon Entry", pzEntry.sInternalName, "-----")
    else
        AM_SetPropertyJournal("Add Paragon Entry", pzEntry.sInternalName, pzEntry.sDisplayName)
    end
    
    --Data.
    AM_SetPropertyJournal("Set Paragon Enabled", pzEntry.sInternalName, pzEntry.bIsEnabled)
    AM_SetPropertyJournal("Set Paragon KO Counts", pzEntry.sInternalName, pzEntry.iKOCurrent, pzEntry.iKORequired)
    AM_SetPropertyJournal("Set Paragon Has Defeated Activator", pzEntry.sInternalName, pzEntry.bDefeatedActivator)
    AM_SetPropertyJournal("Set Paragon Enabled Path", pzEntry.sInternalName, pzEntry.sEnabledPath)
    
end

-- |[Upload Execution]|
for i = 1, #zaParagonDatabase, 1 do
    fnCreateParagonEntry(zaParagonDatabase[i])
end
