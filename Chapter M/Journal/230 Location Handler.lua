-- |[ ==================================== Location Handler ==================================== ]|
--Whenever the journal is opened to locations mode, generates location entries.

--Debug flag.
local bShowAllEntries = AM_GetPropertyJournal("Show All Entries")
bShowAllEntries = true

-- |[Listing]|
--Skip generating the list if it already exists.
if(gzaChMLocationList == nil) then

    --Setup.
    gzaChMLocationList = {}

    --Dimensional Trap.
    fnCreateLocationPack(gzaChMLocationList, "The Estate Grounds", "The Estate Grounds", "Null", 0, 0,
                        "A rotten, dusty old estate lost to time. Sorry we don't have region\\nmarker art of it but that costs money.\\n\\n"..
                        "Look in the basement for some boners!")
end

-- |[Creation]|
--Iterate and create all entries.
for i = 1, #gzaChMLocationList, 1 do
    
    --Check if the player has visited this region.
    local iHasSeenThisRegion = VM_GetVar("Root/Variables/Regions/" .. gzaChMLocationList[i].sInternalName .. "/iHasSeenThisRegion", "N")
    
    --Has visited:
    if(iHasSeenThisRegion == 1.0 or bShowAllEntries) then
        fnUploadLocationPack(gzaChMLocationList[i])
    
    --Upload a blank enter:
    else
        fnUploadLocationDashes(gzaChMLocationList[i])
    end
end
