-- |[ ================================= Chapter 1 Scene Listing ================================ ]|
--Builds a list of cutscenes the player can make use of through the debug menu. Cutscenes in the debug menu are
-- used at the player's own risk, they may produce unusable situations that can only be remedied with the debug menu.
-- It's an addiction, I tell you.
--Note that not all cutscenes are necessarily on the debug list. There may also be cutscenes stored
-- in map folders that are not intended to be executed via the debug menu.

-- |[Setup]|
--Scene Listing variable
local sPrefix = ""
local saSceneList = {}

-- |[Adder Function]|
local fnAddScene = function(sPath)
    local i = #saSceneList + 1
    saSceneList[i] = sPath
end

-- |[ ====================================== Debug Scenes ====================================== ]|
sPrefix = "Chapter M/Scenes/000 Debug/"
--fnAddScene(sPrefix .. "A Quickadd Florentina")

-- |[ ================================== Save Point TF Scenes ================================== ]|
sPrefix = "Chapter M/Scenes/100 Transform/"
fnAddScene(sPrefix .. "Transform_NinaToHuman")
fnAddScene(sPrefix .. "Transform_NinaToZombie")
fnAddScene(sPrefix .. "Transform_NinaToMannequin")

-- |[ ==================================== Defeat Scenes ===================================== ]|
sPrefix = "Chapter M/Scenes/200 Defeat/"
fnAddScene(sPrefix .. "Defeat_Zombie")

-- |[ ==================================== Standard Scenes ===================================== ]|
sPrefix = "Chapter M/Scenes/300 Standards/"
--fnAddScene(sPrefix .. "Defeat_Zombie")

-- |[ ========================================= Upload ========================================= ]|
--Upload these to the debug menu.
local i = 1
ADebug_SetProperty("Cutscenes Total", #saSceneList)
while(saSceneList[i] ~= nil) do
	ADebug_SetProperty("Cutscene Path", i-1, saSceneList[i])
	i = i + 1
end
