-- |[ ==================================== Emergency Revert ==================================== ]|
--After being defeated by a monster, this scene shows the party on the ground. Under most circumstances,
-- a character who reverts their form would do so here.
--Since Nina doesn't do that, we just see her hit the deck.

-- |[Crouch]|
--Knock down Nina.
fnCutsceneSetFrame("Nina", "Crouch")
fnCutsceneWait(5)
fnCutsceneBlocker()

-- |[Wounded]|
--Hit the dirt.
fnCutsceneSetFrame("Nina", "Wounded")
fnCutsceneWait(70)
fnCutsceneBlocker()

-- |[Fade]|
--Fade to black.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 75, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
fnCutsceneWait(75)
fnCutsceneBlocker()

--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()
