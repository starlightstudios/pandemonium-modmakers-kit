-- |[ ================================= Scene Post-Transition ================================== ]|
--After transition, play a random dialogue line.

-- |[Variables]|
--Go here.

-- |[Overlay]|
--Set the overlay to fullblack. Fade in slowly.
AL_SetProperty("Activate Fade", 0, gci_Fade_Over_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1)
fnCutsceneWait(120)
fnCutsceneBlocker()

-- |[Fade In]|
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 180, gci_Fade_Over_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])

--Change Nina to the "Downed" frames.
Cutscene_CreateEvent("Change Mei Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nina")
    ActorEvent_SetProperty("Special Frame", "Wounded")
DL_PopActiveObject()

--Wait a bit.
fnCutsceneWait(240)
fnCutsceneBlocker()

-- |[Crouch]|
--Nina gets up.
Cutscene_CreateEvent("Change Nina Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nina")
    ActorEvent_SetProperty("Special Frame", "Crouch")
DL_PopActiveObject()

--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

-- |[Normal]|
--Nina stands up.
Cutscene_CreateEvent("Change Mei Special Frame", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nina")
    ActorEvent_SetProperty("Special Frame", "Null")
DL_PopActiveObject()

--Wait a bit.
fnCutsceneWait(15)
fnCutsceneBlocker()

-- |[Talking]|
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Dialogue
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[SOFTBLOCK] Why do I suck so much at this?") ]])
