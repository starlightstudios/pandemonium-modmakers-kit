-- |[ ==================================== Defeat By Undead ==================================== ]|
--Cutscene proper. Uses the dialogue's SceneHandler to get the TF sequence.
local bSkipMostOfScene = false

-- |[Variables]|

-- |[Repeat Check]|
--If we already have the TF, skip.
local iHasZombieForm = VM_GetVar("Root/Variables/Global/Nina/iHasZombieForm", "N")
if(iHasZombieForm == 1 and iIsRelivingScene == 0.0) then
	LM_ExecuteScript(gsStandardGameOver)
	return
end

-- |[Map Check]|
--Make sure we're in the cutscene map.
local sCurrentLevelName = AL_GetProperty("Name")
if(sCurrentLevelName ~= "RuinsBasementC") then

	--Transfer to correct map.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
	local sString = "AL_BeginTransitionTo(\"RuinsBasementC\", \"" .. LM_GetCallStack(0) .. "\")"
	fnCutsceneInstruction(sString)
	return
end

-- |[Combat]|
--Restore party to full HP in case the player doesn't want to revisit a save point.
AdvCombat_SetProperty("Restore Party")

-- |[Topics]|
--Unlock these topics if they weren't already.
--WD_SetProperty("Unlock Topic", "Alraunes", 1)

-- |[Music]|
AL_SetProperty("Music", "Null")

-- |[Flags]|
VM_SetVar("Root/Variables/Global/Nina/iHasZombieForm",     "N", 1.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iHasSeenFort", "N", 1.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iOverheardThugs", "N", 1.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawSwanky", "N", 1.0)

-- |[Cutscene Execution]|
--Set form flag.
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I died, there.[SOFTBLOCK] Shocking, I know.)[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Obviously that wasn't the end of my story.[SOFTBLOCK] Here's what happened next...)") ]])
fnCutsceneBlocker()

-- |[Movement]|
--Change forms.
fnCutsceneInstruction([[ LM_ExecuteScript(gsNinaRoot .. "FormHandlers/Nina/Form_Zombie.lua") ]])

--Fade from black to nothing over 120 ticks. This fade is under the UI.
AL_SetProperty("Activate Fade", 120, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0)

--Reposition.
fnCutsceneTeleport("Nina", 20.75, 8.50)
fnCutsceneFace("Nina", 0, 1)
fnCutsceneWait(65)
fnCutsceneBlocker()
fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
fnCutsceneBlocker()

-- |[Dialogue]|
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] I live...[SOFTBLOCK] *again*![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Ha ha ha...[SOFTBLOCK] I feel...[SOFTBLOCK] powerful![SOFTBLOCK] Wicked![SOFTBLOCK] I love this![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Oh.[SOFTBLOCK] I guess I died, didn't I?[SOFTBLOCK] That's what happens when you pick a fight with the dead.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] Perhaps now I can spread the contagion.[SOFTBLOCK] Ha ha ha![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Pah, who am I kidding.[SOFTBLOCK] I'm not evil.[SOFTBLOCK] Just a hunter who is hungry.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] And now I can eat *people*![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Still, didn't think it'd end that way.[SOFTBLOCK] Dying in some sewer.[SOFTBLOCK] Getting beaten to death by a skeleton.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Maybe I should go say hello?[SOFTBLOCK] Thank them for turning me into a...[SOFTBLOCK] zombie?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Aren't elven zombies called banshees?[SOFTBLOCK] I should have paid attention when the priest was talking.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] I should eat the priest![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Hm, I wonder if people I eat will turn into banshees or zombies, too?[SOFTBLOCK] Maybe I can get an army of the dead going?[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] *sigh*[SOFTBLOCK] But I don't really want to inconvenience anyone.[SOFTBLOCK] This sucks.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Oh, I know.[SOFTBLOCK] I can eat bad people![SOFTBLOCK] That'll be fun![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Nina's got a new mission in un-life![SOFTBLOCK] Eat them all![SOFTBLOCK] And now I can kill anyone who crosses me![BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Though I am also currently in some rotting moldy tomb.[SOFTBLOCK] Better find a way out before I start spreading the plague of undeath across the land.") ]])
fnCutsceneBlocker()

