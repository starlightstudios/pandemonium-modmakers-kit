-- |[Transform Nina to Human]|
--Used at a save point when Nina attempts to transform into a Human.

-- |[Variables]|
--Store which form Nina started the scene in, among other things.
local sStartingForm = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")
    
-- |[Execute Transformation]|
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Flash the active character to white. Immediately after, execute the transformation.
Cutscene_CreateEvent("Flash Nina White", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nina")
	ActorEvent_SetProperty("Flashwhite Quickly")
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutsceneInstruction([[ LM_ExecuteScript(gsNinaRoot .. "FormHandlers/Nina/Form_Human.lua") ]])
fnCutsceneWait(gci_Flashwhite_Ticks_Total)
fnCutsceneBlocker()

--Now wait a little bit.
fnCutsceneWait(30)
fnCutsceneBlocker()
