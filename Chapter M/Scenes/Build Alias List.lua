-- |[ ==================================== Scene Alias List ==================================== ]|
--Cutscenes can be stored under an alias. This is used for enemies on the overworld. When the player
-- is defeated or retreats or otherwise calls a script, the alias is checked instead of the literal
-- script path. Those aliases are built in this script.
local sDebugPath     = gsNinaRoot .. "Scenes/000 Debug/"
local sTransformPath = gsNinaRoot .. "Scenes/100 Transform/"
local sDefeatPath    = gsNinaRoot .. "Scenes/200 Defeat/"
local sStandardPath  = gsNinaRoot .. "Scenes/300 Standards/"

--Wipe previous aliases.
PathAlias_Clear()

-- |[ ==================================== Transform Scenes ==================================== ]|
PathAlias_CreatePath("Standard Defeat",  sStandardPath .. "Defeat/Scene_Begin.lua")

-- |[ ===================================== Combat Scenes ====================================== ]|
--Whenever a scene occurs as a result of combat, be it due to retreat, surrender, defeat, victory,
-- or ending for cutscene reasons, an alias must be used.
PathAlias_CreatePath("Standard Defeat",  sStandardPath .. "Defeat/Scene_Begin.lua")
PathAlias_CreatePath("Standard Retreat", sStandardPath .. "Retreat/Scene_Begin.lua")
PathAlias_CreatePath("Standard Revert",  sStandardPath .. "Revert/Scene_Begin.lua")

--Defeat Cutscenes
PathAlias_CreatePath("Defeat Zombie", sDefeatPath .. "Defeat_Zombie/Scene_Begin.lua")

-- |[ ======================================== Aliases ========================================= ]|
--These are the aliases that refer to the paths. These appear in the .slf data saved from Tiled.
PathAlias_CreateAliasToPath("Defeat_Zombie", "Defeat Zombie")
