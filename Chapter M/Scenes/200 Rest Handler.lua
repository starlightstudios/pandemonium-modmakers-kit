-- |[ ====================================== Rest Handler ====================================== ]|
--When the player rests, this script will be called to check if the scenario needs to do anything
-- special, such as triggering dialogues or despawning enemies.

-- |[Mod Activity Check]|
--If the mod is not active, don't do any of the below.
if(OM_IsModActive("Nina's Adventure") == false) then return end

-- |[Execution]|
--Set flags to indicate we handled this.
gbStopModExec = true
gbModHandledCall = true

--Variables.
local sLevelName = AL_GetProperty("Name")
local iVanquishedUndead = VM_GetVar("Root/Variables/NinaMod/MainQuest/iVanquishedUndead", "N")

--If we have vanquished the undead and crushed the skull, and are in RuinsBasementA, then make sure the
-- skeletons *stay dead*.
if(iVanquishedUndead == 1.0 and sLevelName == "RuinsBasementA") then
    
    EM_PushEntity("EnemyAA")
        RE_SetDestruct(true)
    DL_PopActiveObject()
    EM_PushEntity("EnemyBA")
        RE_SetDestruct(true)
    DL_PopActiveObject()
end