-- |[ ======================================= UI Loading ======================================= ]|
--Loads various UI pieces, such as turn-portraits and faces to show equipping.

-- |[Common]|
--Open the datafile.
SLF_Open(gsNinaRoot .. "Datafiles/UI.slf")

-- |[ ================================== Extraction Function =================================== ]|
--Used for the automated list sections. These are portraits, mostly.
local fnExtractList = function(sPrefix, sDLPath, saList, sCharacterPrefix)
	
	--Arg check.
	if(sPrefix == nil) then return end
	if(sDLPath == nil) then return end
	if(saList  == nil) then return end
	
	--sCharacterPrefix can be nil. It is only used for characters with multiple forms.
	
	--Run across the list and extract everything.
	local i = 1
	while(saList[i] ~= nil) do
		
		--If the name is "SKIP", ignore it. This means it will be filled in later.
		if(saList[i] == "SKIP") then
		
		--No character prefix:
		elseif(sCharacterPrefix == nil) then
			DL_ExtractBitmap(sPrefix .. saList[i], sDLPath .. saList[i])
			
		--Character prefix is present:
		else
			DL_ExtractBitmap(sPrefix .. sCharacterPrefix .. saList[i], sDLPath .. sCharacterPrefix .. saList[i])
		end
		
		--Next.
		i = i + 1
	end
end

-- |[ ==================================== Small Portraits ===================================== ]|
-- |[Loading Lists]|
--First is the player's party portraits. This is used for the turn-order indicator in combat.
local saNinaList = {"Human", "Mannequin", "Zombie"}

--Enemies and Bosses. Only the turn-order portraits.
local saEnemyList = {"Leader", "Thief", "Warrior", "SkelArch", "SkelWarr"}

-- |[Execution]|
csPrefix = "PorSml|"
csDLPath = "Root/Images/AdventureUI/TurnPortraits/"
DL_AddPath(csDLPath)

--Player's party characters.
fnExtractList(csPrefix, csDLPath, saNinaList, "Nina_")

--Enemies listing.
fnExtractList(csPrefix, csDLPath, saEnemyList)

-- |[ ========================================== Faces ========================================= ]|
--These appear next to equipment to indicate which character is using it. All the faces
-- are on a single sheet, and the engine uses a subdivision.
DL_ExtractBitmap("CharacterFaces", "Root/Images/AdventureUI/ItemIcons/NinaFaces")

-- |[ ======================================= Item Icons ======================================= ]|
--Icon for Nina's bow.
DL_ExtractBitmap("BowIcon", "Root/Images/AdventureUI/Symbols22/NinaBow")

-- |[ ===================================== Campfire Icons ===================================== ]|
DL_ExtractBitmap("Campfire|Nina", "Root/Images/AdventureUI/ChatIcons/NinaHuman")

-- |[ ======================================== Clean Up ======================================== ]|
--Close the datafiles to clear any pending data.
SLF_Close()