-- |[ ======================================== Mod Setup ======================================= ]|
--This file serves as a declaration file for your mod. If it exists, the game can detect it as
-- a playable chapter. It will be executed once when the "Nowhere" area loads when starting a new
-- game, and again when the player selects to start the chapter. The global flag gbIsStartingChapter
-- will be set to true in the second case, false in the first.

--Debug.
--io.write("== Mod Setup File ==\n")

-- |[ ===================================== Mod Detection ====================================== ]|
--Called at program startup.
if(gbIsStartingChapter == false) then

    -- |[Debug]|
    --Diagnostics written to console.
    if(gbModDetectDebug) then io.write("Appending mod [Nina's Adventure] to global list.\n") end

    -- |[Add Mod Path]|
    --Add this mod to the mod path listing.
    local sCurrentPath = fnResolvePath()
    local sTrimmedPath = fnTrimLastDirectory(sCurrentPath)
    table.insert(gsModDirectories, sTrimmedPath)

    -- |[Add Mod Name]|
    --Human-readable mod name.
    table.insert(gsModNames, "Nina's Adventure")

-- |[ ======================================= Mod Loading ====================================== ]|
--Called at program startup when loading a file.
elseif(gbIsLoadingChapter == true) then

    -- |[Common]|
    --Lua global path is always set, even if the mod is not in use. May be used for diagnostics.
    gsNinaRoot = fnTrimLastDirectory(fnResolvePath())
    
    -- |[Mod Activity Check]|
    --If the mod is not active, don't do any of the below.
    if(OM_IsModActive("Nina's Adventure") == false) then return end

    --Initialize.
    LM_ExecuteScript(gsNinaRoot .. "System/200 Initialize.lua")
    
    --Build topics.
    LM_ExecuteScript(gsNinaRoot .. "Topics/Build Topics.lua")

-- |[ =================================== Mod Initialization =================================== ]|
--Called when the player decides to play this mod.
else

    -- |[Mod Globals]|
    --This is the root path of your mod's folder. It should have a unique name, to make sure
    -- no other mod attempts to use it. This one is named Nina after its protagonist.
    gsNinaRoot = fnTrimLastDirectory(fnResolvePath())
    --io.write("Player playing mod. Root path: " .. gsNinaRoot .. "\n")
    
    --Mark this mod as active. This will cause savefiles to run modded sprites, enemies, etc.
    OM_SetModActive("Nina's Adventure")

    -- |[Load Assets]|
    --From here, we start loading the assets. The files which do this are in the same folder as
    -- this file, and they are executed in order.
    LM_ExecuteScript(gsNinaRoot .. "Audio/ZRouting.lua")
    LM_ExecuteScript(gsNinaRoot .. "System/100 Sprites.lua")
    LM_ExecuteScript(gsNinaRoot .. "System/101 Portraits.lua")
    LM_ExecuteScript(gsNinaRoot .. "System/102 Actors.lua")
    LM_ExecuteScript(gsNinaRoot .. "System/103 Scenes.lua")
    LM_ExecuteScript(gsNinaRoot .. "System/104 Overlays.lua")
    LM_ExecuteScript(gsNinaRoot .. "System/105 UI.lua")
    
    -- |[Initializer Script]|
    --Call this to handle the bulk of the initialization work.
    LM_ExecuteScript(gsNinaRoot .. "System/200 Initialize.lua")
    
    --Build topics.
    LM_ExecuteScript(gsNinaRoot .. "Topics/Build Topics.lua")
    
    -- |[Level Boot]|
    --Boot map paths. This is here because ZLaunch.lua builds map paths before the mod is flagged as active.
    gbModHandleRemapAdditions = true
    LM_ExecuteScript(gsNinaRoot .. "Maps/Map Path Remaps.lua")
    gbModHandleRemapAdditions = false
    
    -- |[Level Boot]|
    --Spawn Nina offscreen. Transition control to her before changing levels.
    TA_Create("Nina")
        TA_SetProperty("Position", -100, -100)
        TA_SetProperty("Facing", gci_Face_South)
        fnSetCharacterGraphics("Root/Images/Sprites/Nina_Human/", true)
        giPartyLeaderID = RO_GetID()
    DL_PopActiveObject()
    
    --Set Nina's ID as the party leader.
	AL_SetProperty("Player Actor ID", giPartyLeaderID)
    
    --Transition to level.
	AL_BeginTransitionTo("WoodsSA", "Null")

end
