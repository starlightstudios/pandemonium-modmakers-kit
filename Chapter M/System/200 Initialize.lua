-- |[ ============================== Nina's Adventure Initializer ============================== ]|
--Called to boot Nina's Adventure.
local sBasePath = fnResolvePath()
gbInitializeDebug = false
Debug_PushPrint(gbInitializeDebug, "Beginning [Nina's Adventure] Initialization.\n")

-- |[ ======================================= Path Setup ======================================= ]|
--Set standard paths for the rest of the chapter.
gsStandardGameOver     = gsNinaRoot .. "Scenes/300 Standards/Defeat/Scene_Begin.lua"
gsStandardRetreat      = gsNinaRoot .. "Scenes/300 Standards/Retreat/Scene_Begin.lua"
gsStandardRevert       = gsNinaRoot .. "Scenes/300 Standards/Revert/Scene_Begin.lua"
gsStandardReliveBegin  = gsNinaRoot .. "Scenes/Z Relive Handler/Scene_Begin.lua"
gsStandardReliveEnd    = gsNinaRoot .. "Scenes/Z Relive Handler/Scene_End.lua"

--Menu Paths
AM_SetProperty("Bestiary Resolve Script", gsNinaRoot .. "Journal/200 Bestiary Handler.lua")
AM_SetProperty("Profile Resolve Script",  gsNinaRoot .. "Journal/210 Profile Handler.lua")
AM_SetProperty("Quest Resolve Script",    gsNinaRoot .. "Journal/220 Quest Handler.lua")
AM_SetProperty("Location Resolve Script", gsNinaRoot .. "Journal/230 Location Handler.lua")
AM_SetProperty("Paragon Resolve Script",  gsNinaRoot .. "Journal/240 Paragon Handler.lua")
AM_SetProperty("Combat Resolve Script",   gsRoot .. "Chapter 0/240 Combat Handler.lua") --Uses defaults

--Autosave Icon
AL_SetProperty("Autosave Icon Path", "Root/Images/AdventureUI/Symbols22/RuneMei")

-- |[ ======================================== Clearing ======================================== ]|
--Clear lingering items from the inventory.
AdInv_SetProperty("Clear")

--Reset catalyst counts. These persist between chapters.
AdInv_SetProperty("Catalyst Count", gciCatalyst_Health,     VM_GetVar("Root/Variables/Global/Catalysts/iHealth", "N"))
AdInv_SetProperty("Catalyst Count", gciCatalyst_Attack,     VM_GetVar("Root/Variables/Global/Catalysts/iAttack", "N"))
AdInv_SetProperty("Catalyst Count", gciCatalyst_Initiative, VM_GetVar("Root/Variables/Global/Catalysts/iInitiative", "N"))
AdInv_SetProperty("Catalyst Count", gciCatalyst_Evade,      VM_GetVar("Root/Variables/Global/Catalysts/iEvade", "N"))
AdInv_SetProperty("Catalyst Count", gciCatalyst_Accuracy,   VM_GetVar("Root/Variables/Global/Catalysts/iAccuracy", "N"))
AdInv_SetProperty("Catalyst Count", gciCatalyst_Skill,      VM_GetVar("Root/Variables/Global/Catalysts/iSkill", "N"))

-- |[ ================================== Party Initialization ================================== ]|
-- |[ =========================== Nina =========================== ]|
--Create Nina. Her default job is "Archer".
if(AdvCombat_GetProperty("Does Party Member Exist", "Nina") == false) then
    
    --Debug.
    Debug_Print(" Creating Nina.\n")
    
    --Create.
    LM_ExecuteScript(gsNinaRoot .. "Combat/Party/Nina/000 Initialize.lua")
end

--Build Nina's costume routines.
LM_ExecuteScript(gsNinaRoot .. "CostumeHandlers/Nina/ZZ Common.lua")

-- |[ ========================================== Flags ========================================= ]|
--Mark Nina as the party leader.
gsPartyLeaderName = "Nina"

--Set the party positions. Nina is the leader, all others are empty.
Debug_Print(" Positioning party slots.\n")
AdvCombat_SetProperty("Clear Party")
AdvCombat_SetProperty("Party Slot", 0, "Nina")

--Setup the XP tables.
Debug_Print(" Booting XP table.\n")
LM_ExecuteScript(gsNinaRoot .. "Combat/Party/XP Table.lua")

-- |[ ================================= Variable Initialization ================================ ]|
-- |[General]|
Debug_Print(" Initializing variables.\n")
VM_SetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N", 10.0)

--Create script variables. This is done in its own file.
LM_ExecuteScript(sBasePath .. "201 Variables.lua")

-- |[Combat]|
--Create paths referring to enemies.
LM_ExecuteScript(gsNinaRoot .. "Combat/Enemies/Enemy List/Alias List.lua")

--Combat Paragon script
AdvCombat_SetProperty("Set Paragon Script",      gsNinaRoot .. "Combat/Enemies/Enemy List/Paragon Handler.lua")
AdvCombat_SetProperty("Set Enemy Auto Script",   gsNinaRoot .. "Combat/Enemies/Enemy List/Enemy Auto Handler.lua")
AdvCombat_SetProperty("Set Post Combat Handler", gsNinaRoot .. "Combat/Enemies/Enemy List/Post Combat Handler.lua")

--Combat Volunteer Script
AdvCombat_SetProperty("Set Volunteer Script", gsNinaRoot .. "Scenes/100 Volunteer Handler.lua")

-- |[ ================================== Character Appearance ================================== ]|
--Call the costume handlers to build default appearances for the characters.
Debug_Print(" Running costume resolvers.\n")
local bSuppressErrorsOld = gbSuppressNoListErrors
gbSuppressNoListErrors = true
LM_ExecuteScript(gsCostumeAutoresolve, "Nina_Human")
gbSuppressNoListErrors = bSuppressErrorsOld

-- |[ =================================== Doctor Bag Setting =================================== ]|
--Give Nina a basic doctor bag.
Debug_Print(" Setting doctor bag.\n")
gbAutoSetDoctorBagCurrentValues = true
LM_ExecuteScript(gsComputeDoctorBagTotalPath)

-- |[ ====================================== Party Restore ===================================== ]|
--Make sure everyone is at full HP. This affects the whole party roster, not just the active party.
Debug_Print(" Restoring party.\n")
AdvCombat_SetProperty("Restore Roster")

-- |[ ====================================== Path Building ===================================== ]|
--Run this script to build cutscenes that can be accessed from the debug menu.
Debug_Print(" Building paths.\n")
LM_ExecuteScript(gsNinaRoot .. "Scenes/Build Scene List.lua")

--Build a set of aliases. These must be used in place of hard paths for in-engine script calls.
LM_ExecuteScript(gsNinaRoot .. "Scenes/Build Alias List.lua")

--Build a list of locations the player can warp to from the debug menu.
LM_ExecuteScript(gsNinaRoot .. "Maps/Build Debug Warp List.lua", "Chapter 1")

--Standard combat paths.
AdvCombat_SetProperty("Standard Retreat Script", gsNinaRoot .. "Scenes/300 Standards/Retreat/Scene_Begin.lua")
AdvCombat_SetProperty("Standard Defeat Script",  gsNinaRoot .. "Scenes/300 Standards/Defeat/Scene_Begin.lua")
AdvCombat_SetProperty("Standard Victory Script", gsNinaRoot .. "Scenes/300 Standards/Victory/Scene_Begin.lua")

-- |[ ===================================== Music and Sound ==================================== ]|
--Default combat music.
Debug_Print(" Setting combat music.\n")
AdvCombat_SetProperty("Default Combat Music", "BattleThemeNina", 0.0000)

-- |[ ===================================== Field Abilities ==================================== ]|
--Wipe field abilities.
Debug_Print(" Setting field abilities.\n")
AdvCombat_SetProperty("Set Field Ability", 0, "NULL")
AdvCombat_SetProperty("Set Field Ability", 1, "NULL")
AdvCombat_SetProperty("Set Field Ability", 2, "NULL")
AdvCombat_SetProperty("Set Field Ability", 3, "NULL")
AdvCombat_SetProperty("Set Field Ability", 4, "NULL")


-- |[ ======================================== Catalysts ======================================= ]|
AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Health, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Health, 0)

AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Attack, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Attack, 0)

AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Initiative, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Initiative, 0)

AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Evade, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Evade, 0)

AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Accuracy, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Accuracy, 0)

AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Skill, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Skill, 0)
