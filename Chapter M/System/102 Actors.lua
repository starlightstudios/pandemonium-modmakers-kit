-- |[ ===================================== Dialogue Actors ==================================== ]|
--In the Starlight Engine, dialogue consists of a text box and a set of actors. The actors all have
-- emotes and voice ticks. This is where the actors your mod will need are created and reference
-- their assets.
--Note that actors with costumes (usually the party) will frequently change their emote sets. 
-- This is caused by changing costumes or forms (for characters who transform). Therefore, the emote
-- sets in this file should not be considered final.

-- |[Debug]|
--io.write("Creating actors.\n")

-- |[ ================================= Party/Important Actors ================================= ]|
-- |[Setup]|
--Alias listings. Characters may be known by multiple names. When speaking, if any of the names
-- used are found, that character is considered to be speaking.
local saNoAliases   = {"NOALIAS"}
local saNinaAliases = {"Nina"}

--Emotion listings.
local saNoEmotions        = {"Neutral"}
local saNinaEmotesFull    = {"Neutral", "Smirk", "Happy", "Upset"}
local saNinaEmotesLimited = {"Neutral"}

-- |[Create Actors]|
--Create actors. Note that these are their "default" aliases and properties. They can be changed later.
fnCreateDialogueActor("Nina", "Voice|Nina", "Root/Images/Portraits/NinaDialogue/", true, saNinaAliases, saNinaEmotesFull)

-- |[Debug]|
--WD_SetProperty("Debug Print Actors")

-- |[ ==================================== Enemy/NPC Actors ==================================== ]|
-- |[Setup]|
--These entities always use the neutral portrait. Some have aliases.
local saNeutralOnly = {"NEUTRALISPATH"}
fnCreateDialogueActor("Leader",  "Voice|Leader",  "Root/Images/Portraits/EnemyDialogue/Brigand_Leader|Neutral",  true, {"Leader",  "Brigand"}, saNeutralOnly)
fnCreateDialogueActor("Thief",   "Voice|Thief",   "Root/Images/Portraits/EnemyDialogue/Brigand_Thief|Neutral",   true, {"Thief",   "Brigand"}, saNeutralOnly)
fnCreateDialogueActor("Warrior", "Voice|Warrior", "Root/Images/Portraits/EnemyDialogue/Brigand_Warrior|Neutral", true, {"Warrior", "Brigand"}, saNeutralOnly)

--NPCs.
fnCreateDialogueActor("Merchant", "Voice|GenericM01", "Root/Images/Portraits/NPCs/MercM", true,  {"Merchant"}, saNeutralOnly)

--The countess.
fnCreateDialogueActor("Countess", "Voice|Countess", "Root/Images/Portraits/CountessDialogue/Countess|Neutral", true, {"Woman", "Master", "Countess"}, saNeutralOnly)

-- |[ ========================================== Debug ========================================= ]|
-- |[Debug]|
--WD_SetProperty("Debug Print Actor Info", "Nina")
