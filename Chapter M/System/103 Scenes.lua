-- |[ ========================================= Scenes ========================================= ]|
--Scenes for chapter 5. This includes TF scenes.

--Nothing here yet.

-- |[Streaming List]|
--Create an "Everything" list. All delayed images not in another list get put in this list and loaded
-- as soon as possible, but not during the load sequence.
--Because scene images tend to be really big, this list *should* be empty in an optimized program.
local sEverythingList = "Chapter M Scenes"
fnCreateDelayedLoadList(sEverythingList)

-- |[ ======================================== Clean Up ======================================== ]|
-- |[Load Issue]|
--Order queued loading.
fnLoadDelayedBitmapsFromList(sEverythingList, gciDelayedLoadLoadAtEndOfTick)

-- |[Debug]|
if(false) then
    fnPrintDelayedBitmapList(sEverythingList)
end

-- |[Close]|
SLF_Close()
