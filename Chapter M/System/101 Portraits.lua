-- |[ ======================================== Portraits ======================================= ]|
--Loads all portraits for your mod. Portraits are both what is used during dialogue and combat.
Debug_PushPrint(gbLoadDebug, "Beginning Portrait loader.\n")

-- |[Delayed Load List]|
--Create an "Everything" list. All delayed images not in another list get put in this list and loaded
-- as soon as possible, but not during the load sequence.
local sEverythingList = "Chapter M Portraits"
fnCreateDelayedLoadList(sEverythingList)

--List used for portraits that load if gbUnloadDialoguePortraitsWhenDialogueCloses is true, and don't when it's false.
local sChMDialogueList = "Chapter M Dialogue"
fnCreateDelayedLoadList(sChMDialogueList)

-- |[Dialogue Portraits Flag]|
--Setup. Allows suppression of streaming warnings if desired for some reason.
local bShowWarningsForDialoguePortraits = true
local bShowWarningsForCombatPortraits = true

-- |[Common Datalibrary Paths]|
DL_AddPath("Root/Images/Portraits/Combat/")
DL_AddPath("Root/Images/Portraits/Paragon/")

-- |[ ========================================== Nina ========================================= ]|
-- |[Setup]|
--Open Nina's portrait file.
SLF_Open(gsNinaRoot .. "Datafiles/Portraits_Nina.slf")

--These lists denote which emotes to load. The full set is used for Human and Zombie. The limited
-- set is used for Mannequin.
local saNinaEmotesFull    = {"Neutral", "Smirk", "Happy", "Upset"}
local saNinaEmotesLimited = {"Neutral"}

--Nina has three playable forms. Human, Zombie, and Mannequin are all playable in combat and have
-- associated emotes. However, Mannequin only has the neutral emote.
local zaNinaForms = {}
table.insert(zaNinaForms, {"Human",     saNinaEmotesFull})
table.insert(zaNinaForms, {"Zombie",    saNinaEmotesFull})
table.insert(zaNinaForms, {"Mannequin", saNinaEmotesLimited})

--Loading lists. These store all of the images this character uses, so that if the character
-- is created, the game can load all their assets at once.
gsaNinaDialogueList = {}
gsaNinaCombatList   = {}

-- |[Library Paths]|
DL_AddPath("Root/Images/Portraits/NinaDialogue/")

-- |[Extraction Iteration]|
--Run across all forms. Assemble a set of loading lists for this character.
for i = 1, #zaNinaForms, 1 do

    -- |[Fast-Access Pointers]|
    local sFormName    = zaNinaForms[i][1]
    local saFormEmotes = zaNinaForms[i][2]

    -- |[Setup]|
    --Resolve the name of the costume. Each form gets a unique costume name.
    local sDelayedLoadList = "Nina Costume " .. zaNinaForms[i][1]
    
    --Create the load list, and a combat variant.
    fnCreateDelayedLoadList(sDelayedLoadList)
    fnCreateDelayedLoadList(sDelayedLoadList .. " Combat")
    
    --Append it to the list of dialogue loading lists.
    table.insert(gsaNinaDialogueList, sDelayedLoadList)
    table.insert(gsaNinaCombatList,   sDelayedLoadList .. " Combat")
    
    -- |[Append To Loading]|
    --For each entry in the pattern, perform a delayed-load. This front-loads the data into RAM
    -- so the program knows where to get it when the image needs to be loaded.
    for p = 1, #saFormEmotes, 1 do
        fnExtractDelayedBitmapToList(sDelayedLoadList, "Por|Nina" .. sFormName .. "|" .. saFormEmotes[p], "Root/Images/Portraits/NinaDialogue/" .. sFormName .. "|" .. saFormEmotes[p])
    end
end

-- |[Combat Portraits]|
--Unset the portraits flag. Combat portraits should bark warnings.
Bitmap_SetNewBitmapNoHandleWarnings(bShowWarningsForCombatPortraits)

--Main
fnExtractDelayedBitmapToList("Nina Costume Human Combat",     "Party|Nina_Human",     "Root/Images/Portraits/Combat/Nina_Human")
fnExtractDelayedBitmapToList("Nina Costume Zombie Combat",    "Party|Nina_Zombie",    "Root/Images/Portraits/Combat/Nina_Zombie")
fnExtractDelayedBitmapToList("Nina Costume Mannequin Combat", "Party|Nina_Mannequin", "Root/Images/Portraits/Combat/Nina_Mannequin")

-- |[ ======================================== Countess ======================================== ]|
-- |[Setup]|
--Open the enemy portrait file.
SLF_Open(gsNinaRoot .. "Datafiles/Portraits_Countess.slf")

-- |[Extract]|
--Countess only has a neutral emote.
DL_AddPath("Root/Images/Portraits/CountessDialogue/")
fnExtractDelayedBitmapToList(sEverythingList, "Por|Countess|Neutral", "Root/Images/Portraits/CountessDialogue/Countess|Neutral")

SLF_Open(gsNinaRoot .. "Datafiles/Portraits_Amy.slf")
DL_AddPath("Root/Images/Portraits/AmyDialogue/")
fnExtractDelayedBitmapToList(sEverythingList, "Por|Golem_Amy|Neutral", "Root/Images/Portraits/AmyDialogue/Golem_Amy|Neutral")

-- |[ ======================================== Enemies ========================================= ]|
-- |[Setup]|
--Open the enemy portrait file.
SLF_Open(gsNinaRoot .. "Datafiles/Portraits_Enemies.slf")

-- |[Dialogue Portraits]|
--Setup.
DL_AddPath("Root/Images/Portraits/EnemyDialogue/")
Bitmap_SetNewBitmapNoHandleWarnings(true)

--Load.
fnExtractDelayedBitmapToList(sEverythingList, "Por|Brigand_Leader|Neutral",  "Root/Images/Portraits/EnemyDialogue/Brigand_Leader|Neutral")
fnExtractDelayedBitmapToList(sEverythingList, "Por|Brigand_Thief|Neutral",   "Root/Images/Portraits/EnemyDialogue/Brigand_Thief|Neutral")
fnExtractDelayedBitmapToList(sEverythingList, "Por|Brigand_Warrior|Neutral", "Root/Images/Portraits/EnemyDialogue/Brigand_Warrior|Neutral")

-- |[Combat Portraits]|
--These go on the everything list and should load when possible.
Bitmap_SetNewBitmapNoHandleWarnings(bShowWarningsForCombatPortraits)

--Load.
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|Brigand_Leader",   "Root/Images/Portraits/Combat/Brigand_Leader")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|Brigand_Thief",    "Root/Images/Portraits/Combat/Brigand_Thief")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|Brigand_Warrior",  "Root/Images/Portraits/Combat/Brigand_Warrior")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|Skeleton_Archer",  "Root/Images/Portraits/Combat/Skeleton_Archer")
fnExtractDelayedBitmapToList(sEverythingList, "Enemy|Skeleton_Warrior", "Root/Images/Portraits/Combat/Skeleton_Warrior")

-- |[Chapter 1 Enemies]|
--Borrowed from the base game.
SLF_Open(gsaDatafilePaths.sChapter1CombatPath)
saList = {"Jelli", "Sproutling", "SproutPecker"}
for i = 1, #saList, 1 do
    fnExtractDelayedBitmapToList(sEverythingList, "Enemy|"   .. saList[i], "Root/Images/Portraits/Combat/"  .. saList[i])
end

-- |[ ======================================= Other NPCs ======================================= ]|
-- |[Setup]|
SLF_Open(gsaDatafilePaths.sChapter0EmotePath)

-- |[Dialogue Portraits]|
DL_AddPath("Root/Images/Portraits/NPCs/")
fnExtractDelayedBitmapToList(sEverythingList, "Por|MercM|Neutral", "Root/Images/Portraits/NPCs/MercM")

-- |[ ======================================== Clean Up ======================================== ]|
-- |[Reset Flag]|
--Default flag state is true.
Bitmap_SetNewBitmapNoHandleWarnings(true)

-- |[Issue Asset Stream]|
--If this flag is set, order all dialogue images that aren't linked to a costume to load.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnLoadDelayedBitmapsFromList(sChMDialogueList, gciDelayedLoadLoadAtEndOfTick)
end

--Order all combat and unconditional images to load.
fnLoadDelayedBitmapsFromList(sEverythingList, gciDelayedLoadLoadImmediately)

-- |[Clean]|
SLF_Close()
Debug_PopPrint("Completed Portrait Loader.\n")
