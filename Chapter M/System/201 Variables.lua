-- |[ ================================ Variable Initialization ================================= ]|
--Called on startup, sets all variables for this mod to their initial values.
DL_AddPath("Root/Variables/NinaMod/")

--Zeroth save point is the one right at the start.
AL_SetProperty("Last Save Point", "WoodsSA")

-- |[System]|
--The party leader's voice is Nina's at all times.
WD_SetProperty("Set Leader Voice", "Nina")

-- |[ =================================== Costume Variables ==================================== ]|
--Nina, Unlocked
DL_AddPath("Root/Variables/Costumes/Nina/")

--Nina, Wearing
VM_SetVar("Root/Variables/Costumes/Nina/sCostumeHuman",     "S", "Normal")
VM_SetVar("Root/Variables/Costumes/Nina/sCostumeZombie",    "S", "Normal")
VM_SetVar("Root/Variables/Costumes/Nina/sCostumeMannequin", "S", "Normal")

-- |[ ================================== Main Quest Variables ================================== ]|
DL_AddPath("Root/Variables/NinaMod/MainQuest/")

-- |[Opening]|
VM_SetVar("Root/Variables/NinaMod/MainQuest/iHasSeenOpening", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iHasSeenFort", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iRevealedStaircase", "N", 0.0)

-- |[Ruins]|
VM_SetVar("Root/Variables/NinaMod/MainQuest/iOverheardThugs", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawSwanky", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iTriggeredGasTrap", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawUndead", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iGoAway", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iMetCountess", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iVanquishedUndead", "N", 0.0)

-- |[Rune Recovery]|
VM_SetVar("Root/Variables/NinaMod/MainQuest/iStartedRuneRecovery", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawTheHunt", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iRecoveredRunestone", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iSeenBigReveal", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iReturnedRunestone", "N", 0.0)

-- |[Human Route]|
VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawHumanDriveOffInvaders", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iHumanBanditsDefeated", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iHumanCompletedQuest", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iHumanSawMissingRunestone", "N", 0.0)

-- |[Mannequin Route]|
VM_SetVar("Root/Variables/NinaMod/MainQuest/iMannequinMetMaster", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawMannequinDriveOffInvaders", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iMannequinBanditsDefeated", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iMannequinCompletedQuest", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iMannequinDialogue", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iMannequinStartedTasks", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawMissingRunestone", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iToldOfLove", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iInformedMissingRunestone", "N", 0.0)

-- |[Zombie Route]|
VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawZombieDriveOffInvaders", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iZombieBanditsDefeated", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iZombieCompletedQuest", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iZombieSawMissingRunestone", "N", 0.0)

-- |[Merchant]|
VM_SetVar("Root/Variables/NinaMod/Merchant/sFormMetMerchant", "S", "Null")
VM_SetVar("Root/Variables/NinaMod/Merchant/iNoRepeats", "N", 0.0)

-- |[Dusting]|
VM_SetVar("Root/Variables/NinaMod/MainQuest/iDustedVase", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iDustedNecklace", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iDustedTablet", "N", 0.0)
VM_SetVar("Root/Variables/NinaMod/MainQuest/iDustedLockbox", "N", 0.0)

-- |[ ==================================== Paragon Tracking ==================================== ]|
--Tracks kill counters and paragon spawn locations.
local saGroupings = {}
local function fnAddGrouping(psName)
    local i = #saGroupings + 1
    saGroupings[i] = psName
end

--Grouping list.
fnAddGrouping("NinaMod")

--Create variables for each grouping.
for i = 1, #saGroupings, 1 do
    DL_AddPath("Root/Variables/Paragons/" .. saGroupings[i] .. "/")
    VM_SetVar("Root/Variables/Paragons/" .. saGroupings[i] .. "/iIsEnabled", "N", 1.0)
    VM_SetVar("Root/Variables/Paragons/" .. saGroupings[i] .. "/iKillCount", "N", 0.0)
    VM_SetVar("Root/Variables/Paragons/" .. saGroupings[i] .. "/iKillsNeeded", "N", gciParagon_KO_Needed_To_Spawn)
    VM_SetVar("Root/Variables/Paragons/" .. saGroupings[i] .. "/iHasDefeatedParagon", "N", 0.0)
end

-- |[ ========================================= Regions ======================================== ]|
DL_AddPath("Root/Variables/Regions/Nina Woods/")
VM_SetVar("Root/Variables/Regions/Nina Woods/iHasSeenThisRegion", "N", 0.0)

-- |[ ====================================== Party Setup ======================================= ]|
-- |[Nina's Stats]|
--System.
DL_AddPath("Root/Variables/Global/Nina/")

--Nina's forms.
VM_SetVar("Root/Variables/Global/Nina/sForm", "S", "Human")
VM_SetVar("Root/Variables/Global/Nina/iHasZombieForm",    "N", 0.0)
VM_SetVar("Root/Variables/Global/Nina/iHasMannequinForm", "N", 0.0)

-- |[ ==================================== Global Variables ==================================== ]|
-- |[Global Variables]|
--Variables.
gsPartyLeaderName = "Nina"
giPartyLeaderID = 0

--Following Characters.
giFollowersTotal = 0
gsaFollowerNames = {}
giaFollowerIDs = {0}
