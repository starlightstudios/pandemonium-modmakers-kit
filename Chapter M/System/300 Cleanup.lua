-- |[ ================================ Nina's Adventure Cleanup ================================ ]|
--Performs cleanup after the chapter/mod is completed.

-- |[Purge]|
--Now that we've stored everything we need, purge the datalibrary of the Chapter 1 variables.
--DL_Purge("Root/Variables/Chapter1/", false)
--DL_Purge("Root/Variables/Global/Mei/", false)
--DL_Purge("Root/Variables/Global/Florentina/", false)
--DL_Purge("Root/Variables/Global/Goat", false)

-- |[Catalyst Counts]|
local iCatalystH  = VM_GetVar("Root/Variables/Global/Catalysts/iHealth", "N")
local iCatalystAt = VM_GetVar("Root/Variables/Global/Catalysts/iAttack", "N")
local iCatalystI  = VM_GetVar("Root/Variables/Global/Catalysts/iInitiative", "N")
local iCatalystE  = VM_GetVar("Root/Variables/Global/Catalysts/iEvade", "N") + VM_GetVar("Root/Variables/Global/Catalysts/iDodge", "N")
local iCatalystAc = VM_GetVar("Root/Variables/Global/Catalysts/iAccuracy", "N")
local iCatalystSk = VM_GetVar("Root/Variables/Global/Catalysts/iMovement", "N") + VM_GetVar("Root/Variables/Global/Catalysts/iSkill", "N")

-- |[Inventory Handling]|
--Remove the Platinum Compass
VM_SetVar("Root/Variables/Global/CatalystTone/iHasCatalystTone", "N", 0.0)

--Clear party leader.
AL_SetProperty("Player Actor ID", 0)

--Clear combat party.
AdvCombat_SetProperty("Clear Party")

--Set Cash to 0, remove crafting ingredients, drop the inventory entirely.
AdInv_SetProperty("Clear")

-- |[Reinstate Catalysts]|
--Clearing the inventory zeroes off the catalyst count, but it persists between chapters. Reset them here.
AdInv_SetProperty("Catalyst Count", gciCatalyst_Health,     iCatalystH)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Attack,     iCatalystAt)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Initiative, iCatalystI)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Evade,      iCatalystE)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Accuracy,   iCatalystAc)
AdInv_SetProperty("Catalyst Count", gciCatalyst_Skill,      iCatalystSk)
    
-- |[Catalyst Counters]|
--Set the counter values to zeroes.
AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Health, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Health, 0)

AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Attack, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Attack, 0)

AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Initiative, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Initiative, 0)

AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Evade, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Evade, 0)

AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Accuracy, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Accuracy, 0)

AdInv_SetProperty("Catalyst Local Cur", gciCatalyst_Skill, 0)
AdInv_SetProperty("Catalyst Local Max", gciCatalyst_Skill, 0)
