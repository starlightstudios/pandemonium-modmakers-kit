-- |[ =================================== Chapter M Loading ==================================== ]|
--This file loads the sprites used in this chapter. It executes when the chapter loads. Note that
-- assets from other chapters can be used in addition to the ones local to the mod.

--Modify the distance filters to keep everything pixellated. This is only for sprites.
local iNearest = DM_GetEnumeration("GL_NEAREST")
ALB_SetTextureProperty("MagFilter", iNearest)
ALB_SetTextureProperty("MinFilter", iNearest)
ALB_SetTextureProperty("Special Sprite Padding", true)

--Setup.
DL_AddPath("Root/Images/Sprites/Special/")

-- |[ ==================================== Base Game Assets ==================================== ]|
-- |[Common]|
--Open the datafile.
SLF_Open(gsDatafilesPath .. "Sprites.slf")

--Cultists from chapter 1 stand in for the brigands. Also use some chapter 1 wild enemies.
local saChapter0EnemyList = {"CultistM"}
local saChapter1EnemyList = {"Jelli", "Sproutling", "Sproutpecker"}
fnExtractSpriteList   (saChapter0EnemyList, false)
fnExtractSpecialFrames(saChapter0EnemyList, "Wounded")
fnExtractSpriteList   (saChapter1EnemyList, false)
fnExtractSpecialFrames(saChapter1EnemyList, "Wounded")

--NPCs.
local saGenericNPCSpriteList = {"MercM"}
fnExtractSpriteList   (saGenericNPCSpriteList, false)
fnExtractSpecialFrames(saGenericNPCSpriteList, "Wounded")

-- |[ ======================================= Mod Assets ======================================= ]|
-- |[Common]|
--Open the datafile.
SLF_Open(gsNinaRoot .. "Datafiles/Sprites.slf")

-- |[Nina's Sprites]|
--Loads all three playable forms. The functions below will place the sprites in the DL folders: 
--  Root/Images/Sprites/Nina_Human/, Root/Images/Sprites/Nina_Zombie/, etc
--For example:
--  Root/Images/Sprites/Nina_Human/South|0 is Nina's "facing down" sprite as a human.
local saNinaList   = {"Human",      "Zombie",      "Mannequin"}
local saNinaCWList = {"Nina_Human", "Nina_Zombie", "Nina_Mannequin"}
fnExtractSpriteList   (saNinaList, true, "Nina_")
fnExtractSpecialFrames(saNinaCWList, "Crouch", "Wounded")

--Non-standard Special Frames:
--DL_ExtractBitmap("Spcl|Nina_Human|Laugh0",   "Root/Images/Sprites/Special/Nina_Human|Laugh0")

-- |[Countess Sprites]|
fnExtractSpriteList ({"Countess"}, false)

-- |[ ======================================== Clean Up ======================================== ]|
--Unset the texture filters so things that load won't be pixellated like sprites.
ALB_SetTextureProperty("Restore Defaults")
ALB_SetTextureProperty("Special Sprite Padding", false)

--Close the datafiles to clear any pending data.
SLF_Close()
