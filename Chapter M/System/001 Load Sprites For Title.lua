-- |[ ================================= Load Sprites for Title ================================= ]|
--During title loading, loads additional sprites to be used by the title screen for this mod. This
-- is usually character sprites that appear when selecting a file.
local sModPath = fnResolvePath() .. "../"

-- |[Sprites]|
SLF_Open(sModPath .. "Datafiles/Sprites.slf")
local iNearest = DM_GetEnumeration("GL_NEAREST")
ALB_SetTextureProperty("MagFilter", iNearest)
ALB_SetTextureProperty("MinFilter", iNearest)
    DL_ExtractBitmap("Nina_Human|SW|1",     "Root/Images/GUI/LoadingImg/Nina_Archer")
    DL_ExtractBitmap("Nina_Zombie|SW|1",    "Root/Images/GUI/LoadingImg/Nina_Banshee")
    DL_ExtractBitmap("Nina_Mannequin|SW|1", "Root/Images/GUI/LoadingImg/Nina_Mannequin")
ALB_SetTextureProperty("Restore Defaults")
