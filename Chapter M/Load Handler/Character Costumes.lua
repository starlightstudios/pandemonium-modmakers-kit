-- |[ ================================ Character Costume Loader ================================ ]|
--Order all characters in this mod to re-run their costume resolvers. This is called after party
-- entities have spawned.

-- |[Mod Activity Check]|
--If the mod is not active, don't do any of the below.
if(OM_IsModActive("Nina's Adventure") == false) then return end

-- |[Execute]|
LM_ExecuteScript(gsCharacterAutoresolve, "Nina")
