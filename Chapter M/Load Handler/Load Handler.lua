-- |[ ====================================== Load Handler ====================================== ]|
--Called when this mod is loading. Sets paths for this mod to use.

-- |[Mod Activity Check]|
--If the mod is not active, don't do any of the below.
if(OM_IsModActive("Nina's Adventure") == false) then return end

-- |[Chapter Check]|
local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")
if(iCurrentChapter ~= 10) then return end

--Flags.
gbStopModExec = true
gbModHandledCall = true

-- |[Execute]|
--Common.
zLoad.sDebugString      = "Running [Nina's Adventure] load.\n"
zLoad.sMapLookupPath    = gsRoot .. "Maps/Z Map Lookups/Chapter 2 Lookups.lua"
zLoad.bCheckNoMapVar    = true
zLoad.sMapCheckVar      = "Root/Variables/Chapter2/Scenes/iHasNoMap"
zLoad.iMapCheckCase     = 0.0
zLoad.sXPTablePath      = gsNinaRoot .. "Combat/Party/XP Table.lua"
zLoad.sEnemyAliasPath   = gsNinaRoot .. "Combat/Enemies/Enemy List/Alias List.lua"
zLoad.sParagonPath      = gsNinaRoot .. "Combat/Enemies/Enemy List/Paragon Handler.lua"
zLoad.sEnemyAutoPath    = gsNinaRoot .. "Combat/Enemies/Enemy List/Enemy Auto Handler.lua"
zLoad.sPostCombatPath   = gsNinaRoot .. "Combat/Enemies/Enemy List/Post Combat Handler.lua"
zLoad.sVolunteerPath    = gsNinaRoot .. "Scenes/100 Volunteer Handler.lua"
zLoad.sSceneListPath    = gsNinaRoot .. "Scenes/Build Scene List.lua"
zLoad.sSceneAliasPath   = gsNinaRoot .. "Scenes/Build Alias List.lua"
zLoad.sWarpListPath     = gsNinaRoot .. "Maps/Build Debug Warp List.lua"
zLoad.sWarpListName     = "Chapter M"
zLoad.sRetreatPath      = gsNinaRoot .. "Scenes/300 Standards/Retreat/Scene_Begin.lua"
zLoad.sDefeatPath       = gsNinaRoot .. "Scenes/300 Standards/Defeat/Scene_Begin.lua"
zLoad.sVictoryPath      = gsNinaRoot .. "Scenes/300 Standards/Victory/Scene_Begin.lua"
zLoad.sCombatMusic      = "BattleThemeNina"
zLoad.fCombatMusicStart = 0.0000
zLoad.sLoadHandler      = "Boot Chapter 1"

--System Paths
zLoad.sStandardGameOver    = gsNinaRoot .. "Scenes/300 Standards/Defeat/Scene_Begin.lua"
zLoad.sStandardRetreat     = gsNinaRoot .. "Scenes/300 Standards/Retreat/Scene_Begin.lua"
zLoad.sStandardRevert      = gsNinaRoot .. "Scenes/300 Standards/Revert/Scene_Begin.lua"
zLoad.sStandardReliveBegin = gsNinaRoot .. "Scenes/Z Relive Handler/Scene_Begin.lua"
zLoad.sStandardReliveEnd   = gsNinaRoot .. "Scenes/Z Relive Handler/Scene_End.lua"
zLoad.sBestiaryHandler     = gsNinaRoot .. "Journal/200 Bestiary Handler.lua"
zLoad.sProfileHandler      = gsNinaRoot .. "Journal/210 Profile Handler.lua"
zLoad.sQuestHandler        = gsNinaRoot .. "Journal/220 Quest Handler.lua"
zLoad.sLocationHandler     = gsNinaRoot .. "Journal/230 Location Handler.lua"
zLoad.sParagonHandler      = gsNinaRoot .. "Journal/240 Paragon Handler.lua"
zLoad.sCombatHandler       = gsRoot .. "Chapter 0/240 Combat Handler.lua"
zLoad.sKOTrackerPath       = "Root/Variables/ChapterM/KOTracker/"
zLoad.sAutosaveIconPath    = "Root/Images/AdventureUI/Symbols22/RuneSanya"

--Images.
zLoad.sFormIcon   = "Root/Images/AdventureUI/CampfireMenuIcon/TransformMei"
zLoad.sReliveIcon = "Root/Images/AdventureUI/CampfireMenuIcon/ReliveMei"
zLoad.sCostumeIcon = "Root/Images/AdventureUI/CampfireMenuIcon/Costume"

--Field Abilities
zLoad.saFieldAbilityPaths = {}

--Load interrupt.
LI_SetProperty("Boot Chapter 1")

--Order assets to load. Normally this happens in the chapter 000 Initialize.lua file, but that
-- file is run before we know what chapter it is.
--If they already loaded, do nothing.
LM_ExecuteScript(gsNinaRoot .. "Audio/ZRouting.lua")
LM_ExecuteScript(gsNinaRoot .. "System/100 Sprites.lua")
LM_ExecuteScript(gsNinaRoot .. "System/101 Portraits.lua")
LM_ExecuteScript(gsNinaRoot .. "System/102 Actors.lua")
LM_ExecuteScript(gsNinaRoot .. "System/103 Scenes.lua")
LM_ExecuteScript(gsNinaRoot .. "System/104 Overlays.lua")
LM_ExecuteScript(gsNinaRoot .. "System/105 UI.lua")
SLF_Close()
