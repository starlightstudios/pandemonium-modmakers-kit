-- |[ =================================== Music Routing File =================================== ]|
-- |[Variables]|
local sBasePath = fnResolvePath()

-- |[Registration Function]|
--Loads music with set loops.
local function fnRegisterMusic(psMusicName, psPath, pfLoopStart, pfLoopEnd)
	
	--Arg check.
	if(psMusicName == nil) then return end
	if(psPath == nil) then return end
	
	--If the pfLoopStart and/or pfLoopEnd are nil, then this doesn't loop.
	if(pfLoopStart == nil or pfLoopEnd == nil) then
		AudioManager_Register(psMusicName, "AsMusic", "AsStream", psPath)
		
	--Otherwise, register with looping.
	else
		AudioManager_Register(psMusicName, "AsMusic", "AsStream", psPath, pfLoopStart, pfLoopEnd)
	end

end

-- |[Battle Themes]|
local sBattlePath = sBasePath
fnRegisterMusic("BattleThemeNina", sBattlePath .. "Nina Battle Theme.ogg", 3.962, 60 + 44.757)
