-- |[ ====================================== Voice Samples ===================================== ]|
--Called at mod startup. Resolves voice clips used in this mod.
local fLocalVolume = 0.45
local sVoicePath = fnResolvePath()

-- |[Main Characters]|
--Function.
local fnRegisterVoice = function(sName, sFilename, fUseVolume)
    
    --Arg check.
    if(sName == nil) then return end
    
    --If no filename is provided, it's the same as the character name.
    if(sFilename == nil) then sFilename = sName end
    
    --Volume. If no volume is passed, used the local volume.
    local fApplyVolume = fLocalVolume
    if(fUseVolume ~= nil) then fApplyVolume = fUseVolume end
    
    --Register.
    AudioManager_Register("Voice|" .. sName,  "AsSound", "AsSample", sVoicePath .. sFilename .. ".ogg")
        AudioPackage_SetLocalVolume(fApplyVolume)
    
end

--Main character listing.
fnRegisterVoice("Countess")
fnRegisterVoice("Leader")
fnRegisterVoice("Nina")
fnRegisterVoice("Thief")
fnRegisterVoice("Warrior")
