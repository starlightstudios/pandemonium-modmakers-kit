-- |[ ====================================== Sound Samples ===================================== ]|
--Called at mod startup. Resolves sound files used by this mod.
local sSoundPath = fnResolvePath()

--Register.
AudioManager_Register("Nina|DustOff", "AsSound", "AsSample", sSoundPath .. "DustOff.ogg")
AudioManager_Register("Nina|Gas",     "AsSound", "AsSample", sSoundPath .. "Gas.ogg")
AudioManager_Register("Nina|Kiss",    "AsSound", "AsSample", sSoundPath .. "Kiss.ogg")
AudioManager_Register("Nina|Petrify", "AsSound", "AsSample", sSoundPath .. "Petrify.ogg")
