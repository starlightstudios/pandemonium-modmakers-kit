-- |[ ===================================== Audio Routing ====================================== ]|
--Routing file. Called on mod boot, calls subscripts to load audio unique to the mod.
local sBasePath = fnResolvePath()
LM_ExecuteScript(sBasePath .. "Music/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "VoiceSFX/ZRouting.lua")
LM_ExecuteScript(sBasePath .. "WorldSFX/ZRouting.lua")
