-- |[ =============================== Character Costume Resolver =============================== ]|
--Called if the base version of the character resolver doesn't catch the character.

-- |[Mod Activity Check]|
--If the mod is not active, don't do any of the below.
if(OM_IsModActive("Nina's Adventure") == false) then return end

-- |[Arguments]|
local bShowDebug = false
local sBasePath = fnResolvePath()
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sCharacterName = LM_GetScriptArgument(0)
Debug_PushPrint(bShowDebug, "[ === Nina's Adventure Character Autoresolve === ]\n")
Debug_Print("Provided name: " .. sCharacterName .. "\n")

--Special: "No Leader" means no character.
if(sCharacterName == "No Leader") then
    Debug_PopPrint("[ === Nina's Adventure Completes, No Leader === ]\n")
    return
end

-- |[ ============ Nina ============ ]|
--Main character of this mod.
if(sCharacterName == "Nina") then
    
    --Flag that we caught the case.
    gbModHandledCall = true
    
    --Character may not exist.
    if(AdvCombat_GetProperty("Does Party Member Exist", "Nina") == false) then return end
    
    --Set internal name, get job.
    gsCharacterFinalName = "Nina"
    AdvCombat_SetProperty("Push Party Member", "Nina")
        local sCurrentJob = AdvCombatEntity_GetProperty("Current Job")
    DL_PopActiveObject()
    
    --Change job to a costume form.
    if(sCurrentJob == "Archer") then
        gsCharacterFinalJob = "Human"
    elseif(sCurrentJob == "Banshee") then
        gsCharacterFinalJob = "Zombie"
    elseif(sCurrentJob == "Mannequin") then
        gsCharacterFinalJob = "Mannequin"
    end
end
Debug_PopPrint("[ === Nina's Adventure Completes === ]\n")