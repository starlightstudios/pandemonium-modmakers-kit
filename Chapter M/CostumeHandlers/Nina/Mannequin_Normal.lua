-- |[ ================================ Nina, Mannequin, Normal ================================= ]|
-- |[Setup]|
--Set all variables for this script. Special frames and alignments need to be entered below.
-- First, constants across this character.
local sActorName = "Nina"
local sCombatName = "Nina"
local sFieldName = "Nina"
local sFormVar = "Root/Variables/Global/Nina/sForm"

--Costume Variables
local sCostumeName = "Normal"
local sCostumeVar = "Root/Variables/Costumes/Nina/sCostumeMannequin"

--Actor Portrait
local sDialoguePath = "Root/Images/Portraits/NinaDialogue/Mannequin"

--Sprite
local sSpritePath = "Root/Images/Sprites/Nina_Mannequin/"
local sSpecialPath = "Root/Images/Sprites/Special/Nina|"
local fIndexX = 2
local fIndexY = 0

--Combat
local sCombatPortrait = "Root/Images/Portraits/Combat/Nina_Mannequin"
local sCountermask    = "NULL"
local sTurnPortrait   = "Root/Images/AdventureUI/TurnPortraits/Nina_Mannequin"

--Delayed-Load Groupings
local sDialogueCostumeGrouping = "Nina Costume Zombie"
local sCombatCostumeGrouping   = sDialogueCostumeGrouping .. " Combat"
local saDialogueGroupingList   = gsaNinaDialogueList
local saCombatGroupingList     = gsaNinaCombatList

-- |[Stop Check]|
--If not in this form, just flag the variable.
local sCharacterForm = VM_GetVar(sFormVar, "S")
VM_SetVar(sCostumeVar, "S", sCostumeName)
if(sCharacterForm ~= "Mannequin") then
    return
end

-- |[ ======================================== Dialogue ======================================== ]|
--Standard.
fnSetNinaDialogue(sDialoguePath, true)

-- |[ ====================================== Field Sprite ====================================== ]|
fnSetNinaFieldSprite(sSpritePath, sSpecialPath, false)
fnSetNinaLuaSpriteLookup(sSpritePath)
fnSetNinaIdleAnimationAdvanced("Null")

-- |[ ===================================== Combat and UI ====================================== ]|
--Basic.
fnSetNinaCombatData(sCombatName, sCombatPortrait, sCountermask, sTurnPortrait, sSpritePath, fIndexX, fIndexY)

--Alignments.
if(AdvCombat_GetProperty("Does Party Member Exist", sCombatName) == true) then
    AdvCombat_SetProperty("Push Party Member", sCombatName)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Basemenu,         -69, 449)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      150, 125)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -191, -26)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 197, 111)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Equipment,        -13,  97)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Skills,            17, 130)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Status,            13,  88)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Vendor,           -94, 125)
        AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Victory,   620,  96)
    DL_PopActiveObject()
end

-- |[ ================================= Loading and Unloading ================================== ]|
--Order all not-in-use dialogue and combat images to unload their data. Then order all in-use 
-- images to load their data. Images that are in multiple sets will correctly load their data
-- if the load is called after the unload.
--Dialogue is ordered to load if this flag is false. Otherwise, it loads on demand.
if(gbUnloadDialoguePortraitsWhenDialogueCloses == false) then
    fnUnloadGrouping(saDialogueGroupingList, sDialogueCostumeGrouping)
    fnLoadGrouping  (saDialogueGroupingList, sDialogueCostumeGrouping)
end

--Combat always loads.
fnUnloadGrouping(saCombatGroupingList, sCombatCostumeGrouping)
fnLoadGrouping  (saCombatGroupingList, sCombatCostumeGrouping)
