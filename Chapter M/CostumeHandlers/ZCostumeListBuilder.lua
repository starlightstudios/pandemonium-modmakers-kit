-- |[ ================================== Costume Routing File ================================== ]|
--Mods may add their local characters to the costume listing. This always fires regardless of "chapter".

-- |[Mod Activity Check]|
--If the mod is not active, don't do any of the below.
if(OM_IsModActive("Nina's Adventure") == false) then return end

-- |[Execute]|
if(gsPartyLeaderName == "Nina") then
    AM_SetProperty("Register Costume Heading", "NinaHuman", "Nina", "Nina", "Elf", "Root/Images/Portraits/NinaDialogue/Human|Neutral")
    AM_SetProperty("Set Heading Alignments",   "NinaHuman", -10.0, 0.0, 142.0,  9.0, 454.0, 454.0)
end