-- |[ ================================ Form Autoresolve Builder ================================ ]|
--Only called at startup. Places extra character information on the costume autoresolve lists.
local sBasePath = fnResolvePath()

-- |[Mod Activity Check]|
--If the mod is not active, don't do any of the below.
if(OM_IsModActive("Nina's Adventure") == false) then return end

--Debug.
--io.write("Running [Nina's Adventure] form autoresolve.\n")

-- |[ =========================== Nina =========================== ]|
--Human
fnAddCostumeGroup("Nina_Human", "Human", "Root/Variables/Global/Nina/sForm", "Root/Variables/Costumes/Nina/sCostumeHuman")
fnAddCostumePath("Normal", sBasePath .. "Nina/Human_Normal.lua")

--Zombie
fnAddCostumeGroup("Nina_Zombie", "Zombie", "Root/Variables/Global/Nina/sForm", "Root/Variables/Costumes/Nina/sCostumeZombie")
fnAddCostumePath("Normal", sBasePath .. "Nina/Zombie_Normal.lua")

--Mannequin
fnAddCostumeGroup("Nina_Mannequin", "Mannequin", "Root/Variables/Global/Nina/sForm", "Root/Variables/Costumes/Nina/sCostumeMannequin")
fnAddCostumePath("Normal", sBasePath .. "Nina/Mannequin_Normal.lua")

