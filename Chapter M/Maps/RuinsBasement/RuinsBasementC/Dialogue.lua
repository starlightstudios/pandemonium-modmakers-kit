-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Countess]|
    if(sActorName == "Countess") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] A tomb?[SOFTBLOCK] Buried beneath the estate?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] And not belonging to the estate itself, of course.[SOFTBLOCK] This tomb is much older.[SOFTBLOCK] I wonder how old?[SOFTBLOCK] There's no writing in here.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I don't recognize the architecture, either.[SOFTBLOCK] Nor is there any clue here as to why a skull might be doing wild necromancy.[BLOCK][CLEAR]") ]])
        
        -- |[Special]|
        local sNinaForm = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")
        if(sNinaForm == "Mannequin") then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Perhaps when I have repaired the estate, I could find a scholar to check the site.[SOFTBLOCK] For now, best let the dead lie.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Master.[SOFTBLOCK] Do you know any necromantic magic?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Me?[SOFTBLOCK] Heavens, no.[SOFTBLOCK] I know a arcanistry from my schooling.[SOFTBLOCK] I can conjure simple things, warp objects if I have prepared a spot.[SOFTBLOCK] Perhaps light a candle.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Binding the souls of the dead is far, far beyond my abilities.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Why do you ask?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Are you going to make me order you to tell me?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I.[SOFTBLOCK] am.[SOFTBLOCK] Worried.[SOFTBLOCK] I am worried the skull reacted to your magic.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Oh, oh, ohhhh...[SOFTBLOCK] I had not thought of that.[SOFTBLOCK] I did make use of my spells to teleport my things here, after all.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] But I have you to protect me, mannequin.[SOFTBLOCK] If another such incident happens, I will have a guardian.[SOFTBLOCK] We should clear the tomb later, though.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I appreciate your concern.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[SOFTBLOCK] Ngh.[SOFTBLOCK] Thank you, master.") ]])
        
        -- |[Normal]|
        else
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Perhaps when I have repaired the estate, I could find a scholar to check the site.[SOFTBLOCK] For now, best let the dead lie.") ]])
        end
    end
end
