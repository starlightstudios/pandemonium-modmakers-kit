-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "QuantirManseTheme"
local sMapResolveName = "Map Resolve Name"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation(sMapResolveName)

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If the player has finished the Countess' quest and told her about it, she spawns here.
    local iMannequinCompletedQuest = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMannequinCompletedQuest", "N")
    if(iMannequinCompletedQuest == 1.0) then
        fnStandardNPCByPosition("Countess")
    end

    -- |[Overlays]|
    --Place overlay code here.
    local iVanquishedUndead = VM_GetVar("Root/Variables/NinaMod/MainQuest/iVanquishedUndead", "N")
    if(iVanquishedUndead == 0.0) then
        AL_SetProperty("Set Layer Disabled", "SkullWrecked", true)
    else
        AL_SetProperty("Set Layer Disabled", "SkullActive", true)
    end
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.

end
