-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
-- |[ ====================================== Examinables ======================================= ]|
if(sObjectName == "TheSkull") then

    -- |[Variables]|
    local sNinaForm = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")
            
    -- |[Hasn't Destroyed]|
    local iVanquishedUndead = VM_GetVar("Root/Variables/NinaMod/MainQuest/iVanquishedUndead", "N")
    if(iVanquishedUndead == 0.0) then
        
        -- |[Flag]|
        VM_SetVar("Root/Variables/NinaMod/MainQuest/iVanquishedUndead", "N", 1.0)
        local iGoAway = VM_GetVar("Root/Variables/NinaMod/MainQuest/iGoAway", "N")
        
        -- |[Human]|
        if(sNinaForm == "Human") then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (I happened upon a skull, its eyes glowing with unearthly power.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Perhaps if it had been left to sit for a century, gathering magic into itself, it could have become a real threat.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] (So I smashed it!)") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()

            --Layer change.
            fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
            fnCutsceneLayerDisabled("SkullWrecked", false)
            fnCutsceneLayerDisabled("SkullActive",  true)
            fnCutsceneWait(25)
            fnCutsceneBlocker()

            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Well that was pretty easy![SOFTBLOCK] A bit too easy?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I guess that was it.[SOFTBLOCK] Smash the skull and you win.[SOFTBLOCK] Disappointing.[BLOCK][CLEAR]") ]])
            if(iGoAway == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Maybe that lady will let me in now.") ]])
            else
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Maybe there's something actually useful in that room I saw near the entrance.") ]])
            end
            fnCutsceneBlocker()
        
        -- |[Mannequin]|
        elseif(sNinaForm == "Mannequin") then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (I happened upon a skull, its eyes glowing with unearthly power.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Perhaps if it had been left to sit for a century, gathering magic into itself, it could have become a real threat.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (But I was not intending to sit there and allow that to happen.[SOFTBLOCK] My master had ordered me to destroy the undead threat.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (I crushed it to dust.)") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()

            --Layer change.
            fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
            fnCutsceneLayerDisabled("SkullWrecked", false)
            fnCutsceneLayerDisabled("SkullActive",  true)
            fnCutsceneWait(25)
            fnCutsceneBlocker()

            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] The undead threat is extinguished.[SOFTBLOCK] The master's will be done.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I must return to the master.[SOFTBLOCK] I must inform her of my success.") ]])
            fnCutsceneBlocker()
        
        -- |[Zombie]|
        elseif(sNinaForm == "Zombie") then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (I happened upon a skull, its eyes glowing with unearthly power.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Perhaps if it had been left to sit for a century, gathering magic into itself, it could have become a real threat.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] (...[SOFTBLOCK] So I crushed it to dust!)") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()

            --Layer change.
            fnCutsceneInstruction([[ AudioManager_PlaySound("World|Thump") ]])
            fnCutsceneLayerDisabled("SkullWrecked", false)
            fnCutsceneLayerDisabled("SkullActive",  true)
            fnCutsceneWait(25)
            fnCutsceneBlocker()

            --Dialogue.
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] Sorry, skelly, you cramp my style.[SOFTBLOCK] There's only room for one queen of the dead, and it's me.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Oh crap was I supposed to serve that thing?[SOFTBLOCK] Was I bound to it's will?[SOFTBLOCK] Will I crumble to dust now?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Doesn't seem so.[SOFTBLOCK] Now I'm in charge![SOFTBLOCK] Unless that deanimated the boners outside.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] And if it did, screw them for killing me to begin with![SOFTBLOCK] Yeah, suck it, creepy skull![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I'm bored.[SOFTBLOCK] Maybe I can go find someone to eat or something...") ]])
            fnCutsceneBlocker()
    
        end

    -- |[Has Destroyed]|
    else
        -- |[Human]|
        if(sNinaForm == "Human") then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (The skull had been reduced to fragments.[SOFTBLOCK] The malevolence in the air had subsided almost insantly.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (There was nothing more for me to do here.)") ]])
            fnCutsceneBlocker()
        
        -- |[Mannequin]|
        elseif(sNinaForm == "Mannequin") then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (The skull had been reduced to fragments.[SOFTBLOCK] The malevolence in the air had subsided almost insantly.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (My mission complete, I returned to my master...)") ]])
            fnCutsceneBlocker()
        
        -- |[Zombie]|
        elseif(sNinaForm == "Zombie") then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (The skull had been reduced to fragments.[SOFTBLOCK] The malevolence in the air had subsided almost insantly.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] (It felt empowering to destroy it.[SOFTBLOCK] I quite liked destroying things all of the sudden...)") ]])
            fnCutsceneBlocker()

        end
    end

-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
