-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[ ======================= Countess ======================= ]|
    if(sActorName == "Countess") then
        
        -- |[Variables]|
        local sNinaForm = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")
        
        -- |[ ================== Mannequin Form ================== ]|
        if(sNinaForm == "Mannequin") then
        
            -- |[Variables]|
            local iMannequinCompletedQuest  = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMannequinCompletedQuest", "N")
            local iSawMissingRunestone      = VM_GetVar("Root/Variables/NinaMod/MainQuest/iSawMissingRunestone", "N")
            local iInformedMissingRunestone = VM_GetVar("Root/Variables/NinaMod/MainQuest/iInformedMissingRunestone", "N")
            
            -- |[Mannequin Quest]|
            if(iMannequinCompletedQuest == 0.0) then
            
                --Variables.
                local iMannequinBanditsDefeated = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMannequinBanditsDefeated", "N")
                local iVanquishedUndead         = VM_GetVar("Root/Variables/NinaMod/MainQuest/iVanquishedUndead", "N")
                
                --Common.
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
                
                --Has not dealt with the undead or the bandits:
                if(iMannequinBanditsDefeated < 3.0 and iVanquishedUndead == 0.0) then
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Mannequin, did you need my assistance?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I believe my instructions were quite clear.[SOFTBLOCK] Did you have difficulty understanding them?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] No, master.[SOFTBLOCK] I will drive away the bandits and eliminate the undead.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Good.[SOFTBLOCK] To your tasks, then.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.") ]])
                
                --Has dealt with the bandits:
                elseif(iMannequinBanditsDefeated >= 3.0 and iVanquishedUndead == 0.0) then
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Mannequin, did you need my assistance?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I believe my instructions were quite clear.[SOFTBLOCK] Did you have difficulty understanding them?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] No, master.[SOFTBLOCK] The bandits have been driven off.[SOFTBLOCK] I will destroy the undead.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Seems you're making good progress.[SOFTBLOCK] Well done, mannequin.[SOFTBLOCK] Return to your task.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Hearing my master congratulate me on a job well done made my skin tingle.[SOFTBLOCK] I was addicted to serving.)[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (The mantra kept running in my head.[SOFTBLOCK] I had to go to the eastern parts of the basement to destroy the undead.)") ]])
                
                --Has dealt with the undead:
                elseif(iMannequinBanditsDefeated < 3.0 and iVanquishedUndead == 1.0) then
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Mannequin, did you need my assistance?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I believe my instructions were quite clear.[SOFTBLOCK] Did you have difficulty understanding them?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] No, master.[SOFTBLOCK] The undead situation has been dealt with.[SOFTBLOCK] I will drive the bandits off.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Seems you're making good progress.[SOFTBLOCK] Well done, mannequin.[SOFTBLOCK] Return to your task.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Hearing my master congratulate me on a job well done made my skin tingle.[SOFTBLOCK] I was addicted to serving.)[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (The mantra kept running in my head.[SOFTBLOCK] I had to go to the walled garden to drive off the bandits.)") ]])
                
                --Quest complete:
                else
                
                    --Flag quest completed.
                    VM_SetVar("Root/Variables/NinaMod/MainQuest/iMannequinCompletedQuest", "N", 1.0)
                    
                    --Dialogue.
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Mannequin, is your task completed?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[SOFTBLOCK] Your will has been done.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Good.[SOFTBLOCK] First, what did you do to the bandits?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Defeated them in combat.[SOFTBLOCK] They have fled.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Seems you're more than a simple warrior.[SOFTBLOCK] Then again, perhaps your single-minded determination is what makes you formidable.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Now, what was the cause of my undead problem?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] An artifact, a glowing skull.[SOFTBLOCK] I smashed it.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Hm, was it possessed?[SOFTBLOCK] Cursed?[SOFTBLOCK] How did it get here, and why now?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I am a mannequin.[SOFTBLOCK] Loyalty undying.[SOFTBLOCK] Serve the master.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Blast.[SOFTBLOCK] Seems you don't actually know.[SOFTBLOCK] I'll look into it myself, then.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Well then, I suppose congratulations are in order.[SOFTBLOCK] Good job, mannequin.[SOFTBLOCK] You have proven useful to me.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Ngh...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] ...?[SOFTBLOCK] What was that?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Mannequin.[SOFTBLOCK] Good job, I appreciate your work.[SOFTBLOCK] You are useful to me.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Ungh...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Ha ha![SOFTBLOCK] You came![BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[SOFTBLOCK] I came.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] If you had any fluids you've be soaked right now.[SOFTBLOCK] I suppose that's an advantage of being resin, isn't it?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] So, I wonder if it was the mantra, or the transformation, that brought this about?[SOFTBLOCK] Mannequin, did you enjoy serving a noble lady when you were a human?[SOFTBLOCK] Or elf, I suppose.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] No, master.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] If I can make a slave fall in love with me...[SOFTBLOCK] Is she really a slave?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] We'll need to find more humans to experiment on.[SOFTBLOCK] You continue to impress me, mannequin.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Ahhh...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Oh my, that was unintentional.[SOFTBLOCK] Well, what's an orgasm between a master and her mannequin?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] All right, enough fun.[SOFTBLOCK] Mannequin, I have some cleaning duties for you...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] But...[SOFTBLOCK] It's been a long time since I had someone to talk to.[SOFTBLOCK] Perhaps we should...[SOFTBLOCK] chat for a moment?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[SOFTBLOCK] Your will be done.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (It was an unusual request, but at the time my mind obviously was in no shape to comprehend that.)") ]])
                end
                fnCutsceneBlocker()
            
            -- |[Normal Dialogue]|
            elseif(iMannequinCompletedQuest == 1.0 and iSawMissingRunestone == 0.0) then
            
                --Variables.
                local iMannequinDialogue = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMannequinDialogue", "N")
            
                --First sequence:
                if(iMannequinDialogue == 0.0) then
                    VM_SetVar("Root/Variables/NinaMod/MainQuest/iMannequinDialogue", "N", 1.0)
                    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Mannequin...[SOFTBLOCK] do you like speaking with me?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I order you to tell me the truth, right now.[SOFTBLOCK] You can hide nothing from me.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[SOFTBLOCK] I enjoy talking to you.[SOFTBLOCK] I like the sound of your voice.[SOFTBLOCK] You are very pretty.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] ...[SOFTBLOCK] Truly?[SOFTBLOCK] But you are a mannequin.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I am.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] How can you have emotions?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I wonder more about you with each passing moment.[SOFTBLOCK] Are you truly in my control?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Then take your knife, and hold it to your throat.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (I took out my skinning knife and held it to my throat.[SOFTBLOCK] I waited for another order.)[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I could push that knife into you and kill you, you know.[SOFTBLOCK] You have no veins, no blood, but slicing your head off would kill you.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] What do you think of that, mannequin?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I am a mannequin.[SOFTBLOCK] Loyalty undying.[SOFTBLOCK] Serve the master.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] That's enough, put the knife away.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Should I assemble an army of such things?[SOFTBLOCK] Well, that'd be a quick way to get a price on my head.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] But an unceasingly loyal thing that follows my every command, and somehow seems to be attracted to me...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Tch.[SOFTBLOCK] I must repair the estate before I do any such things.[SOFTBLOCK] But you will aid me in that, won't you, mannequin?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[SOFTBLOCK] Your will be done.") ]])
            
                --Second sequence:
                elseif(iMannequinDialogue == 1.0) then
                    VM_SetVar("Root/Variables/NinaMod/MainQuest/iMannequinDialogue", "N", 2.0)
                    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] So, my mannequin.[SOFTBLOCK] What do you wish to talk about?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I can think of nothing.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Don't you want to know more about me?[SOFTBLOCK] Where I'm from, why I'm here?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Then why didn't you ask me?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I am a mannequin.[SOFTBLOCK] Loyalty -[SOFTBLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Silence.[SOFTBLOCK] Answer me.[SOFTBLOCK] I command it.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I -[SOFTBLOCK] I -[SOFTBLOCK] I do nothing if you do not order it, master.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I will ask you if you tell me to.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Hm.[SOFTBLOCK] I wonder.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Despite your protestations, you have some willpower in there.[SOFTBLOCK] You do have curiosity, but that loyalty overrides it.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Well then.[SOFTBLOCK] Mannequin, when I tell you to, you will ask me whatever questions about me suit your fancy.[SOFTBLOCK] And if I tell you to be quiet, you will be.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[SOFTBLOCK] Your will be done.") ]])
                    
                --Third sequence:
                elseif(iMannequinDialogue == 2.0) then
                    VM_SetVar("Root/Variables/NinaMod/MainQuest/iMannequinDialogue", "N", 3.0)
                    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Hm?[SOFTBLOCK] Mannequin, are you tired?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] No, master.[SOFTBLOCK] I do not tire.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Well, I do.[SOFTBLOCK] Mannequin...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] ...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I am going to sleep.[SOFTBLOCK] Stand guard over me through the night.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[SOFTBLOCK] Your will be done.") ]])
                    fnCutsceneBlocker()
                    fnCutsceneWait(25)
                    fnCutsceneBlocker()
                    
                    --Fade out.
                    fnCutsceneWait(165)
                    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
                    fnCutsceneBlocker()
                    
                    --Reposition.
                    fnCutsceneTeleport("Nina", 9.25, 14.50)
                    fnCutsceneFace("Nina", 0, 1)
                    fnCutsceneTeleport("Countess", 13.25, 13.50)
                    fnCutsceneFace("Countess", 1, 0)
                    fnCutsceneBlocker()
                    fnCutsceneWait(25)
                    fnCutsceneBlocker()
                    
                    --Fade in.
                    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
                    fnCutsceneBlocker()

                    --Movement.
                    fnCutsceneMove("Countess", 13.25, 14.50)
                    fnCutsceneMove("Countess", 10.25, 14.50)
                    fnCutsceneBlocker()
                    fnCutsceneWait(25)
                    fnCutsceneBlocker()
                    fnCutsceneFace("Nina", 1, 0)
                    fnCutsceneBlocker()
                    fnCutsceneWait(25)
                    fnCutsceneBlocker()
                    
                    --Dialogue.
                    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Good morning.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] What did you do for the night?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I stood watch over you as you slept, as instructed.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] No threats emerged.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] How many times did you repeat your mantra?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I was not counting.[SOFTBLOCK] Would you like me to count?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Give me an estimate.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Over one thousand.[SOFTBLOCK] I am a mannequin.[SOFTBLOCK] Loyalty undying.[SOFTBLOCK] Serve the master.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Over one thousand and one, then.[SOFTBLOCK] You will be good at keeping assassins from me.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] No harm will come to you, master.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Are you hungry?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] No, master.[SOFTBLOCK] I do not hunger.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Well, I am.[SOFTBLOCK] However, I can conjure my own meals.[SOFTBLOCK] At least until we have the estate fixed up and hire a cook.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Then again...[SOFTBLOCK] mannequin, are you a good cook?[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] No, master.[SOFTBLOCK] I can prepare basic meals.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Feh, no matter.[SOFTBLOCK] I will order you to practice later, and you will practice until I am satisfied.[SOFTBLOCK] Even if it takes a thousand hours.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[SOFTBLOCK] I will practice if you order me to.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I feel...[SOFTBLOCK] safe.[SOFTBLOCK] I feel safe with you near me, mannequin.[SOFTBLOCK] Thank you.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[SOFTBLOCK] Ngh...[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Heh heh ha, that will be your reward for keeping watch over me all night.[SOFTBLOCK] I suppose I have kept you long enough.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Mannequin, upstairs is my gallery.[SOFTBLOCK] It is where you triggered my gas trap, and were transformed.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] The artifacts I brought here are secured to the pedestals, but you should check them nonetheless.[SOFTBLOCK] If a brigand found a way to steal them, best if we go after them sooner than later.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] And while you're there...[SOFTBLOCK] dust them off for me.[SOFTBLOCK] I suppose you're my maid staff at the moment, as well.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[SOFTBLOCK] Your will be done.") ]])
                    fnCutsceneBlocker()
                    
                --Fourth sequence:
                elseif(iMannequinDialogue == 3.0) then
                    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Mannequin, I order that you speak freely.[SOFTBLOCK] Ask me whatever questions you like.[SOFTBLOCK] Let us converse.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[SOFTBLOCK] I will speak freely.[BLOCK][CLEAR]") ]])
                    fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "Countess") ]])
                end
            
            -- |[Stolen Runestone!]|
            elseif(iMannequinCompletedQuest == 1.0 and iSawMissingRunestone == 1.0 and iInformedMissingRunestone == 0.0) then
            
                -- |[Flag]|
                VM_SetVar("Root/Variables/NinaMod/MainQuest/iInformedMissingRunestone", "N", 1.0)
                VM_SetVar("Root/Variables/NinaMod/MainQuest/iStartedRuneRecovery", "N", 1.0)
                
                -- |[Dialogue]|
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Master, I have completed my task.[SOFTBLOCK] One of your treasures is missing.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] What![SOFTBLOCK] Which one!?[SOFTBLOCK] Who took it!?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] The small disk with a yellow marking.[SOFTBLOCK] I do not know who or what took it.[SOFTBLOCK] It was not on any of the bandits I defeated.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Hm, really?[SOFTBLOCK] The small disk?[SOFTBLOCK] Well, nothing to get worked up about.[SOFTBLOCK] It wasn't really mine.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I got it on the way here for a single platina.[SOFTBLOCK] Some beggar insisted I take it as a good luck charm.[SOFTBLOCK] Hasn't been any good luck if you ask me.[SOFTBLOCK] Unless...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Perhaps the luck was referring to you, mannequin?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] In that case, we need to get it back.[SOFTBLOCK] I wonder why they stole just that, though.[SOFTBLOCK] The magic I put on that pedestal was no more powerful on it than any of the others.[SOFTBLOCK] Surely the necklace would be easier to fence.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Mannequin.[SOFTBLOCK] You are hereby assigned to recover my lost treasure.[SOFTBLOCK] Are you able to track the thieves?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[SOFTBLOCK] I am used to tracking prey and humans alike.[SOFTBLOCK] I will recover it for you.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Splendid.[SOFTBLOCK] I -[SOFTBLOCK] uh, mannequin, I order you not to take any undue risks.[SOFTBLOCK] If it will cost you your life, do not risk injury.[SOFTBLOCK] Forget the task and return to me instead.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] You are worth more to me than a trinket.[SOFTBLOCK] Is that understood?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[SOFTBLOCK] I will not risk my life to recover the treasure.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Neither of us could possibly have known what was about to happen, of course.[SOFTBLOCK] The countess thought it was some unimportant tidbit, and she sent me after it more out of vengeance than a desire to hold the item.)[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Me, all I could think was the mantra.[SOFTBLOCK] I wasn't really considering the implications.[SOFTBLOCK] I didn't even wonder how the thieves had stolen it.[SOFTBLOCK] I thought only of my task.)[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Search the woods first, mannequin.[SOFTBLOCK] After that, I will leave it to you to track them down.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Teach them what happens when you steal from me, but don't kill them.[SOFTBLOCK] Leave them alive to tell the tale.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[SOFTBLOCK] Your will be done.") ]])
            
            -- |[Stolen Runestone Repeat]|
            elseif(iMannequinCompletedQuest == 1.0 and iSawMissingRunestone == 1.0 and iInformedMissingRunestone == 1.0) then
            
                -- |[Dialogue]|
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Did you want to discuss something with me, mannequin?[SOFTBLOCK] I suppose it will not give your quarry too much lead to talk for a few moments.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Activate Topics After Dialogue", "Countess") ]])
            end
        
        -- |[ ==================== Zombie Form =================== ]|
        elseif(sNinaForm == "Zombie") then
        
            --Variables.
            local iZombieBanditsDefeated = VM_GetVar("Root/Variables/NinaMod/MainQuest/iZombieBanditsDefeated", "N")
            local iZombieCompletedQuest  = VM_GetVar("Root/Variables/NinaMod/MainQuest/iZombieCompletedQuest", "N")
            
            --Quest not completed:
            if(iZombieCompletedQuest == 0.0 and iZombieBanditsDefeated < 3.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Was there something else I could do?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Don't worry, I'll have those goons gone before you know it.") ]])
            
            --First time completing quest.
            elseif(iZombieCompletedQuest == 0.0 and iZombieBanditsDefeated >= 3.0) then
                VM_SetVar("Root/Variables/NinaMod/MainQuest/iOverheardThugs", "N", 1.0)
                VM_SetVar("Root/Variables/NinaMod/MainQuest/iZombieCompletedQuest", "N", 1.0)
                VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawSwanky", "N", 1.0)
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Did you take care of those thugs?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yeah...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Is something wrong?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Just...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] They started pleading and begging and I didn't even want to eat them.[SOFTBLOCK] Just no dignity at all, you know?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I suppose that is what happens when you deal with such vermin.[SOFTBLOCK] I'd rather shrivel up than drink a rat's blood.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I hear ya.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] So, how about that kiss?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] [SOUND|Nina|Kiss][SOFTBLOCK]Mmm, you like?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] Woah, momma![SOFTBLOCK] If I had a pulse, it'd be quickening![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I suppose a reward is in order.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] The kiss was plenty![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I insist.[SOFTBLOCK] Please.[SOFTBLOCK] Follow me.") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Change rooms.
                fnCutsceneInstruction([[ AL_BeginTransitionTo("RuinsUpperA", "FORCEPOS:2.0x2.0x0") ]])
            
            end
        
        -- |[ ==================== Human Form ==================== ]|
        elseif(sNinaForm == "Human") then
        
            --Variables.
            local iHumanBanditsDefeated = VM_GetVar("Root/Variables/NinaMod/MainQuest/iHumanBanditsDefeated", "N")
            local iHumanCompletedQuest  = VM_GetVar("Root/Variables/NinaMod/MainQuest/iHumanCompletedQuest", "N")
            
            --Quest not completed:
            if(iHumanCompletedQuest == 0.0 and iHumanBanditsDefeated < 3.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Was there something else I could do?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Just stay here, I'll have those brigands dealt with in a moment.") ]])
            
            --First time completing quest.
            elseif(iHumanCompletedQuest == 0.0 and iHumanBanditsDefeated >= 3.0) then
                VM_SetVar("Root/Variables/NinaMod/MainQuest/iOverheardThugs", "N", 1.0)
                VM_SetVar("Root/Variables/NinaMod/MainQuest/iHumanCompletedQuest", "N", 1.0)
                VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawSwanky", "N", 1.0)
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
                fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] You're back![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] They barely even put up a fight![SOFTBLOCK] I don't think I broke a sweat.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] You're -[SOFTBLOCK] hurt![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Huh?[SOFTBLOCK] Oh it's just a scrape.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Nonsense, here.[SOFTBLOCK] Let me bandage you.[SOFTBLOCK] I know a little magic, I can heal the wound.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] You're a mage?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Certainly not![SOFTBLOCK] I just received a bit of magical education.[SOFTBLOCK] I'm not the sort to go throwing lightning around.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Otherwise I'd have driven off those ruffians myself.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Thanks.[SOFTBLOCK] The bleeding stopped in an instant.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Leave the bandage on for a while so nothing gets in the wound until it finishes healing.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] S-[SOFTBLOCK]so, would you like a tour of my estate, noble hero?[SOFTBLOCK] It's the least I can do.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] Do you have anything to eat?[SOFTBLOCK] I haven't eaten in days![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] [SOUND|World|MagicA]I can conjure a little bread.[SOFTBLOCK] It's not very good but - [BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] *chomp chomp*[SOFTBLOCK] Delicious![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Really?[SOFTBLOCK] Plain bread is delicious?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Spiced with three days of hunger, it's a gourmet meal.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Well, then.[SOFTBLOCK] I can cook a little.[SOFTBLOCK] Your reward for helping me shall be -[SOFTBLOCK] a feast![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] ...[SOFTBLOCK] After I have the estate restored, mind you.[SOFTBLOCK] But a feast it shall be.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Sounds good to me, I can wait as long as I have to.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Now, about that tour...") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                
                --Change rooms.
                fnCutsceneInstruction([[ AL_BeginTransitionTo("RuinsUpperA", "FORCEPOS:6.0x2.0x0") ]])
            
            end
        end
    end
end
