-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
-- |[MannequinIntro]|
if(sObjectName == "MannequinIntroW" or sObjectName == "MannequinIntroS") then

    -- |[Repeat Check]|
    local iMannequinMetMaster = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMannequinMetMaster", "N")
    if(iMannequinMetMaster == 1.0) then return end
    
    -- |[Activation Check]|
    local sNinaForm = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")
    if(sNinaForm ~= "Mannequin") then return end

    -- |[Flag]|
    VM_SetVar("Root/Variables/NinaMod/MainQuest/iMannequinMetMaster", "N", 1.0)
    
    -- |[Movement]|
    if(sObjectName == "MannequinIntroW") then
        fnCutsceneMove("Nina", 4.25, 14.50)
        fnCutsceneMove("Nina", 7.25, 14.50)
    else
        fnCutsceneMove("Nina", 10.25, 17.50)
        fnCutsceneMove("Nina", 10.25, 15.50)
    end
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Resting Action]|
    --Delete all KO data for bandits.
    AL_SetProperty("Wipe Destroyed Enemies")
    
    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
    if(sObjectName == "MannequinIntroW") then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] Halt![SOFTBLOCK] Who is there?[SOFTBLOCK] How did you know of my secret entrance?[BLOCK][CLEAR]") ]])
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] Halt![SOFTBLOCK] Who is there?[BLOCK][CLEAR]") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (The master was a slight woman of noble heritage, dressed in finery.[SOFTBLOCK] Her skin remained pale no matter how much time she spent in the sun.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Despite appearances, I would later learn she was not a vampire, but had been accused of it a few times.[SOFTBLOCK] Her skin was simply naturally pale, likely due to her bloodline.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (As she sat in the dingy basement reading, she let off a calm elegance.[SOFTBLOCK] Even if my will was my own, I would have been attracted to her.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Mannequin.[SOFTBLOCK] I am a mannequin.[SOFTBLOCK] I have come to serve.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] A mannequin?[SOFTBLOCK] Perhaps one of those louts triggered my trap.[SOFTBLOCK] Are you with those brigands?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] No, master.[SOFTBLOCK] I am Nina Avarro, a hunter from Tserro.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] Tserro?[SOFTBLOCK] You're a long way from home.[SOFTBLOCK] Well, I suppose this is your new home.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] Perhaps some good fortune has finally come.[SOFTBLOCK] I've never needed a slave quite as much as right now.[SOFTBLOCK] Are you a warrior?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[SOFTBLOCK] I have been hunting for over two decades.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] Have you ever killed a man?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] Good.[SOFTBLOCK] Not that guilt would be an issue.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I am a mannequin.[SOFTBLOCK] Loyalty undying.[SOFTBLOCK] Serve the master.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] That mantra is more potent than I had expected.[SOFTBLOCK] Mannequin, what do you think of my dress?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] It is beautiful, master.[SOFTBLOCK] It is perfect on you, and compliments your eyes.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] Interesting.[SOFTBLOCK] Mannequin, do you know any stories?[SOFTBLOCK] I am sleepy, and may want you to tuck me in to bed.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I know the stories of my village, master.[SOFTBLOCK] Should I list them for you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] Not right now.[SOFTBLOCK] Mannequin, what is your opinion on the coming war being Rondheim and Quantir?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I am a mannequin.[SOFTBLOCK] Loyalty undying.[SOFTBLOCK] Serve the master.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] Absolutely fascinating.[SOFTBLOCK] So the mind resorts to the mantra whenever it cannot think of what to say.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] I must remember to copy that down.[SOFTBLOCK] Then again, perhaps I should keep that tidbit to myself, lest another copy my methods.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] Well then, my mannequin.[SOFTBLOCK] I have a task for you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[SOFTBLOCK] Anything.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] There are some problems with invaders in my home.[SOFTBLOCK] Upstairs, some thieves have decided to help themselves to my belongings.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] And in the basement, some awful thing has happened.[SOFTBLOCK] I don't know why, but there are undead creatures there.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] We can figure out their cause later.[SOFTBLOCK] I want you to send the bandits packing, and destroy the undead.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] And try not to die in the process.[SOFTBLOCK] I need you for other things.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[SOFTBLOCK] Your will be done.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] I believe there was a bow left behind here when I moved in.[SOFTBLOCK] If you need it for your task, by all means.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] I suppose all of your things are my property now, aren't they?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] That was a rhetorical question.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I am a mannequin.[SOFTBLOCK] Loyalty undying.[SOFTBLOCK] Serve the master.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[E|Neutral] *sigh*[SOFTBLOCK] I suppose I ought not to rely on a slave to keep me company.[SOFTBLOCK] Mannequin, fulfill my orders.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.") ]])
    fnCutsceneBlocker()

-- |[ ======================================= Wait, Stay! ====================================== ]|
-- |[Wait Stay!]|
--Force player to stay and chat if the mannequin quest is completed.
elseif(sObjectName == "WaitStayN" or sObjectName == "WaitStayS") then

    -- |[Variables]|
    --Variables.
    local iMannequinCompletedQuest = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMannequinCompletedQuest", "N")
    local iMannequinDialogue       = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMannequinDialogue", "N")
    
    -- |[Ignore]|
    if(iMannequinCompletedQuest == 0.0) then return end
    if(iMannequinDialogue >= 3.0) then return end
    
    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Master:[VOICE|Countess] Mannequin, wait!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    --Where the player moves to is based on which trigger they activated.
    if(sObjectName == "WaitStayN") then
        fnCutsceneMove("Nina", 4.25, 14.50)
        fnCutsceneFace("Nina", 1, 0)
    else
        fnCutsceneMove("Nina", 9.25, 17.50)
        fnCutsceneFace("Nina", 0, -1)
    end
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] There's no hurry.[SOFTBLOCK] Let's...[SOFTBLOCK] talk for a moment.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.") ]])
    fnCutsceneBlocker()
    
-- |[Not This Way]|
--Prevents the player from going the wrong way as a mannequin.
elseif(sObjectName == "NotThisWayMannequin") then

    -- |[Variables]|
    --Variables.
    local iMannequinMetMaster = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMannequinMetMaster", "N")
    if(iMannequinMetMaster == 1.0) then return end
    
    -- |[Activation Check]|
    local sNinaForm = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")
    if(sNinaForm ~= "Mannequin") then return end
    
    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] This is the wrong way.[SOFTBLOCK] My master is to the west.") ]])
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Nina", 19.25, 25.50)
    
-- |[ ====================================== Undead Intro ====================================== ]|
elseif(sObjectName == "Undead") then

    -- |[Repeat Check]|
    local iSawUndead = VM_GetVar("Root/Variables/NinaMod/MainQuest/iSawUndead", "N")
    if(iSawUndead == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawUndead", "N", 1.0)
    
    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Hm, what's this?[SOFTBLOCK] Some sort of ruin?[SOFTBLOCK] Doesn't smell like a sewer.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] By the southern star, a -[SOFTBLOCK] skeleton![SOFTBLOCK] The undead![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] I -[SOFTBLOCK] I have an obligation to put the dead to rest, but how?[SOFTBLOCK] I've never seen one before...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] The priest might know what to do, but it's a long way back...[SOFTBLOCK] I have to take it into my own hands...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] A skeleton would be extremely resistant to my arrows, though.[SOFTBLOCK] Maybe I can sneak around them...") ]])

end
