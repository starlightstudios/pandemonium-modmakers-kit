-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
-- |[ ====================================== Examinables ======================================= ]|
if(sObjectName == "CountessDoor") then
    
    -- |[Variables]|
    local sNinaForm            = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")
    local iMetCountess         = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMetCountess", "N")
    local iStartedRuneRecovery = VM_GetVar("Root/Variables/NinaMod/MainQuest/iStartedRuneRecovery", "N")
    
    -- |[ ====================== Always Open ===================== ]|
    --If the runestone recovery quest is active, always opens. Opens if we've previously met the countess.
    if(iStartedRuneRecovery == 1.0 or iMetCountess == 1.0) then
        AudioManager_PlaySound("World|FlipSwitch")
        AL_SetProperty("Open Door", "CountessDoor")
        return
    
    -- |[ ==================== Mannequin Form ==================== ]|
    --Door is always open in mannequin form. Also, 
    elseif(sNinaForm == "Mannequin") then
        AudioManager_PlaySound("World|FlipSwitch")
        AL_SetProperty("Open Door", "CountessDoor")
        return
        
    -- |[ ====================== Human Form ====================== ]|
    elseif(sNinaForm == "Human") then
        
        --Variables
        local iGoAway = VM_GetVar("Root/Variables/NinaMod/MainQuest/iGoAway", "N")
    
        -- |[Quest Incomplete]|
        local iVanquishedUndead = VM_GetVar("Root/Variables/NinaMod/MainQuest/iVanquishedUndead", "N")
        if(iVanquishedUndead == 0.0) then
            if(iGoAway == 0.0) then
                VM_SetVar("Root/Variables/NinaMod/MainQuest/iGoAway", "N", 1.0)
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[VOICE|Nina] Locked.[SOFTBLOCK] Maybe I should knock?") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|Knock")
                fnCutsceneWait(65)
                fnCutsceneBlocker()
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Countess] Go away![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[VOICE|Nina] Hello?[SOFTBLOCK] Is there someone in there?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Countess] I'm not falling for your tricks![SOFTBLOCK] Leave![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[VOICE|Nina] Hey, I'm trying to help you here![SOFTBLOCK] There's skeletons out here![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Countess] Yes, I know.[SOFTBLOCK] Prove you're not one of them, and destroy them.[SOFTBLOCK] Otherwise, go away![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[VOICE|Nina] Sheesh, what a jerk.[SOFTBLOCK] Then again, maybe she thinks I'm tricking her.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[VOICE|Nina] I can't just leave someone in need, though.[SOFTBLOCK] I should help...") ]])
                fnCutsceneBlocker()
            else
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Countess] I said to go away![SOFTBLOCK] Off with you!") ]])
                fnCutsceneBlocker()
            end
            return
        end
    
        -- |[Quest Complete]|
        --Flags.
        VM_SetVar("Root/Variables/NinaMod/MainQuest/iMetCountess", "N", 1.0)
        VM_SetVar("Root/Variables/NinaMod/MainQuest/iRevealedStaircase", "N", 1.0)
        VM_SetVar("Root/Variables/NinaMod/MainQuest/iHasSeenFort", "N", 1.0)
        VM_SetVar("Root/Variables/NinaMod/MainQuest/iOverheardThugs", "N", 1.0)
        VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawSwanky", "N", 1.0)
        
        -- |[Movement]|
        fnCutsceneMove("Nina", 9.25, 21.50)
        fnCutsceneFace("Nina", 0, -1)
        fnCutsceneBlocker()
        
        -- |[Dialogue]|
        if(iGoAway == 0.0) then
            VM_SetVar("Root/Variables/NinaMod/MainQuest/iGoAway", "N", 1.0)
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[VOICE|Nina] Locked.[SOFTBLOCK] Maybe I should knock?") ]])
            fnCutsceneBlocker()
            fnCutsceneWait(25)
            fnCutsceneBlocker()
            fnCutscenePlaySound("World|Knock")
            fnCutsceneWait(65)
            fnCutsceneBlocker()
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Countess] Go away![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[VOICE|Nina] Hello?[SOFTBLOCK] Is there someone in there?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Countess] I'm not falling for your tricks![SOFTBLOCK] Leave![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[VOICE|Nina] What tricks?[SOFTBLOCK] Knocking is not a trick![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Countess] Prove you're not one of those skeletons, or I'll not let you in![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[VOICE|Nina] You mean the skeletons I destroyed?[SOFTBLOCK] Those skeletons?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Countess] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Countess] It does seem the malevolence in the air has vanished.[SOFTBLOCK] Very well.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Countess] [SOUND|World|FlipSwitch]It is unlocked, please come in.") ]])
            fnCutsceneBlocker()
        else
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[VOICE|Nina] Hey lady, I took care of those skeletons for you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Countess] It does seem the malevolence in the air has vanished.[SOFTBLOCK] Very well.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Countess] [SOUND|World|FlipSwitch]It is unlocked, please come in.") ]])
            fnCutsceneBlocker()
        end
        
        -- |[Movement]|
        fnCutsceneInstruction([[ AL_SetProperty("Open Door", "CountessDoor") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Respawn]|
        AL_SetProperty("Wipe Destroyed Enemies")
        
        -- |[Movement]|
        fnCutsceneMove("Countess", 10.25, 14.50)
        fnCutsceneMove("Countess",  9.25, 14.50)
        fnCutsceneFace("Countess", 0, 1)
        fnCutsceneMove("Nina", 9.25, 15.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Dialogue]|
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] H-[SOFTBLOCK]hi there![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Oh, I...[SOFTBLOCK] expected someone more...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] B-[SOFTBLOCK]burly.[SOFTBLOCK] Warrior-like.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] N-[SOFTBLOCK]nope, just me.[SOFTBLOCK] Elven archer, big ears.[SOFTBLOCK] Uh, got a bow here.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] It's a nice bow.[SOFTBLOCK] I like your...[SOFTBLOCK] cloak.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] That dress is really nice![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] So uh, I found a skull over there in a tomb.[SOFTBLOCK] I smashed it and look at that, the skeletons are gone.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Thank you kindly, hero.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I'm not a hero, just, you know, helping people in need.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Oh, uh, well I believe there was a bow in the storage room when I moved in.[SOFTBLOCK] Perhaps you could use it?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Thanks![SOFTBLOCK] Uh...[SOFTBLOCK] do you need anything...[SOFTBLOCK] else?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] No I couldn't impose on you any more...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Surely you need a strong hero to help you in some way.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I suppose you could drive off those irritating thieves upstairs.[SOFTBLOCK] They'll likely see themselves out when they see I have nothing to steal.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] Brigands![SOFTBLOCK] Of course I'll help you milady![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] My name's Nina, by the way.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Countess Yarla Sciawin, pleased to meet you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Is that a secret passage over there?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Yes, it will lead you to my courtyard.[SOFTBLOCK] Save you a bit of a walk.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] D-[SOFTBLOCK]don't get injured, now![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] No problem, I'll go vanquish those rogues![SOFTBLOCK] Be back soon![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] (She's hot, but I bet she doesn't like me...)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] (What a beautiful and kind woman.[SOFTBLOCK] A shame she probably can't stand someone like me...)") ]])

    -- |[ ===================== Zombie Form ====================== ]|
    elseif(sNinaForm == "Zombie") then
    
        -- |[Setup]|
        --If we've met the countess, open immediately.
        local iMetCountess = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMetCountess", "N")
        if(iMetCountess == 1.0) then
            AudioManager_PlaySound("World|FlipSwitch")
            AL_SetProperty("Open Door", "CountessDoor")
            return
        end
        
        --Variables
        local iGoAway = VM_GetVar("Root/Variables/NinaMod/MainQuest/iGoAway", "N")
        
        -- |[Quest Incomplete]|
        local iVanquishedUndead = VM_GetVar("Root/Variables/NinaMod/MainQuest/iVanquishedUndead", "N")
        if(iVanquishedUndead == 0.0) then
            if(iGoAway == 0.0) then
                VM_SetVar("Root/Variables/NinaMod/MainQuest/iGoAway", "N", 1.0)
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[VOICE|Nina] Locked.[SOFTBLOCK] Maybe I should knock?") ]])
                fnCutsceneBlocker()
                fnCutsceneWait(25)
                fnCutsceneBlocker()
                fnCutscenePlaySound("World|Knock")
                fnCutsceneWait(65)
                fnCutsceneBlocker()
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Countess] Go away![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[VOICE|Nina] Hello?[SOFTBLOCK] Is there someone in there?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Countess] I'm not falling for your tricks![SOFTBLOCK] Leave![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[VOICE|Nina] (Crud.[SOFTBLOCK] Gonna be hard to eat her if there's a door in the way.)[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[VOICE|Nina] Look, I'm trying to help you, lady.[SOFTBLOCK] There's skeletons all over out here![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Countess] Prove you're not one of them, then.[SOFTBLOCK] Destroy them, and I'll open the door.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[VOICE|Nina] Fine. I'll go see what's causing these layabouts.[SOFTBLOCK] I'll be back!") ]])
                fnCutsceneBlocker()
            else
                fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Countess] I said to go away![SOFTBLOCK] Off with you!") ]])
                fnCutsceneBlocker()
            end
            return
        end

        --Otherwise, do that scene.
        VM_SetVar("Root/Variables/NinaMod/MainQuest/iMetCountess", "N", 1.0)
        VM_SetVar("Root/Variables/NinaMod/MainQuest/iRevealedStaircase", "N", 1.0)

        -- |[Movement]|
        fnCutsceneMove("Nina", 9.25, 21.50)
        fnCutsceneFace("Nina", 0, -1)
        fnCutsceneBlocker()
        
        -- |[Dialogue]|
        fnCutscenePlaySound("World|Knock")
        fnCutsceneWait(65)
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        if(iGoAway == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[VOICE|Nina] Hello?[SOFTBLOCK] Is anyone in there?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Countess] Go away![SOFTBLOCK] I'll not open that door until the undead threat has been dealt with![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[VOICE|Nina] Well today is your lucky day, because I did that.[SOFTBLOCK] Smashed a skull and everything.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[VOICE|Nina] (Sucker![SOFTBLOCK] The undead threat is me!)[BLOCK][CLEAR]") ]])
        elseif(iGoAway == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[VOICE|Nina] Hey in there, I took care of the boners for you.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[VOICE|Nina] (Let me in so I can eat you!)[BLOCK][CLEAR]") ]])
        end
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Countess] You did?[SOFTBLOCK] I can feel the hatred in the air has lessened.[SOFTBLOCK] All right.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Voice:[VOICE|Countess] [SOUND|World|FlipSwitch]It is unlocked, please come in.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ AL_SetProperty("Open Door", "CountessDoor") ]])
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Respawn]|
        AL_SetProperty("Wipe Destroyed Enemies")
        
        -- |[Movement]|
        fnCutsceneMove("Countess", 10.25, 14.50)
        fnCutsceneMove("Countess",  9.25, 14.50)
        fnCutsceneFace("Countess", 0, 1)
        fnCutsceneMove("Nina", 9.25, 15.50)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Dialogue]|
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Surprise![SOFTBLOCK] Now I'm gonna eat you![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] You damned fool![SOFTBLOCK] You cannot eat me, I'm a vampire![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] (Please buy it, please buy it, please buy it!)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Seriously?[SOFTBLOCK] A vampire?[SOFTBLOCK] Argh![SOFTBLOCK] I can't believe it![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] (What a clod!)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Yes.[SOFTBLOCK] Pale skin, red eyes, could I be anything else?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Look I was going to bust in here, rip you apart, eat the bits...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] And I had intended to pull you in and drain every last drop of your blood.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Seems we tricked each other, didn't we?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yeah...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] You're pretty cute, though, so it's not a total loss.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Oh my, you think so?[SOFTBLOCK] I'm not sure if my vampiric charms work on a zombie.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Me neither.[SOFTBLOCK] I haven't been a zombie for long.[SOFTBLOCK] Is it like a thing you can turn on and off?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] No, those around me just succumb to my whims, falling in love with me.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Awesome![SOFTBLOCK] I'm starting to like being dead.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] But why are you in this ratty old sewer, then?[SOFTBLOCK] Aren't you supposed to be a powerful lady or something?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Indeed, this is my estate.[SOFTBLOCK] But some brigands, fancying themselves vampire hunters, have broken in.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] With preparations, my powers are useless against them, and my vampiric charm has no effect in broad daylight.[SOFTBLOCK] But they weren't expecting...[SOFTBLOCK] you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] You want me to kill them for you?[SOFTBLOCK] What's in it for me?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] ...[SOFTBLOCK] A kiss?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Tongue, or no tongue?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Whichever you like.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] Lady, I'd slaughter an army to make you smile.[SOFTBLOCK] A couple jerkoffs for a kiss is a real bargain.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Well then, I suppose that's a deal.[SOFTBLOCK] Oh, and I believe I had a bow in my storage room.[SOFTBLOCK] It's yours if you like.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Thanks![SOFTBLOCK] I'll go kill those morons, then.[SOFTBLOCK] Just up the stairs in the back?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Yes.[SOFTBLOCK] Please get rid of them.[SOFTBLOCK] If you choose to eat them or not, that's up to you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] (I couldn't believe my luck.[SOFTBLOCK] A beautiful vampire just happened to be un-living in the building, and she was prepared to give me a kiss.[SOFTBLOCK] All I had to do was a bit of casual murder.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] (At the time, I didn't realize it, but my inhibitions were gone.[SOFTBLOCK] That little voice telling me to keep my thoughts to myself, to consider others, all of that.[SOFTBLOCK] Gone.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] (She was beautiful, I wanted her, I was going to get her.[SOFTBLOCK] I wanted to eat, there were people, I would eat them.[SOFTBLOCK] Nothing would stand in my way.[SOFTBLOCK] Un-life was turning out a lot better than life ever had!)") ]])
        fnCutsceneBlocker()
    end
    
-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
