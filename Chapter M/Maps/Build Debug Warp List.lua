-- |[ ================================= Build Debug Warp List ================================== ]|
--Builds a list of locations the player can warp to. This is only used by the debug menu, as not all levels
-- are considered valid destinations for normal play. Some are used only for cutscenes, so only scripts can
-- warp there unless there is another reason to visit them.

-- |[Arguments]|
--Argument Listing:
-- 0: sChapterName - Name of the chapter to build a list for.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sChapterName = LM_GetScriptArgument(0)

-- |[Common]|
local zaListList = {}

-- |[ ====================== List Building ======================= ]|
--Baseline lists.
local saWoodsList = {"WoodsSA", "WoodsSB", "WoodsSC"}

--Add the lists to the master list.
zaListList[1] = {saWoodsList, "Woods South/"}

-- |[ ======================= List Upload ======================== ]|
-- |[Counting]|
--Iterate across the list of lists, counting all the entries:
local i = 1
local iGlobalCount = 0
while(zaListList[i] ~= nil) do
	
	--All members of this sublist have the second element appended before their name.
	local p = 1
	local saSublist = zaListList[i][1]
	while(saSublist[p] ~= nil) do
		iGlobalCount = iGlobalCount + 1
		p = p + 1
	end
	
	--Next.
	i = i + 1
	
end

-- |[Finalize List]|
--Set this as the total count.
ADebug_SetProperty("Warp Destinations Total", iGlobalCount)

--Re-iterate, except this time actually add them.
i = 1
iGlobalCount = 0
while(zaListList[i] ~= nil) do
	
	--All members of this sublist have the second element appended before their name.
	local p = 1
	local saSublist = zaListList[i][1]
	local sPrefix = zaListList[i][2]
	while(saSublist[p] ~= nil) do
		ADebug_SetProperty("Warp Destination", iGlobalCount, sPrefix .. saSublist[p])
		iGlobalCount = iGlobalCount + 1
		p = p + 1
	end
	
	--Next.
	i = i + 1
end