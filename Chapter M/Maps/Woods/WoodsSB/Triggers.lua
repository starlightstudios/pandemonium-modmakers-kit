-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "SeeFort") then

    -- |[Repeat Check]|
    local iHasSeenFort = VM_GetVar("Root/Variables/NinaMod/MainQuest/iHasSeenFort", "N")
    if(iHasSeenFort == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/NinaMod/MainQuest/iHasSeenFort", "N", 1.0)
    
    -- |[Movement]|
    fnCutsceneMove("Nina", 23.25, 13.50)
    fnCutsceneFace("Nina", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Nina", -1, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Nina", 1, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Nina", 0, -1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] A fort?[SOFTBLOCK] Here?[SOFTBLOCK] No, that can't be right.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I'd have seen it from the hills, surely.[SOFTBLOCK] I must be farther west than I thought.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] I don't see anyone around.[SOFTBLOCK] Maybe I could get something to eat if I...[SOFTBLOCK] asked nicely...") ]])
    fnCutsceneBlocker()
    

end
