-- |[ ===================================== Combat Victory ===================================== ]|
--The party won!
VM_SetVar("Root/Variables/NinaMod/MainQuest/iRecoveredRunestone", "N", 1.0)
    
-- |[Variables]|
local sNinaForm = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")

--Music change.
AudioManager_PlayMusic("Null")

-- |[Movement]|
--Wait a bit.
fnCutsceneWait(45)
fnCutsceneBlocker()

-- |[Mannequin Dialogue]|
if(sNinaForm == "Mannequin") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Leader", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Heh...[SOFTBLOCK] heh...[SOFTBLOCK] you're a real scrapper aren't you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Take the damn thing, if it's all you care about.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I should...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Huh?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I should...[SOFTBLOCK] KILL YOU.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] You dared threaten my master.[SOFTBLOCK] You robbed her estate.[SOFTBLOCK] You stole her things.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I should kill you and hang your flayed corpse in a gibbet to remind others to respect her.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Your crimes are unforgivable![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] I stole one little thing, and that's unforgivable?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] DIE![SOFTBLOCK] DIE![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Aahhhh!!!![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] ...[SOFTBLOCK] ?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] The master bade me not to kill you.[SOFTBLOCK] I was to merely injure you, leaving you alive to tell the tale.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Tell everyone that any who cross my master will live or die at the edge of her whims.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Fine![SOFTBLOCK] Of course![SOFTBLOCK] All for her, glory be![SOFTBLOCK] Just let me live![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Leave.[SOFTBLOCK] Now.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

-- |[Zombie Dialogue]|
elseif(sNinaForm == "Zombie") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Leader", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Heh...[SOFTBLOCK] go on, take it. Take my life![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] By the southern star, I have been waiting so long for -[SOFTBLOCK] *sniff*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] *sniff*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Ugh![SOFTBLOCK] I'm gonna puke![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Wh-[SOFTBLOCK]what![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] *bleagh*[SOFTBLOCK] Ugh, I gotta wash out my mouth![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] What's the matter with you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] What's the matter with me?[SOFTBLOCK] You smell like burnt bacon covered in sweaty grease and excrement![SOFTBLOCK] Do you never bathe?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] The water in the forest is free![SOFTBLOCK] You can take it home with you![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] I am literally a monster who eats rotten flesh and I don't want to eat you![SOFTBLOCK] Get out of my sight, and more importantly, my smell![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Aieee!!!") ]])
    fnCutsceneBlocker()

-- |[Human Dialogue]|
elseif(sNinaForm == "Human") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Leader", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Ugh...[SOFTBLOCK] You win, elf.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Yeah, I do![SOFTBLOCK] Hand it over![SOFTBLOCK] And maybe run for your life while you're at it![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Aieee!!!") ]])
    fnCutsceneBlocker()
end

-- |[Movement]|
fnCutsceneMove("Leader", 17.25, 30.50, 2.50)
fnCutsceneMove("Leader", 39.25, 30.50, 2.50)
fnCutsceneTeleport("Leader", -1.25, -1.50)
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()
fnCutsceneMove("Nina", 20.25, 28.50)
fnCutsceneMove("Nina", 20.25, 29.50)
fnCutsceneMove("Nina", 17.25, 29.50)
fnCutsceneMove("Nina", 17.25, 28.50)
fnCutsceneFace("Nina", 0, 1)
fnCutsceneBlocker()
fnCutsceneWait(25)
fnCutsceneBlocker()
fnCutscenePlaySound("World|TakeItem")
fnCutsceneBlocker()
fnCutsceneWait(65)
fnCutsceneBlocker()

-- |[Mannequin Dialogue]|
if(sNinaForm == "Mannequin") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] The master's treasure has been retrieved.[SOFTBLOCK] I must return it to the master.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...?") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
-- |[Zombie Dialogue]|
elseif(sNinaForm == "Zombie") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I'm starting to think I don't actually want to eat people.[SOFTBLOCK] Maybe fried, with a bit of spice, but raw?[SOFTBLOCK] No way.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Luckily I got this little rock back and -") ]])

-- |[Human Dialogue]|
elseif(sNinaForm == "Human") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] I was having such bad luck on the way over here, and now everything is coming up Nina![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] The countess will be so happy![SOFTBLOCK] I - [SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Woah![SOFTBLOCK] My head is spinning![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] The...[SOFTBLOCK] stone...[SOFTBLOCK] belongs in the gallery?[SOFTBLOCK] Yes, it does.[SOFTBLOCK] It really has to stay there.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Ugh.[SOFTBLOCK] What just happened?[SOFTBLOCK] Did I black out?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] It's like a minute passed and I just wasn't there for it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] I'll just get this back to the countess and then I can be done with it!") ]])
    return
end
    
-- |[Execute Transformation]|
--Wait a bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

--Flash the active character to white. Immediately after, execute the transformation.
Cutscene_CreateEvent("Flash Nina White", "Actor")
	ActorEvent_SetProperty("Subject Name", "Nina")
	ActorEvent_SetProperty("Flashwhite Quickly")
DL_PopActiveObject()
fnCutsceneWait(5)
fnCutsceneBlocker()
fnCutsceneInstruction([[ LM_ExecuteScript(gsNinaRoot .. "FormHandlers/Nina/Form_Human.lua") ]])
fnCutsceneWait(gci_Flashwhite_Ticks_Total)
fnCutsceneBlocker()

--Now wait a little bit.
fnCutsceneWait(30)
fnCutsceneBlocker()

-- |[Mannequin Dialogue]|
if(sNinaForm == "Mannequin") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] I -[SOFTBLOCK] I'm![SOFTBLOCK] I'm refleshed![SOFTBLOCK] I'm a human again![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] The runestone must have done this and -[SOFTBLOCK] wait...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] How...[SOFTBLOCK] how do I know it did that?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] (It was as if I had already known, like someone had told me long ago as a bedtime story and I had simply forgotten.[SOFTBLOCK] In that moment, I remembered the story.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] (I could transform if I held the runestone to me, into any form I had previously.[SOFTBLOCK] It would protect me if I was hurt, and many other things.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] (I didn't know how I could know that, but it was so obviously true.[SOFTBLOCK] Like knowing the sky was blue, or that water was wet.[SOFTBLOCK] It was without saying.[SOFTBLOCK] Something more primitive than knowledge.[SOFTBLOCK] Instinct buried deep in me.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I'm -[SOFTBLOCK] so confused.[SOFTBLOCK] Why didn't it do that for anyone else?[SOFTBLOCK] Has nobody ever held it before?[SOFTBLOCK] Only humans?[SOFTBLOCK] So many questions...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Maybe master will know![SOFTBLOCK] I - [BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] M-[SOFTBLOCK]master.[SOFTBLOCK] I'm not a mannequin anymore.[SOFTBLOCK] I could just...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] She turned me into a mannequin and treated me like her slave![SOFTBLOCK] Typical![SOFTBLOCK] I should - [BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] But she also meant it as a trap for thieves, and that was me.[SOFTBLOCK] I shouldn't have broken in.[SOFTBLOCK] I - [BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] No![SOFTBLOCK] It's unforgivable![SOFTBLOCK] To enslave someone like that![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] But she treated me really nice and she clearly likes me.[SOFTBLOCK] I like her too![SOFTBLOCK] I should - [BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Ahhhhh![SOFTBLOCK] So many thoughts![SOFTBLOCK] So many - [BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I am a mannequin.[SOFTBLOCK] Loyalty undying.[SOFTBLOCK] Serve the master.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] It's still there, but it's just in the background.[SOFTBLOCK] Thrumming along in the back of my head.[SOFTBLOCK] It's so nice.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Okay, calm down.[SOFTBLOCK] I'll go back to -[SOFTBLOCK] the countess.[SOFTBLOCK] Talk it over with her.[SOFTBLOCK] It is her runestone, after all.[SOFTBLOCK] I'm a lot of things, but I'm not going to be a thief.[SOFTBLOCK] Not after all that's happened.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] M-[SOFTBLOCK]maybe she'll hire me as a guard, or a hunter, and I can get a steady meal and a place to sleep![SOFTBLOCK] There's worse fates than working for a noble, especially one that's actually kind...") ]])
    fnCutsceneBlocker()

-- |[Zombie Dialogue]|
elseif(sNinaForm == "Zombie") then
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I -[SOFTBLOCK] I'm alive![SOFTBLOCK] This sucks![SOFTBLOCK] Stupid rock![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Is it because I keep letting people go instead of slaughtering them?[SOFTBLOCK] Give me another chance![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Ngh...[SOFTBLOCK] my head...[SOFTBLOCK] This thing...[SOFTBLOCK] transformed me...[SOFTBLOCK] how?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] (It was as if I had already known, like someone had told me long ago as a bedtime story and I had simply forgotten.[SOFTBLOCK] In that moment, I remembered the story.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] (I could transform if I held the runestone to me, into any form I had previously.[SOFTBLOCK] It would protect me if I was hurt, and many other things.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] (I didn't know how I could know that, but it was so obviously true.[SOFTBLOCK] Like knowing the sky was blue, or that water was wet.[SOFTBLOCK] It was without saying.[SOFTBLOCK] Something more primitive than knowledge.[SOFTBLOCK] Instinct buried deep in me.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Okay, calm down.[SOFTBLOCK] I can turn back into a zombie later.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] Or a vampire![SOFTBLOCK] That'd be - [BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Oh who am I kidding.[SOFTBLOCK] The countess wasn't a vampire, she smelled exactly like a human...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[SOFTBLOCK] Maybe we could go find a vampire who'd be willing to bite us and not be a jerk about it?[SOFTBLOCK] Where do you go to find that?[SOFTBLOCK] Put up a poster?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I guess I should at least go give the countess her rock back.[SOFTBLOCK] Maybe she needs an undead eternal guard to hang around her place and...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Having inhibitions sucks![SOFTBLOCK] I want to tongue her vagina![SOFTBLOCK] Stop it you stupid living brain![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Zombie Nina is way more fun to be and be around, you haven't heard the last of the undead, runestone!") ]])
end
