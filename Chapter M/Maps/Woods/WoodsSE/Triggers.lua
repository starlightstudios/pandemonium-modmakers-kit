-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
-- |[Confrontation]|
if(sObjectName == "Confrontation") then

    -- |[Repeat Check]|
    local iRecoveredRunestone = VM_GetVar("Root/Variables/NinaMod/MainQuest/iRecoveredRunestone", "N")
    if(iRecoveredRunestone == 1.0) then return end
    
    -- |[Variables]|
    local sNinaForm = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")

    -- |[Movement]|
    fnCutsceneMove("Nina", 22.25, 28.50)
    fnCutsceneFace("Nina", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Mannequin Dialogue]|
    if(sNinaForm == "Mannequin") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Thief", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Warrior", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thief:[E|Neutral] Oh -[SOFTBLOCK] oh crap![SOFTBLOCK] Oh crap![SOFTBLOCK] She found us![SOFTBLOCK] The wax girl found us![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I am not wax.[SOFTBLOCK] I am resin.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Warrior:[E|Neutral] R-[SOFTBLOCK]r-[SOFTBLOCK]run![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] You have stolen a treasure from my master.[SOFTBLOCK] Return it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thief:[E|Neutral] Wait, no we didn't![SOFTBLOCK] I didn't steal anything![SOFTBLOCK] I put it all back![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Warrior:[E|Neutral] I left it all behind![SOFTBLOCK] Please don't hurt us![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Return my master's treasure, or I will kill you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Warrior:[E|Neutral] B-[SOFTBLOCK]b-[SOFTBLOCK]but[SOFTBLOCK]") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
    -- |[Zombie Dialogue]|
    elseif(sNinaForm == "Zombie") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Thief", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Warrior", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] Hello, boys![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thief:[E|Neutral] Oh no, I just got my not-getting-eaten socks on![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Not this again, shut up.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Warrior:[E|Neutral] She's gonna eat us![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Look you jackoffs, you stole a disk from the estate.[SOFTBLOCK] Give it back and I won't eat you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Might kill you but that's pleasure, not business...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    -- |[Human Dialogue]|
    elseif(sNinaForm == "Human") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Thief", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Warrior", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Hey I need that - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thief:[E|Neutral] HEEEEELP![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I just want back what - [SOFTBLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Warrior:[E|Neutral] LET US LIVE PLEEEASE![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] I'll let you live if you give back the thing you stole!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    end
    
    -- |[Movement]|
    fnCutsceneMove("Leader", 17.25, 28.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Thief", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Warrior", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 6, "Leader", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Make yourselves scarce, idiots.[SOFTBLOCK] Or are you too cowardly even to run?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Warrior:[E|Neutral] Don't gotta tell me twice![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Thief:[E|Neutral] Toodaloo!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Warrior", 39.25, 27.50, 2.50)
    fnCutsceneTeleport("Warrior", -1.25, -1.50)
    fnCutsceneMove("Thief", 18.25, 29.50, 2.50)
    fnCutsceneMove("Thief", 39.25, 29.50, 2.50)
    fnCutsceneTeleport("Thief", -1.25, -1.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    
    -- |[Mannequin Dialogue]|
    if(sNinaForm == "Mannequin") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Leader", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] I'd like to apologize on their behalf, madam.[SOFTBLOCK] They're very brave when they have the advantage.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] I don't want to offend their delicate sensibilities, you see, as I tear you apart piece by piece.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Do you possess the master's stone disk?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] In fact, I do.[SOFTBLOCK] Took a lot of work to get it off that pedestal, let me tell you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Return it, or die.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Do you know what it is?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Return it.[SOFTBLOCK] Or die.[SOFTBLOCK] Now.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] You don't know what it is, you probably don't have a thought in your head.[SOFTBLOCK] Just a dumb automaton.[SOFTBLOCK] No use arguing with you.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] What.[SOFTBLOCK] Is the stone disk?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Oh, so there is something in there?[SOFTBLOCK] A stray thought?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] What is the stone disk?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Something more valuable than any amount of money or land.[SOFTBLOCK] Something that can be put to better use than to rot in some noble's gallery.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Dark forces swirl even now at the edges of your vision.[SOFTBLOCK] They come to us, coat the land, swallow its people.[SOFTBLOCK] Decay will set in.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] And they want this stone.[SOFTBLOCK] I'm going to give it to them.[SOFTBLOCK] Can't beat 'em, join 'em, right?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] You want in on this, resin girl?[SOFTBLOCK] Abandon your master, come serve me.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Never.[SOFTBLOCK] I will kill you and return my master's treasure.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I am a mannequin.[SOFTBLOCK] Loyalty undying.[SOFTBLOCK] You are a cur.[SOFTBLOCK] Prepare yourself.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Have it your way.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
    -- |[Zombie Dialogue]|
    elseif(sNinaForm == "Zombie") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Leader", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] I'd like to apologize on their behalf, madam.[SOFTBLOCK] They're very brave when they have the advantage.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Uh huh.[SOFTBLOCK] You got that disk thingy?[SOFTBLOCK] It's not mine but it's certainly not yours.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Are you a thief as well?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] All I steal is hearts, baby.[SOFTBLOCK] Figuratively...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] AND LITERALLY![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] I don't get it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] I'm going to pull your heart out of your chest, and then eat it.[SOFTBLOCK] Ideally while it's still beating.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] I still don't get it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Look, why do you want that rock so badly you're prepared to get killed, by me, to have it?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] It is no mere rock.[SOFTBLOCK] This rune is a symbol of great things to come, and it is wanted by dark forces more powerful than you can imagine.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Dark forces?[SOFTBLOCK] Like, evil?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] I intend to give them this valuable stone, and join them.[SOFTBLOCK] They will win in the end, and I wish to be on the winning side.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] That sounds extremely tempting.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] These 'dark forces' of yours.[SOFTBLOCK] Tell me, are they cute?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] I -[SOFTBLOCK] I wouldn't say so.[SOFTBLOCK] I've seen visions of eyes larger than the moon, staring at me, shredding my skin with their gaze.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Gonna have to pass on that one, because I'm also a member of some 'dark forces' and I might get another kiss if I bring that rock back.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] When you get to hell, tell your dark friends that sex sells.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] A fight it is, then!") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    -- |[Human Dialogue]|
    elseif(sNinaForm == "Human") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Leader", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] I'd like to apologize on their behalf, madam.[SOFTBLOCK] They're very brave when they have the advantage.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] So you're in charge, are you.[SOFTBLOCK] Do you have a small disk with a symbol on it?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Indeed, I do.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Goody.[SOFTBLOCK] Return it and I won't give you a new piercing.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Aren't you curious why I stole that treasure instead of the others?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Is it because you couldn't figure out how to get the others off the pedestals?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] No no, in fact, this one was the most secured of them all.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] This stone is an artifact of unimaginable power, wanted by beings beyond our comprehension.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Do you have a small dick or something?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Yes, and I'm not ashamed of it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Just asking.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Look, I don't care what stupid story you have, just give it back and I won't need to rough you up.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] That won't be happening.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Then I'll be roughing you up, then.") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
    
    end
    
    -- |[Battle!]|
    fnCutsceneInstruction([[ AdvCombat_SetProperty("World Pulse", true) ]])
    fnCutsceneInstruction([[ AdvCombat_SetProperty("Reinitialize") ]])
    fnCutsceneInstruction([[ AdvCombat_SetProperty("Activate") ]])
    fnCutsceneInstruction([[ AdvCombat_SetProperty("Unretreatable", true) ]])
    fnCutsceneInstruction([[ AdvCombat_SetProperty("Victory Script", gsNinaRoot .. "Maps/Woods/WoodsSE/Combat_Victory.lua") ]])
    fnCutsceneInstruction([[ AdvCombat_SetProperty("Defeat Script",  gsStandardGameOver) ]])
    fnCutsceneInstruction([[ LM_ExecuteScript(gsNinaRoot .. "Combat/Enemies/Enemy List/000 Scripted Enemies/Boss Bandit Leader.lua") ]])
    fnCutsceneBlocker()

end
