-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
-- |[Opening]|
if(sObjectName == "Opening") then

    -- |[Repeat Check]|
    local iHasSeenOpening = VM_GetVar("Root/Variables/NinaMod/MainQuest/iHasSeenOpening", "N")
    if(iHasSeenOpening == 1.0) then return end
    
    -- |[Flags]|
    VM_SetVar("Root/Variables/NinaMod/MainQuest/iHasSeenOpening", "N", 1)
    
    --Create Nina's starting loadout.
    LM_ExecuteScript(gsItemListing, "Cypress Bow")
    LM_ExecuteScript(gsItemListing, "Hunter's Clothes")
    LM_ExecuteScript(gsItemListing, "Bow Sight")
    
    --Make her equip the items.
    AdvCombat_SetProperty("Push Party Member", "Nina")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Weapon",      "Cypress Bow")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Body",        "Hunter's Clothes")
        AdvCombatEntity_SetProperty("Equip Item To Slot", "Accessory A", "Bow Sight")
    DL_PopActiveObject()
    
    -- |[Fading]|
    --Instant cut to black.
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 1, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneWait(45)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (My name is Nina Avarro.[SOFTBLOCK] I'm a 45 year old woman from the eastern edge of the continent now called Arulenta.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] (If you're wondering how I look so lovely at the tender age of 45, well, we elves don't show our ages past our 20's.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] (It makes a lot of people think that we live forever, or at least a long time.[SOFTBLOCK] Not so.[SOFTBLOCK] We just don't show it.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (But that wasn't my concern at this time.[SOFTBLOCK] I was always hungry.[SOFTBLOCK] Eastern Arulenta was wild at the time.[SOFTBLOCK] Vast forests, no farms.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (You either hunted, or fished, or swore fealty to someone with an army and farmed the land for them someplace to the north.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] (If you're born under the stars, hunger is the price you pay for freedom.[SOFTBLOCK] That's what I think, anyway.[SOFTBLOCK] A lot of my contemporaries disagree.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (But that's neither here nor there.[SOFTBLOCK] Let me tell you about something that happened to me once, when I was far from my normal hunting grounds...)") ]])
    fnCutsceneBlocker()

-- |[The Hunt]|
elseif(sObjectName == "TheHunt") then

    -- |[Repeat Check]|
    local iSawTheHunt = VM_GetVar("Root/Variables/NinaMod/MainQuest/iSawTheHunt", "N")
    if(iSawTheHunt == 1.0) then return end

    -- |[Activation Check]|
    --Doesn't activate after getting the order to find the bandits.
    local iHumanSawMissingRunestone  = VM_GetVar("Root/Variables/NinaMod/MainQuest/iHumanSawMissingRunestone", "N")
    local iInformedMissingRunestone  = VM_GetVar("Root/Variables/NinaMod/MainQuest/iInformedMissingRunestone", "N")
    local iZombieSawMissingRunestone = VM_GetVar("Root/Variables/NinaMod/MainQuest/iZombieSawMissingRunestone", "N")
    
    -- |[Human]|
    if(iHumanSawMissingRunestone == 1.0) then
        
        -- |[Flag]|
        VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawTheHunt", "N", 1.0)
        
        -- |[Dialogue]|
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Judging by the disturbed soft earth and the ripples in the forest's mana, those guys went south.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] They didn't even bother to cover their tracks.[SOFTBLOCK] Amatuers![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Just hope this doesn't take too long.[SOFTBLOCK] My stomach is rumbling again already.") ]])
        fnCutsceneBlocker()
        
    -- |[Mannequin]|
    elseif(iInformedMissingRunestone == 1.0) then
        
        -- |[Flag]|
        VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawTheHunt", "N", 1.0)
        
        -- |[Dialogue]|
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] The humans fled to the south.[SOFTBLOCK] They did not cover their tracks, nor scrub their magical wake.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] They cannot be far.[SOFTBLOCK] I will retrieve my master's treasure.") ]])
        fnCutsceneBlocker()
    
    -- |[Zombie]|
    elseif(iZombieSawMissingRunestone == 1.0) then
        
        -- |[Flag]|
        VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawTheHunt", "N", 1.0)
        
        -- |[Dialogue]|
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Sheesh, I don't even need my tracking skills, I can practically smell that the humans went south.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] Can't have gotten far.[SOFTBLOCK] This will be easy!") ]])
        fnCutsceneBlocker()
    end
    
-- |[Walkback]|
elseif(sObjectName == "NotThisWay") then

    -- |[Variables]|
    local sNinaForm = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")
    local iRecoveredRunestone = VM_GetVar("Root/Variables/NinaMod/MainQuest/iRecoveredRunestone", "N")

    -- |[Runestone Recovered, Ignore]|
    if(iRecoveredRunestone == 1.0) then return end

    -- |[Human Form]|
    if(sNinaForm == "Human") then
        
        -- |[Variables]|
        local iHumanSawMissingRunestone = VM_GetVar("Root/Variables/NinaMod/MainQuest/iHumanSawMissingRunestone", "N")
        if(iHumanSawMissingRunestone == 1.0) then return end
        
        -- |[Dialogue]|
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] That's the way I came to get here.[SOFTBLOCK] Didn't find a single thing, not even a mouse.[SOFTBLOCK] Terrible hunting grounds.") ]])
        fnCutsceneBlocker()
        
        -- |[Movement]|
        fnCutsceneMove("Nina", 23.75, 22.50)
        fnCutsceneFace("Nina", 0, -1)
        
    -- |[Mannequin]|
    elseif(sNinaForm == "Mannequin") then
    
        -- |[Activation Check]|
        --Doesn't activate after getting the order to find the bandits.
        local iInformedMissingRunestone = VM_GetVar("Root/Variables/NinaMod/MainQuest/iInformedMissingRunestone", "N")
        if(iInformedMissingRunestone == 1.0) then return end
        
        -- |[Dialogue]|
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I must see to the master.[SOFTBLOCK] This way leads deeper into the forest, and she has not ordered me away.") ]])
        fnCutsceneBlocker()
        
        -- |[Movement]|
        fnCutsceneMove("Nina", 23.75, 22.50)
        fnCutsceneFace("Nina", 0, -1)
    
    -- |[Zombie]|
    elseif(sNinaForm == "Zombie") then
    
        -- |[Activation Check]|
        --Doesn't activate after getting the order to find the bandits.
        local iZombieSawMissingRunestone = VM_GetVar("Root/Variables/NinaMod/MainQuest/iZombieSawMissingRunestone", "N")
        if(iZombieSawMissingRunestone == 1.0) then return end
        
        -- |[Dialogue]|
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I should probably deal with the humans in that estate over there before I run off into the woods.") ]])
        fnCutsceneBlocker()
        
        -- |[Movement]|
        fnCutsceneMove("Nina", 23.75, 22.50)
        fnCutsceneFace("Nina", 0, -1)
    end

end
