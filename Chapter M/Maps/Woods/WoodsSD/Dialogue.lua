-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[ ======================= Merchant ======================= ]|
    if(sActorName == "Merchant") then
        
        -- |[Variables]|
        local sNinaForm        = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")
        local sFormMetMerchant = VM_GetVar("Root/Variables/NinaMod/Merchant/sFormMetMerchant", "S")
        
        -- |[Common Code]|
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Merchant", "Neutral") ]])
        
        -- |[ ========= First Meeting ======== ]|
        if(sFormMetMerchant == "Null") then
        
            --Store Nina's current form.
            VM_SetVar("Root/Variables/NinaMod/Merchant/sFormMetMerchant", "S", sNinaForm)
            
            -- |[Human]|
            if(sNinaForm == "Human") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] A merchant?[SOFTBLOCK]  Way out here?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Well hello there.[SOFTBLOCK]  I was just on my way to town.[SOFTBLOCK]  Are you new in the area?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] You could say that.[SOFTBLOCK]  Are you travelling alone?[SOFTBLOCK]  Isn't that dangerous?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] I used to be a soldier, and guards cost money.[SOFTBLOCK]  Trim those margins![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] If you say so.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Did you see any dumb guys going by here?[SOFTBLOCK]  Really dumb?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Like eating bricks dumb?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] That's them![SOFTBLOCK]  Which way did they go?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Just south of here.[SOFTBLOCK]  They didn't buy anything.[SOFTBLOCK]  Cheapskates.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Let me see what you have for sale, then.[SOFTBLOCK]  I can at least browse.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Sure.[SOFTBLOCK] Take a look.") ]])
                fnCutsceneBlocker()

                --Run the shop.
                local sBasePath = fnResolvePath()
                local sString = "AM_SetShopProperty(\"Show\", \"Travelling Merchant\", \"" .. sBasePath .. "Merchant.lua\", \"Null\")"
                fnCutsceneInstruction(sString)
                fnCutsceneBlocker()

            -- |[Mannequin]|
            elseif(sNinaForm == "Mannequin") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Holy smokes![SOFTBLOCK] What are you?[SOFTBLOCK] What's your deal?[SOFTBLOCK] What are you doing?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I am a mannequin.[SOFTBLOCK] Loyalty undying.[SOFTBLOCK] Serve the master.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Oh.[SOFTBLOCK] Well I guess that does answer my questions.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Have you seen any thieves go by recently?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Hey, I'm not supposed to just go ratting on anyone who comes by.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] But they also didn't buy anything so taff them.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Where are they?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Just south of here.[SOFTBLOCK] But you didn't hear it from me.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] You got any money, mannequin?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] My money belongs to my master.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] And would you spend your master's money to buy things for your master?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Well perhaps you'd like to take a look...") ]])
                fnCutsceneBlocker()

                --Run the shop.
                local sBasePath = fnResolvePath()
                local sString = "AM_SetShopProperty(\"Show\", \"Travelling Merchant\", \"" .. sBasePath .. "Merchant.lua\", \"Null\")"
                fnCutsceneInstruction(sString)
                fnCutsceneBlocker()

            -- |[Zombie]|
            elseif(sNinaForm == "Zombie") then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Looking good, human![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Same to you...[SOFTBLOCK] zombie?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Banshee, zombie, something to that effect.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Where'd you get that shirt?[SOFTBLOCK] It looks great on you![BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] This kind of outfit costs only a few platina in the city.[SOFTBLOCK] I go there often.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Neat![SOFTBLOCK] Why do you go there?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] I'm a travelling merchant.[SOFTBLOCK] If I wasn't wearing this shirt, I'd love to sell it to you.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I'm not sure the blue would work with my skin...[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Hey, did you see any common criminals go by here recently?[SOFTBLOCK] Possibly running in terror?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] In fact, I did.[SOFTBLOCK] They didn't buy anything.[SOFTBLOCK] They're just south of here.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Want me to kill them for you?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Look, if you want to kill them, be my guest, but I'm not getting involved.[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Nuts.[SOFTBLOCK] You got anything for sale I might use for aforementioned murder?[BLOCK][CLEAR]") ]])
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Well perhaps you'd like to take a look...") ]])
                fnCutsceneBlocker()

                --Run the shop.
                local sBasePath = fnResolvePath()
                local sString = "AM_SetShopProperty(\"Show\", \"Travelling Merchant\", \"" .. sBasePath .. "Merchant.lua\", \"Null\")"
                fnCutsceneInstruction(sString)
                fnCutsceneBlocker()
        
            end
        
        -- |[ ======== Second Meeting ======== ]|
        else
        
            -- |[Variables]|
            local iNoRepeats = VM_GetVar("Root/Variables/NinaMod/Merchant/iNoRepeats", "N")
        
            -- |[Skip All Scenes]|
            if(iNoRepeats == 1.0) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Looking to buy?") ]])
                fnCutsceneBlocker()
                local sBasePath = fnResolvePath()
                local sString = "AM_SetShopProperty(\"Show\", \"Travelling Merchant\", \"" .. sBasePath .. "Merchant.lua\", \"Null\")"
                fnCutsceneInstruction(sString)
                fnCutsceneBlocker()
        
            -- |[Form is the same as First Meeting]|
            elseif(sNinaForm == sFormMetMerchant) then
                fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Come for some more shopping?") ]])
                fnCutsceneBlocker()
                local sBasePath = fnResolvePath()
                local sString = "AM_SetShopProperty(\"Show\", \"Travelling Merchant\", \"" .. sBasePath .. "Merchant.lua\", \"Null\")"
                fnCutsceneInstruction(sString)
                fnCutsceneBlocker()
        
            -- |[Different]|
            else
        
                -- |[Now a Human]|
                if(sNinaForm == "Human") then
                    
                    --Flag.
                    VM_SetVar("Root/Variables/NinaMod/Merchant/iNoRepeats", "N", 1.0)
                    
                    --Dialogue.
                    if(sFormMetMerchant == "Mannequin") then
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Hey you're -[SOFTBLOCK] oh.[SOFTBLOCK] Sorry.[SOFTBLOCK] Thought you were someone else.[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] You didn't rough up a wax girl and steal her clothes, did you?[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Really?[SOFTBLOCK] Really?[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Sorry, I'm not accusing you of - [SOFTBLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Resin![SOFTBLOCK] Not wax![SOFTBLOCK] Why does everything think I'm wax?[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Jeez.[SOFTBLOCK] Wait maybe I shouldn't just tell random people about this...[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Did you eat some of the red berries, buddy?[SOFTBLOCK] They make you see stuff.[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Ah, no.[SOFTBLOCK] No berries.[SOFTBLOCK] It's just a weird coincidence that I look exactly like the mannequin you saw a few minutes ago.[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] It is?[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Yep![SOFTBLOCK] I saw her and was like, [SOFTBLOCK]'You dress *sharp*, madam!'[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] And she sort of looked at me and was said [SOFTBLOCK]'I am a mannequin,'[SOFTBLOCK] and walked away.[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] That sounds like her all right.[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] So are you a merchant?[SOFTBLOCK] Got anything for sale?[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Of course![SOFTBLOCK] Take a look.") ]])
                        fnCutsceneBlocker()

                    --Zombie:
                    elseif(sFormMetMerchant == "Zombie") then
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Hey man you didn't miss [SOFTBLOCK]*anything*[SOFTBLOCK] down there.[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Do I know you?[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I used to be a zombie.[SOFTBLOCK] Now I'm not.[SOFTBLOCK] It sucks.[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] How the hell did that happen?[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Magic.[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Screw magic![BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Tell me about it![BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Can you go turn back into a zombie?[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I think so, but it still sucks.[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] What even is a lung, right?[SOFTBLOCK] Just a big bladder full of air, who needs it![BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Oh no, what if I start bleeding, might die![SOFTBLOCK] You know who won't?[SOFTBLOCK] A zombie![BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Joke's on you, I can't die if I'm already dead![BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Anyway, you got anything for sale?[BLOCK][CLEAR]") ]])
                        fnCutsceneInstruction([[ WD_SetProperty("Append", "Merchant:[E|Neutral] Yeah, of course.[SOFTBLOCK] Take a look.") ]])
                        fnCutsceneBlocker()
                    end
                
                    --Run the shop.
                    local sBasePath = fnResolvePath()
                    local sString = "AM_SetShopProperty(\"Show\", \"Travelling Merchant\", \"" .. sBasePath .. "Merchant.lua\", \"Null\")"
                    fnCutsceneInstruction(sString)
                    fnCutsceneBlocker()
                end
            end
        
        end
    end
end
