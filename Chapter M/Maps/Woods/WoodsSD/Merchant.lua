-- |[ =================================== Merchant Shop Setup ================================== ]|
--Stuff the merchant stocks.

-- |[Item Stock Listing]|
--Adamantite
AM_SetShopProperty("Add Item", "Cypress Bow", -1, -1)
AM_SetShopProperty("Add Item", "Leather Cloak", -1, -1)
AM_SetShopProperty("Add Item", "Swamp Boots", -1, -1)
AM_SetShopProperty("Add Item", "Healing Tincture", -1, -1)
AM_SetShopProperty("Add Item", "Flash Powder", -1, -1)
