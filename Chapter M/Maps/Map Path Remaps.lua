-- |[ ==================================== Map Path Remaps ===================================== ]|
--Called at program startup if this mod is detected. Adds additional map path remaps atop the ones
-- that already should exist in the main game.

-- |[Mod Activity Check]|
--If the mod is not active, don't do any of the below.
if(OM_IsModActive("Nina's Adventure") == false) then return end

-- |[Execution]|
--Setup.
local sRootPath = fnTrimLastDirectory(fnResolvePath())
local sLastDir = fnGetLastDirectory(sRootPath)

--Functions.
if(fnAddRemapping == nil) then
    local sBasePath = fnResolvePath()
    LM_ExecuteScript(gsRoot .. "Maps/fnAddRemapping.lua")
    LM_ExecuteScript(gsRoot .. "Maps/fnAddRemapsPattern.lua")
end

--Internal mod. Standard layout, mod is in a chapter folder with the other chapters.
local saNinaModList = {}
if(gbModIsExternal ~= true) then
    fnAddRemapping(saNinaModList, "WoodsSA",        "../" .. sLastDir .. "/Maps/Woods/WoodsSA")
    fnAddRemapping(saNinaModList, "WoodsSB",        "../" .. sLastDir .. "/Maps/Woods/WoodsSB")
    fnAddRemapping(saNinaModList, "WoodsSC",        "../" .. sLastDir .. "/Maps/Woods/WoodsSC")
    fnAddRemapping(saNinaModList, "WoodsSD",        "../" .. sLastDir .. "/Maps/Woods/WoodsSD")
    fnAddRemapping(saNinaModList, "WoodsSE",        "../" .. sLastDir .. "/Maps/Woods/WoodsSE")
    fnAddRemapping(saNinaModList, "RuinsMainA",     "../" .. sLastDir .. "/Maps/RuinsMain/RuinsMainA")
    fnAddRemapping(saNinaModList, "RuinsMainB",     "../" .. sLastDir .. "/Maps/RuinsMain/RuinsMainB")
    fnAddRemapping(saNinaModList, "RuinsUpperA",    "../" .. sLastDir .. "/Maps/RuinsUpper/RuinsUpperA")
    fnAddRemapping(saNinaModList, "RuinsBasementA", "../" .. sLastDir .. "/Maps/RuinsBasement/RuinsBasementA")
    fnAddRemapping(saNinaModList, "RuinsBasementB", "../" .. sLastDir .. "/Maps/RuinsBasement/RuinsBasementB")
    fnAddRemapping(saNinaModList, "RuinsBasementC", "../" .. sLastDir .. "/Maps/RuinsBasement/RuinsBasementC")

--External mod. Only used by developers. Requires a special flag to be set.
else
    fnAddRemapping(saNinaModList, "WoodsSA",        "../../../" .. sRootPath .. "/Maps/Woods/WoodsSA")
    fnAddRemapping(saNinaModList, "WoodsSB",        "../../../" .. sRootPath .. "/Maps/Woods/WoodsSB")
    fnAddRemapping(saNinaModList, "WoodsSC",        "../../../" .. sRootPath .. "/Maps/Woods/WoodsSC")
    fnAddRemapping(saNinaModList, "WoodsSD",        "../../../" .. sRootPath .. "/Maps/Woods/WoodsSD")
    fnAddRemapping(saNinaModList, "WoodsSE",        "../../../" .. sRootPath .. "/Maps/Woods/WoodsSE")
    fnAddRemapping(saNinaModList, "RuinsMainA",     "../../../" .. sRootPath .. "/Maps/RuinsMain/RuinsMainA")
    fnAddRemapping(saNinaModList, "RuinsMainB",     "../../../" .. sRootPath .. "/Maps/RuinsMain/RuinsMainB")
    fnAddRemapping(saNinaModList, "RuinsUpperA",    "../../../" .. sRootPath .. "/Maps/RuinsUpper/RuinsUpperA")
    fnAddRemapping(saNinaModList, "RuinsBasementA", "../../../" .. sRootPath .. "/Maps/RuinsBasement/RuinsBasementA")
    fnAddRemapping(saNinaModList, "RuinsBasementB", "../../../" .. sRootPath .. "/Maps/RuinsBasement/RuinsBasementB")
    fnAddRemapping(saNinaModList, "RuinsBasementC", "../../../" .. sRootPath .. "/Maps/RuinsBasement/RuinsBasementC")

end

-- |[ =========================== Upload Remaps ============================ ]|
--If this flag is true, the mod needs to upload map path remaps itself. This only occurs when starting a new game.
if(gbModHandleRemapAdditions == false) then return end

--Using the master list, upload this to the C++ code.
AL_SetProperty("Remappings Total", #gsaMasterList)
for i = 1, #gsaMasterList, 1 do
	AL_SetProperty("Remapping", i-1, gsaMasterList[i].sStartName, gsaMasterList[i].sRemapName)
end