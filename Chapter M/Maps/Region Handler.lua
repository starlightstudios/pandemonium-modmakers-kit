-- |[ ===================================== Region Handler ===================================== ]|
--Handles region indicators, normally in the top right of the screen. If this mod handles the 
-- region passed in, trip gbHandledRegion to true.

-- |[Mod Activity Check]|
--If the mod is not active, don't do any of the below.
if(OM_IsModActive("Nina's Adventure") == false) then return end

-- |[Arguments]|
if(fnArgCheck(1) == false) then return end
local sMapPath = LM_GetScriptArgument(0)

--In this mod, Nina is the party leader. If she isn't, then the player may have been previously
-- playing the mod on this savefile but isn't now.
if(gsPartyLeaderName ~= "Nina") then return end

-- |[ ===================================== Name Resolver ====================================== ]|
--Run backwards through the name and isolate the last slash to figure out the actual map name.
--io.write(" Resolving map name.\n")
local iLen = string.len(sMapPath)
local bNonSlashes = false
local iNameStart = 1
local iNameEnd = iLen
for i = iLen - 1, 1, -1 do
    
    --Get this letter.
    local sLetter = string.sub(sMapPath, i, i)

    --If it's a slash:
    if(sLetter == "/" or sLetter == "\\") then
        
        --Hasn't seen a non-slash yet.
        if(bNonSlashes == false) then
        
        --Has seen a non-slash, end here.
        else
            iNameStart = i+1
            break
        end
    
    --Normal letter:
    else

        --Hasn't seen a non-slash letter, so this is the last letter.
        if(bNonSlashes == false) then
            bNonSlashes = true
            iNameEnd = i
        end
    end
end

--Result
local sMapTrueName = string.sub(sMapPath, iNameStart, iNameEnd)
--io.write(" Final name is: " .. sMapTrueName .. "\n")

-- |[ ===================================== Lookup Builder ===================================== ]|
--If this is the first time running this script, set up the ref table here.
gsaNinaRefTable = nil
if(gsaNinaRefTable == nil) then
    
    -- |[ ================================== Constants ========================================= ]|
    local cfStandardFinalScale = 0.30
    
    -- |[ ================================== Functions ========================================= ]|
    -- |[Region Lookup Data]|
    --Creation function, adds entries to the table.
    gsaNinaRefTable = {}
    local sRefArea = "null"
    local function fnAddToRefTable(psRegionName, psMapName, psRangeS, psRangeE)
        local i = #gsaNinaRefTable + 1
        
        if(psRangeS ~= nil and psRangeE ~= nil) then
            local iByteS = string.byte(psRangeS, 1)
            local iByteE = string.byte(psRangeE, 1)
            for p = iByteS, iByteE, 1 do
                i = #gsaNinaRefTable + 1
                gsaNinaRefTable[i] = {psRegionName, psMapName .. string.char(p)}
            end
        else
            gsaNinaRefTable[i] = {psRegionName, psMapName}
        end
    end
    
    -- |[ ===================================== By Area ======================================== ]|
    -- |[Woods]|
    --Nix Nedar
    local sRegionName = "Nina Woods"
    fnAddToRefTable(sRegionName, "WoodsS", "A", "C")

    -- |[ ================================= Properties ========================================= ]|
    -- |[Region Property Data]|
    --This stores the data used by each region.
    gsaNinaRegionData = {}
    local function fnAddToDataTable(psRegionName, psFileName, psImagePrefix, piImagesTotal, pfScale)
        local i = #gsaNinaRegionData + 1
        gsaNinaRegionData[i] = {psRegionName, psFileName, psImagePrefix, piImagesTotal, pfScale}
    end
    
    --Create.
    fnAddToDataTable("Nina Woods", "UIRegionEvermoonForest", "EvermoonForest|", 22, cfStandardFinalScale)
end

-- |[ ========================================= Lookup ========================================= ]|
--Scan across the ref table and find a map name match. That's the region in question.
local sUseRegion = "Null"
for i = 1, #gsaNinaRefTable, 1 do
    if(gsaNinaRefTable[i][2] == sMapTrueName) then
        sUseRegion = gsaNinaRefTable[i][1]
        break
    end
end

--Debug.
--io.write(" Resolved region: " .. sUseRegion .. "\n")

-- |[ ======================================= Execution ======================================== ]|
--First, if the region came back as "Null", do nothing. It may be an unlisted region.
if(sUseRegion == "Null") then return end

--Flag that we handled the region.
gbStopModExec = true
gbModHandledCall = true

--Locate the relevant variable. This determines if the player has seen this region before.
local iHasSeenThisRegion = VM_GetVar("Root/Variables/Regions/"..sUseRegion.."/iHasSeenThisRegion", "N")
--io.write("Has seen region: " .. iHasSeenThisRegion .. "\n")

-- |[Has Visited Region Before]|
--Has seen the region before.
if(iHasSeenThisRegion == 1.0) then
    
    -- |[Property Grouping]|
    --Resolve which property set to use.
    local saPropertySet = {"None", "None", 0}
    for i = 1, #gsaNinaRegionData, 1 do
        if(gsaNinaRegionData[i][1] == sUseRegion) then
            saPropertySet = gsaNinaRegionData[i]
            --io.write(" Got property set in slot " .. i .. ".\n")
            break
        end
    end
    if(saPropertySet[1] == "None") then
        --io.write(" Error: No property set matching " .. sUseRegion .. "\n")
        return
    end
    
    --On a saved game, the region may have been seen but the asset not loaded. Check if the header exists.
    if(DL_Exists("Root/Images/RegionNotifiers/Permanent/" .. sUseRegion) == false) then
        
        --Open.
        SLF_Open(gsDatafilesPath .. saPropertySet[2] .. ".slf")
        
        --Extract.
        local sLastImageBuf = string.format("%s%02i", saPropertySet[3], saPropertySet[4]-1)
        DL_AddPath("Root/Images/RegionNotifiers/Permanent/")
        DL_ExtractBitmap(sLastImageBuf, "Root/Images/RegionNotifiers/Permanent/" .. sUseRegion)
        
        --Clean up.
        SLF_Close()
    end
    
    --Set it as the region notifier.
    AL_SetProperty("Region Notifier Image", "Root/Images/RegionNotifiers/Permanent/" .. sUseRegion)
    
    --Sizing.
    AL_SetProperty("Region Notifier Final Scale", saPropertySet[5])

-- |[Has Not Visited Region Before]|
--Has not seen the region, show the large overlay.
else

    -- |[Property Grouping]|
    --Resolve which property set to use.
    local saPropertySet = {"None", "None", "None", 0}
    for i = 1, #gsaNinaRegionData, 1 do
        if(gsaNinaRegionData[i][1] == sUseRegion) then
            saPropertySet = gsaNinaRegionData[i]
            --io.write(" Got property set in slot " .. i .. ".\n")
            break
        end
    end
    if(saPropertySet[1] == "None") then
        --io.write(" Error: No property set matching " .. sUseRegion .. "\n")
        return
    end

    -- |[Flags]|
    --Flag the region as seen.
    DL_AddPath("Root/Variables/Regions/"..sUseRegion.."/")
    VM_SetVar("Root/Variables/Regions/"..sUseRegion.."/iHasSeenThisRegion", "N", 1.0)

    --Setup.
    SLF_Open(gsDatafilesPath .. saPropertySet[2] .. ".slf")
    
    --Sizing.
    AL_SetProperty("Region Notifier Final Scale", saPropertySet[5])

    -- |[Permanent Notifier]|
    --Permanent notifier is always the last image.
    local sLastImageBuf = string.format("%s%02i", saPropertySet[3], saPropertySet[4]-1)
    DL_AddPath("Root/Images/RegionNotifiers/Permanent/")
    DL_ExtractBitmap(sLastImageBuf, "Root/Images/RegionNotifiers/Permanent/" .. sUseRegion)
    AL_SetProperty("Region Notifier Image", "Root/Images/RegionNotifiers/Permanent/" .. sUseRegion)

    -- |[Temporary Notifiers]|
    --Allocate space.
    AL_SetProperty("Allocate Region Notifier Images", saPropertySet[4], 2)

    --Load the temporary images.
    DL_AddPath("Root/Images/RegionNotifiers/Temporary/")
    for p = 0, saPropertySet[4]-1, 1 do
        local sImageBuf = string.format("%s%02i", saPropertySet[3], p)
        DL_ExtractBitmap(sImageBuf, "Root/Images/RegionNotifiers/Temporary/" .. p)
        AL_SetProperty("Set Region Notifier Frame", p, "Root/Images/RegionNotifiers/Temporary/" .. p)
    end

    -- |[Clean Up]|
    --io.write(" Done.\n")
    SLF_Close()
end
