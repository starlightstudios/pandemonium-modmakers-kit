-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "TheyKnowWeAreHere"
local sMapResolveName = "Map Resolve Name"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation(sMapResolveName)

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If an entity needs to spawn after a post-exec cutscene, put them here.

    -- |[Overlays]|
    --The runestone can be taken during the game's events. Check that here.
    local iZombieCompletedQuest    = VM_GetVar("Root/Variables/NinaMod/MainQuest/iZombieCompletedQuest", "N")
    local iHumanCompletedQuest     = VM_GetVar("Root/Variables/NinaMod/MainQuest/iHumanCompletedQuest", "N")
    local iMannequinCompletedQuest = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMannequinCompletedQuest", "N")
    local iReturnedRunestone       = VM_GetVar("Root/Variables/NinaMod/MainQuest/iReturnedRunestone", "N")
    if((iMannequinCompletedQuest == 1.0 or iZombieCompletedQuest == 1.0 or iHumanCompletedQuest == 1.0) and iReturnedRunestone == 0.0) then
        AL_SetProperty("Set Layer Disabled", "Runestone", true)
    end
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.

end
