-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
-- |[Swanky Place Ya Got Here]|
if(sObjectName == "Swanky") then

    -- |[Repeat Check]|
    local iSawSwanky = VM_GetVar("Root/Variables/NinaMod/MainQuest/iSawSwanky", "N")
    if(iSawSwanky == 1.0) then return end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawSwanky", "N", 1.0)
    
    -- |[Movement]|
    fnCutsceneMove("Nina", 14.75, 17.50)
    fnCutsceneFace("Nina", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Quite a bit swankier up here.[SOFTBLOCK] Almost like someone lives here.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] But who would live in a dusty old ruin like this?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] Woah, hello, is that a museum?[SOFTBLOCK] Let's hope it hasn't been stripped![SOFTBLOCK] I'm eating good tonight!") ]])
    fnCutsceneBlocker()

-- |[Mannequin TF Sequence]|
elseif(sObjectName == "GasTrap") then

    -- |[Repeat Check]|
    local iTriggeredGasTrap = VM_GetVar("Root/Variables/NinaMod/MainQuest/iTriggeredGasTrap", "N")
    if(iTriggeredGasTrap == 1.0) then return end
    
    -- |[Activation Check]|
    local iMetCountess = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMetCountess", "N")
    if(iMetCountess == 1.0) then return end
    
    -- |[ ==================== Zombie Version ==================== ]|
    --If Nina is a zombie, this scene plays.
    local sNinaForm = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")
    if(sNinaForm == "Zombie") then
    
        -- |[Flag]|
        VM_SetVar("Root/Variables/NinaMod/MainQuest/iTriggeredGasTrap", "N", 1.0)
    
        -- |[Dialogue]|
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Hm, this spot doesn't have a pedestal.[SOFTBLOCK] Wonder why?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] [SOUND|Nina|Gas]Hm?[SOFTBLOCK] What was that?[SOFTBLOCK] It sounds like...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        --Wait a bit.
        fnCutsceneWait(25)
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        
        -- |[Scene]|
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Nina]Gas![SOFTBLOCK] It's a gas trap![SOFTBLOCK] *cough cough*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Nina]...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Nina](It was at that point I remembered I, a zombie, the undead, did not need to breathe.[SOFTBLOCK] I felt pretty sheepish.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Nina](The gas spread around me and, after a few moments, fell to the floor, inert.[SOFTBLOCK] It left a beige powder behind.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Nina]I bet whoever made that trap was really proud of it, but it has a big flaw.[SOFTBLOCK] Oh well.") ]])
        fnCutsceneBlocker()
    
        -- |[Fade In]|
        fnCutsceneWait(25)
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
        fnCutsceneBlocker()
    
        return
    end
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/NinaMod/MainQuest/iTriggeredGasTrap", "N", 1.0)
    VM_SetVar("Root/Variables/NinaMod/MainQuest/iRevealedStaircase", "N", 1.0)
    VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawUndead", "N", 1.0)
    
    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Hm, this spot doesn't have a pedestal.[SOFTBLOCK] Wonder why?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] [SOUND|Nina|Gas]Hm?[SOFTBLOCK] What was that?[SOFTBLOCK] It sounds like...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    --Wait a bit.
    fnCutsceneWait(25)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    -- |[Scene]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Activate Scene") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Nina]Gas![SOFTBLOCK] It's a gas trap![SOFTBLOCK] *cough cough*[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Nina](I had already breathed a lot of it in.[SOFTBLOCK] It was a very fast-acting agent.[SOFTBLOCK] Crafted by a master.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Nina]*cough*[SOFTBLOCK] Oh no, oh crap. Wait, don't panic![SOFTBLOCK] Just gotta think calmly and I'll get out of this...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Nina]The most *cough*[SOFTBLOCK] important thing to do in a crisis is to breathe the gas in.[SOFTBLOCK] Let it coat the lungs.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Nina][SOUND|Nina|Petrify]*sniff*[SOFTBLOCK] Yeah, that's it.[SOFTBLOCK] Wait is that right?[SOFTBLOCK] My head is getting foggy...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Nina]I'll be fine if I just stick to the plan.[SOFTBLOCK] Keep it together, stick to the plan.[SOFTBLOCK] Breathe the gas in...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Nina]Wait, it's screwing with my head![SOFTBLOCK] This isn't right![SOFTBLOCK] Fight it, Nina![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Nina][SOUND|Nina|Petrify]When in a crisis, the first thing you do is serve your master![SOFTBLOCK] Serve![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Nina][SOUND|Nina|Petrify]Mannequins must always be loyal, and reaffirming that loyalty is crucial![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Nina]Wait, something about that didn't sound right.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Nina]But what was it?[SOFTBLOCK] Something about...[SOFTBLOCK] something...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Nina]...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Nina]...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Nina][SOUND|Nina|Petrify]...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "[VOICE|Nina]...") ]])
    fnCutsceneBlocker()
    
    --Execute transformation. Wait a bit.
    fnCutsceneInstruction([[ LM_ExecuteScript(gsNinaRoot .. "FormHandlers/Nina/Form_Mannequin.lua") ]])
    fnCutsceneWait(65)
    fnCutsceneBlocker()
    
    --Fade in.
    fnCutsceneWait(25)
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 25, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Mannequin.[SOFTBLOCK] I'm a mannequin.[SOFTBLOCK] Serve the master.[SOFTBLOCK] Loyalty undying.[SOFTBLOCK] Serve the master.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] The master is in the basement.[SOFTBLOCK] I must find the master and serve.[SOFTBLOCK] I must.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Mannequin...[SOFTBLOCK] I am a mannequin...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (The mantra repeated inside my head.[SOFTBLOCK] It was pure bliss, to say it over and over.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (The thought of serving my master made my body tingle.[SOFTBLOCK] I had been transformed and enslaved all at once.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (After being transformed, the next thing I did was find my master in the basement.[SOFTBLOCK] There was a secret entrance in the courtyard near the gravestones.)") ]])
    fnCutsceneBlocker()

-- |[Mannequin Task Zone]|
elseif(sObjectName == "TaskActivate") then

    -- |[Repeat Check]|
    local iMannequinStartedTasks = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMannequinStartedTasks", "N")
    if(iMannequinStartedTasks == 1.0) then return end

    -- |[Quest Check]|
    local iMannequinCompletedQuest = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMannequinCompletedQuest", "N")
    if(iMannequinCompletedQuest == 0.0) then return end

    -- |[Flags]|
    VM_SetVar("Root/Variables/NinaMod/MainQuest/iMannequinStartedTasks", "N", 1.0)

    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I'm a mannequin.[SOFTBLOCK] Serve the master.[SOFTBLOCK] Loyalty undying.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] The master has willed me to check on her treasures, and to clean them.[SOFTBLOCK] Her will be done.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (I didn't realize at the time just what was missing.[SOFTBLOCK] I was focused on cleaning, not thinking it would be possible to steal any of these.)") ]])
    fnCutsceneBlocker()

-- |[Mannequin Walkback Zone]|
elseif(sObjectName == "MannequinTasksL" or sObjectName == "MannequinTasksC" or sObjectName == "MannequinTasksR") then

    -- |[Activity Check]|
    --This doesn't occur if the player hasn't started the sequence, or if they ended it.
    local iMannequinStartedTasks = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMannequinStartedTasks", "N")
    local iSawMissingRunestone   = VM_GetVar("Root/Variables/NinaMod/MainQuest/iSawMissingRunestone", "N")
    if(iMannequinStartedTasks == 0.0 or iSawMissingRunestone == 1.0) then return end

    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] The master has willed that I clean her treasures.[SOFTBLOCK] I will clean her treasures.[SOFTBLOCK] I must not leave until I have.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

    -- |[Walkback]|
    --Position to move to is based on which trigger was hit.
    if(sObjectName == "MannequinTasksL") then
        fnCutsceneMove("Nina", 9.75, 14.50)
        fnCutsceneFace("Nina", 0, -1)
    elseif(sObjectName == "MannequinTasksR") then
        fnCutsceneMove("Nina", 19.75, 14.50)
        fnCutsceneFace("Nina", 0, -1)
    else
        fnCutsceneMove("Nina", 14.75, 11.50)
        fnCutsceneFace("Nina", 0, -1)
    end
    
-- |[ ==================================== Zombie Treasures ==================================== ]|
-- |[Zombie Treasures]|
elseif(sObjectName == "ZombieScene") then

    -- |[Flag]|
    VM_SetVar("Root/Variables/NinaMod/MainQuest/iZombieSawMissingRunestone", "N", 1.0)
    VM_SetVar("Root/Variables/NinaMod/MainQuest/iStartedRuneRecovery", "N", 1.0)

    -- |[Cut to Black]|
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    -- |[Position]|
    fnCutsceneTeleport("Nina", 15.25, 10.50)
    fnCutsceneFace("Nina", 0, -1)
    TA_Create("Countess")
        TA_SetProperty("Position", 14, 10)
        TA_SetProperty("Facing", gci_Face_North)
        TA_SetProperty("Clipping Flag", true)
        TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
        fnSetCharacterGraphics("Root/Images/Sprites/Countess/", false)
    DL_PopActiveObject()
    
    -- |[Fade In]|
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] This, my friend, is my gallery of treasures.[SOFTBLOCK] I hadn't finished moving my things in when those lowlifes attacked.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] But, you may take any one of them you like.[SOFTBLOCK] Let it always be a symbol of my gratitude.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Neat![SOFTBLOCK] How come the bandits didn't steal any of them, though?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I have magical powers, of course.[SOFTBLOCK] I put a spell on the pedestals to keep the items attached.[SOFTBLOCK] It takes a lot of force to remove one.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Sadly, I only had time to apply it to these five things.[SOFTBLOCK] I cannot impress you with the full wealth of my treasures.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[SOFTBLOCK] Four things.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Four...[SOFTBLOCK] things?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Is my brain rotting?[SOFTBLOCK] I only count four.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] There was a...[SOFTBLOCK] on the...[SOFTBLOCK] one is missing![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] It was just a minor...[SOFTBLOCK] minor thing.[SOFTBLOCK] I -[SOFTBLOCK] I don't care if it's gone, it was worthless![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] *sniff*[SOFTBLOCK] It doesn't matter.[SOFTBLOCK] Please take whatever you like.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Are you...[SOFTBLOCK] crying?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] *sniff*[SOFTBLOCK] I am not.[SOFTBLOCK] Truly.[SOFTBLOCK] The little disk was a luck charm, given to me by a nobody![SOFTBLOCK] I don't care about it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I don't know why I am crying...[SOFTBLOCK] I cannot stop the tears...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Don't cry, stop![SOFTBLOCK] Please![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I'll get it back for you, don't worry![SOFTBLOCK] I'll rip their spines out and beat them to death with them![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I am telling you not to, please, it doesn't matter.[SOFTBLOCK] I just can't stop crying.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] M-[SOFTBLOCK]must be garlic they left behind![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Oh come on, I know - [SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Please take a treasure![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] I'll bring your treasure back, and that's final.[SOFTBLOCK] Little stone disk?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] A little stone disk with a yellow symbol on it, yes.[SOFTBLOCK] But really, it doesn't matter.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] I'm on it.[SOFTBLOCK] Nobody messes with the undead and lives!") ]])
    fnCutsceneBlocker()

    
-- |[ ==================================== Human Treasures ===================================== ]|
-- |[Human Treasures]|
elseif(sObjectName == "HumanScene") then

    -- |[Flag]|
    VM_SetVar("Root/Variables/NinaMod/MainQuest/iHumanSawMissingRunestone", "N", 1.0)
    VM_SetVar("Root/Variables/NinaMod/MainQuest/iStartedRuneRecovery", "N", 1.0)

    -- |[Cut to Black]|
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 0, gci_Fade_Under_GUI, true, 0, 0, 0, 1, 0, 0, 0, 1) ]])
    fnCutsceneBlocker()
    
    -- |[Position]|
    fnCutsceneTeleport("Nina", 15.25, 10.50)
    fnCutsceneFace("Nina", 0, -1)
    TA_Create("Countess")
        TA_SetProperty("Position", 14, 10)
        TA_SetProperty("Facing", gci_Face_North)
        TA_SetProperty("Clipping Flag", true)
        TA_SetProperty("Activation Script", fnResolvePath() .. "Dialogue.lua")
        fnSetCharacterGraphics("Root/Images/Sprites/Countess/", false)
    DL_PopActiveObject()
    
    -- |[Fade In]|
    fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, false, 0, 0, 0, 1, 0, 0, 0, 0) ]])
    fnCutsceneBlocker()
    fnCutsceneWait(45)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] And this is my gallery.[SOFTBLOCK] I hadn't finished setting it up, of course.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Wow![SOFTBLOCK] All these are yours?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Gifts, heirlooms.[SOFTBLOCK] The lockbox was my great-grandmother's.[SOFTBLOCK] I don't have the key anymore, but I kept the box itself.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] The necklace was given to me by my oldest sister.[SOFTBLOCK] She made it herself, actually![SOFTBLOCK] I'm not sure where the gem came from, though.[SOFTBLOCK] She likely bought it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] You've a goldsmith in the family?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] We all have hobbies we took.[SOFTBLOCK] I practiced magic, horseriding, sailing.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] Do you have any horses?[SOFTBLOCK] I love horses![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Oh, no, I had to rent one to bring my things here.[SOFTBLOCK] But I would love to get some for the estate.[SOFTBLOCK] Perhaps build a range for them.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Oh, and the stone in the back was sold to me by a beggar.[SOFTBLOCK] He was most adamant I take it, so I bought it from him for a platina.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I hope he made good use of that.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] What stone?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Wait!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneFace("Nina", 1, -1)
    fnCutsceneMove("Countess", 16.25, 10.50)
    fnCutsceneMove("Countess", 16.25, 9.50)
    fnCutsceneFace("Countess", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutscenePlaySound("World|FlipSwitch")
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Apologies, I had placed a trap on that spot.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] It would have turned you into a resin mannequin briefly, to allow me to apprehend you.[SOFTBLOCK] I came up with the formula myself.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] A resin mannequin?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] They are used in the shops in Jeffespeir to show clothes to buyers.[SOFTBLOCK] I decided to model a trap based on one.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] The thief would be compelled to hold up the thing they were trying to steal.[SOFTBLOCK] Obviously I haven't completed them yet.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] And I hope I won't be having my first test subject be a brave hero![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Heh.[SOFTBLOCK] But as I was saying.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Nina", 14.25, 6.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Nina", -1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Nina", 1, 0)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    fnCutsceneFace("Nina", 0, -1)
    fnCutsceneWait(85)
    fnCutsceneBlocker()
    fnCutsceneFace("Nina", 0, 1)
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Nope, didn't fall on the ground either.[SOFTBLOCK] There's no stone here.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] It -[SOFTBLOCK] it has been stolen! But how![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] The pedestals also are enchanted to hold down the items on them.[SOFTBLOCK] P-[SOFTBLOCK]perhaps I did not finish the spell fully?[SOFTBLOCK] I should have tested it![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Bother yourself no more about it, Miss Nina.[SOFTBLOCK] It was a mere trinket.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Yeah, but those guys did rob you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] It's not worth a man's life, and certainly not an elf's life.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] No way, you think I'm going to just let people steal from you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Uh, if it's okay with you, that is![SOFTBLOCK] I'll get it back for you![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Please don't.[SOFTBLOCK] I would not want to risk you getting hurt.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] You're...[SOFTBLOCK] awfully concerned about me.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Am I?[SOFTBLOCK] Are you sure I am not this way about everyone?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Are you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] No, but...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] Countess, I will recover your stolen treasure for you.[SOFTBLOCK] On the condition that you have some more of that tasty bread when I return![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] If I'm going to be your crusading hero, I can't be letting anyone get away with wronging you, can I?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] My crusading hero?[SOFTBLOCK] You?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] You -[SOFTBLOCK] don't want me to?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] It's just that I had other ideas of what sorts of things -[SOFTBLOCK] work![SOFTBLOCK] Work, I wanted you to do for me.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Things that did not take you far from the estate.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Then I'd best hurry, right?[SOFTBLOCK] Those thugs won't have gotten far.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] If I can't talk you out of it, at least take this with you.") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Countess", 15.25, 9.50)
    fnCutsceneMove("Countess", 15.25, 6.50)
    fnCutsceneBlocker()
    fnCutsceneFace("Nina", 1, 0)
    fnCutsceneFace("Countess", -1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Hm? Take what with me?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] [SOUND|Nina|Kiss]*kiss*[SOFTBLOCK]This.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] You can count on me, lady Yarla!") ]])
end
