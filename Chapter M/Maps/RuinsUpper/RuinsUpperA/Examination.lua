-- |[ ======================================= Examination ====================================== ]|
--Script that is called when the player examines an object. The name of the object examined will
-- be passed in as the 0th argument.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the object/point/whatever. This is set in the constructer script.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= Exits ========================================== ]|
-- |[ ====================================== Examinables ======================================= ]|
-- |[Tablet]|
if(sObjectName == "Tablet") then
    
    -- |[Variables]|
    local sNinaForm = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")
    
    -- |[Human]|
    if(sNinaForm == "Human" or sNinaForm == "Zombie") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A tablet with purple gems embedded into it.[SOFTBLOCK] Looks expensive, but I don't know what language the text is in.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (It seems stuck to the pedestal.[SOFTBLOCK] Might be magic...)") ]])
        fnCutsceneBlocker()
        
    -- |[Mannequin]|
    elseif(sNinaForm == "Mannequin") then
    
        --Variables.
        local iMannequinCompletedQuest = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMannequinCompletedQuest", "N")
        local iDustedTablet            = VM_GetVar("Root/Variables/NinaMod/MainQuest/iDustedTablet", "N")
    
        --Has not met the countess, or completed the quest:
        if(iMannequinCompletedQuest == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (One of the master's treasures, a tablet with ancient writing and gems encrusted into the stone.)") ]])
            fnCutsceneBlocker()
        
        --Completed the quest, has not dusted the tablet.
        elseif(iMannequinCompletedQuest == 1.0 and iDustedTablet == 0.0) then
            VM_SetVar("Root/Variables/NinaMod/MainQuest/iDustedTablet", "N", 1.0)
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (One of the master's treasures, a tablet with ancient writing and gems encrusted into the stone.[SOFTBLOCK] It has accrued some dust.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] [SOUND|Nina|DustOff](I have dusted the tablet off.)") ]])
            fnCutsceneBlocker()
        
        --Completed the quest. Dusted the tablet.
        elseif(iMannequinCompletedQuest == 1.0 and iDustedTablet == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (One of the master's treasures, a tablet with ancient writing and gems encrusted into the stone.[SOFTBLOCK] It has been cleaned recently.)") ]])
            fnCutsceneBlocker()
        end
        
    end

-- |[Necklace]|
elseif(sObjectName == "Necklace") then
    
    -- |[Variables]|
    local sNinaForm = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")
    
    -- |[Human]|
    if(sNinaForm == "Human" or sNinaForm == "Zombie") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A necklace with a little locket on it, hanging on a black felt display.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (It's incredibly heavy.[SOFTBLOCK] Must be a spell to weigh it down and stop thieves.)") ]])
        fnCutsceneBlocker()
        
    -- |[Mannequin]|
    elseif(sNinaForm == "Mannequin") then
    
        --Variables.
        local iMannequinCompletedQuest = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMannequinCompletedQuest", "N")
        local iDustedNecklace          = VM_GetVar("Root/Variables/NinaMod/MainQuest/iDustedNecklace", "N")
    
        --Has not met the countess, or completed the quest:
        if(iMannequinCompletedQuest == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (One of the master's treasures, a necklace with a garnet locket.") ]])
            fnCutsceneBlocker()
        
        --Completed the quest, has not dusted the necklace.
        elseif(iMannequinCompletedQuest == 1.0 and iDustedNecklace == 0.0) then
            VM_SetVar("Root/Variables/NinaMod/MainQuest/iDustedNecklace", "N", 1.0)
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (One of the master's treasures, a necklace with a garnet locket.[SOFTBLOCK] It has accrued some dust.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] [SOUND|Nina|DustOff](I have dusted the necklace off.)") ]])
            fnCutsceneBlocker()
        
        --Completed the quest. Dusted the necklace.
        elseif(iMannequinCompletedQuest == 1.0 and iDustedNecklace == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (One of the master's treasures, a necklace with a garnet locket.[SOFTBLOCK] It has been cleaned recently.)") ]])
            fnCutsceneBlocker()
        end
    
    end

-- |[Vase]|
elseif(sObjectName == "Vase") then
    
    -- |[Variables]|
    local sNinaForm = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")
    
    -- |[Human]|
    if(sNinaForm == "Human" or sNinaForm == "Zombie") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A ceramic vase painted with blue birds.[SOFTBLOCK] Looks like something made back home, in the old days.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (I can't see a point of attachment, but it seems to be held to the pedestal.)") ]])
        fnCutsceneBlocker()
        
    -- |[Mannequin]|
    elseif(sNinaForm == "Mannequin") then
    
        --Variables.
        local iMannequinCompletedQuest = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMannequinCompletedQuest", "N")
        local iDustedVase              = VM_GetVar("Root/Variables/NinaMod/MainQuest/iDustedVase", "N")
    
        --Has not met the countess, or completed the quest:
        if(iMannequinCompletedQuest == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (One of the master's treasures, a ceramic vase made in the eastern states.") ]])
            fnCutsceneBlocker()
        
        --Completed the quest, has not dusted the vase.
        elseif(iMannequinCompletedQuest == 1.0 and iDustedVase == 0.0) then
            VM_SetVar("Root/Variables/NinaMod/MainQuest/iDustedVase", "N", 1.0)
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (One of the master's treasures, a ceramic vase made in the eastern states.[SOFTBLOCK] It has accrued some dust.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] [SOUND|Nina|DustOff](I have dusted the necklace off.)") ]])
            fnCutsceneBlocker()
        
        --Completed the quest. Dusted the vase.
        elseif(iMannequinCompletedQuest == 1.0 and iDustedVase == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (One of the master's treasures, a ceramic vase made in the eastern states.[SOFTBLOCK] It has been cleaned recently.)") ]])
            fnCutsceneBlocker()
        end
    
    end

-- |[Lockbox]|
elseif(sObjectName == "Lockbox") then
    
    -- |[Variables]|
    local sNinaForm = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")
    
    -- |[Human]|
    if(sNinaForm == "Human" or sNinaForm == "Zombie") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A simple wooden lockbox.[SOFTBLOCK] It doesn't have any jewelry inside, and isn't locked.[SOFTBLOCK] Maybe the box itself is valuable?)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (But I can't pick it up.[SOFTBLOCK] It's like it's held in place with magic.)") ]])
        fnCutsceneBlocker()
        
    -- |[Mannequin]|
    elseif(sNinaForm == "Mannequin") then
    
        --Variables.
        local iMannequinCompletedQuest = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMannequinCompletedQuest", "N")
        local iDustedLockbox           = VM_GetVar("Root/Variables/NinaMod/MainQuest/iDustedLockbox", "N")
    
        --Has not met the countess, or completed the quest:
        if(iMannequinCompletedQuest == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (One of the master's treasures, a lockbox with great sentimental value.") ]])
            fnCutsceneBlocker()
        
        --Completed the quest, has not dusted the vase.
        elseif(iMannequinCompletedQuest == 1.0 and iDustedLockbox == 0.0) then
            VM_SetVar("Root/Variables/NinaMod/MainQuest/iDustedLockbox", "N", 1.0)
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (One of the master's treasures, a lockbox with great sentimental value.[SOFTBLOCK] It has accrued some dust.)[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] [SOUND|Nina|DustOff](I have dusted the necklace off.)") ]])
            fnCutsceneBlocker()
        
        --Completed the quest. Dusted the vase.
        elseif(iMannequinCompletedQuest == 1.0 and iDustedLockbox == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (One of the master's treasures, a lockbox with great sentimental value.[SOFTBLOCK] It has been cleaned recently.)") ]])
            fnCutsceneBlocker()
        end
    
    end

-- |[Runestone]|
elseif(sObjectName == "Runestone") then
    
    -- |[Variables]|
    local sNinaForm = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")
    
    -- |[Human]|
    if(sNinaForm == "Human" or sNinaForm == "Zombie") then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A little grey circle with a bright yellow rune on it.[SOFTBLOCK] I've never seen that symbol before.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (Looks worthless.[SOFTBLOCK] Must be here for sentimental reasons.)") ]])
        fnCutsceneBlocker()
        
    -- |[Mannequin]|
    elseif(sNinaForm == "Mannequin") then
    
        --Variables.
        local iMannequinCompletedQuest = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMannequinCompletedQuest", "N")
        local iSawMissingRunestone     = VM_GetVar("Root/Variables/NinaMod/MainQuest/iSawMissingRunestone", "N")
        local iReturnedRunestone       = VM_GetVar("Root/Variables/NinaMod/MainQuest/iReturnedRunestone", "N")
    
        --Has not completed the quest. The runestone is still here:
        if(iMannequinCompletedQuest == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (One of the master's treasures, a small circular stone disc with an unfamiliar rune on it.)") ]])
            fnCutsceneBlocker()
    
        --Has completed the quest, but not noticed the runestone is missing yet.
        elseif(iMannequinCompletedQuest == 1.0 and iSawMissingRunestone == 0.0) then
            VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawMissingRunestone", "N", 1.0)
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] This pedestal housed a stone disc with a rune on it.[SOFTBLOCK] It is not here.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] It has not fallen on the ground.[SOFTBLOCK] I do not see it nearby.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I must tell the master about this right away.") ]])
            fnCutsceneBlocker()
    
        --Has completed the quest, and noticed the runestone missing, but not found and returned it yet.
        elseif(iMannequinCompletedQuest == 1.0 and iSawMissingRunestone == 1.0 and iReturnedRunestone == 0.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (A pedestal that held one of the master's treasures.[SOFTBLOCK] It has been stolen.)") ]])
            fnCutsceneBlocker()
    
        --Runestone returned.
        elseif(iMannequinCompletedQuest == 1.0 and iSawMissingRunestone == 1.0 and iReturnedRunestone == 1.0) then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thought:[VOICE|Leader] (One of the master's treasures, a small circular stone disc with an unfamiliar rune on it.[SOFTBLOCK] It has been returned to its rightful place in the master's collection.)") ]])
            fnCutsceneBlocker()
        end
    end
    
-- |[ ========================================== Other ========================================= ]|
-- |[ ========================================== Debug ========================================= ]|
else
	fnStandardDialogue("Warning: Unable to parse examination case: " .. sObjectName .. "!")
end
