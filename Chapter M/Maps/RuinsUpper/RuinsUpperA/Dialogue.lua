-- |[ ===================================== Dialogue Script ==================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.

-- |[Arguments]|
--Argument Listing:
-- 0: sTopicName - The topic under consideration. If this is the first time talking to the NPC, the topic is always "Hello".
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ========================================= "Hello" ======================================== ]|
--"Hello", standard entry topic. Other topics are used as responses for decisions.
if(sTopicName == "Hello") then
	
    -- |[Name Resolve]|
    --Resolve the name of the calling entity.
    local sActorName = TA_GetProperty("Name")

    --Set facing.
    TA_SetProperty("Face Character", "PlayerEntity")
    
    -- |[Countess]|
    if(sActorName == "Countess") then
        
        -- |[Variables]|
        local sNinaForm = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")
        
        -- |[Human]|
        if(sNinaForm == "Human") then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I can't talk you out of it?[SOFTBLOCK] Perhaps I should hire you full time.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] I can hunt and fish with the best of them![SOFTBLOCK] Hire me as a hunter![BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] And you are hungry?[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Incredibly bad luck.[SOFTBLOCK] Haven't seen a single animal in days.[SOFTBLOCK] It's eerie.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] But I know my luck is turning around!") ]])
        
        -- |[Zombie]|
        elseif(sNinaForm == "Zombie") then
            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I don't think I can go any further into your debt, my zombie friend.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I'm thinking of all the crimes I can commit against anyone who'd make you cry.[BLOCK][CLEAR]") ]])
            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I'll be back.[SOFTBLOCK] But I'll leave their severed heads outside.[SOFTBLOCK] Wouldn't want to stain the carpet.") ]])
        
        end
    end
end
