-- |[ ======================================= Constructor ====================================== ]|
--Map construction script. Requires no arguments, builds the map and populates it according to script variables.
-- If an argument is passed, that indicates this is an internal transition case. The argument needs to exist, but
-- doesn't have to be anything specific. Internal transitions do not spawn party characters for debug reasons.
local sLevelName = fnResolveDirectory() --Map's name is the name of the folder it is in.
local sLevelMusic = "TheyKnowWeAreHere"
local sMapResolveName = "Map Resolve Name"

--Check arguments.
local iArgsTotal = LM_GetNumOfArgs()
local iConstructorType = 0.0
if(iArgsTotal > 0) then iConstructorType = LM_GetScriptArgument(0, "N") end

-- |[ ======================================== Standard ======================================== ]|
--Creates the level, parses its mapdata, and sets the examination script.
if(iConstructorType == gci_Constructor_Start) then

    -- |[Construction]|
	--Music.
	AL_SetProperty("Music", sLevelMusic)
	
	--Chest path.
	DL_AddPath("Root/Variables/Chests/" .. sLevelName .. "/")
	
	--Construct the level based on the path.
	local sBasePath = fnResolvePath()
	fnStandardLevel(sBasePath)
	AL_SetProperty("Name", sLevelName)

	--Create the player character if she doesn't exist for any reason.
	if(iArgsTotal ~= 1) then fnStandardCharacter() end

	--Standard spawn enemies and pulse them to ignore the player.
	fnStandardEnemyPulse()
	
	--Map Setup
	fnResolveMapLocation(sMapResolveName)

-- |[ ======================================== Post-Exec ======================================= ]|
--Called after the internal transition process has been handled. You can now safely remove objects such as
-- exits or NPCs without causing object-not-found errors.
elseif(iConstructorType == gci_Constructor_PostExec) then

	-- |[NPC Spawning]|
	--If bandits are driven off, remove them here.
    local iMannequinBanditsDefeated = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMannequinBanditsDefeated", "N")
    local iZombieBanditsDefeated    = VM_GetVar("Root/Variables/NinaMod/MainQuest/iZombieBanditsDefeated", "N")
    local iHumanBanditsDefeated     = VM_GetVar("Root/Variables/NinaMod/MainQuest/iHumanBanditsDefeated", "N")
    if(iMannequinBanditsDefeated >= 3.0 or iZombieBanditsDefeated >= 3.0 or iHumanBanditsDefeated >= 3.0) then
        EM_PushEntity("EnemyAA")
            RE_SetDestruct(true)
        DL_PopActiveObject()
        EM_PushEntity("EnemyBA")
            RE_SetDestruct(true)
        DL_PopActiveObject()
        EM_PushEntity("EnemyCA")
            RE_SetDestruct(true)
        DL_PopActiveObject()
    end

    -- |[Overlays]|
    --Player has not revealed the secret staircase, so hide it here.
    local iRevealedStaircase = VM_GetVar("Root/Variables/NinaMod/MainQuest/iRevealedStaircase", "N")
    if(iRevealedStaircase == 0.0) then
        AL_SetProperty("Set Layer Disabled", "StairsDown", true)
        AL_SetProperty("Disable Staircase", "ToRuinsBasementA")
    end
    
    -- |[Skillbooks]|
    --Place skillbook overlay code here.

end
