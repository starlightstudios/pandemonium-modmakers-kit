-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(2)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
if(sObjectName == "DefeatHumans") then

    -- |[Variables]|
    local sNinaForm = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")
    local iStartedRuneRecovery = VM_GetVar("Root/Variables/NinaMod/MainQuest/iStartedRuneRecovery", "N")
    
    -- |[Don't Bother]|
    --Already past this part of the quest.
    if(iStartedRuneRecovery == 1.0) then return end

    -- |[Mannequin]|
    if(sNinaForm == "Mannequin") then

        -- |[Repeat Check]|
        local iSawMannequinDriveOffInvaders = VM_GetVar("Root/Variables/NinaMod/MainQuest/iSawMannequinDriveOffInvaders", "N")
        if(iSawMannequinDriveOffInvaders == 1.0) then return end
        
        -- |[Activation Check]|
        --Has to have spoken to the countess first.
        local iMannequinMetMaster = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMannequinMetMaster", "N")
        if(iMannequinMetMaster == 0.0) then return end

        -- |[Flag]|
        VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawMannequinDriveOffInvaders", "N", 1.0)

        -- |[Dialogue]|
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (The master's magic has transformed me, and robbed me of my emotions.[SOFTBLOCK] Which I didn't particularly mind, of course.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Still, she had given me an order, and I was going to carry it out.[SOFTBLOCK] The mantra repeating in my head made sure of that.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I am a mannequin.[SOFTBLOCK] Loyalty undying.[SOFTBLOCK] Serve the master.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I must defeat three bandits.[SOFTBLOCK] That should drive them off.[SOFTBLOCK] Master has bid me do this.[SOFTBLOCK] I must do it.") ]])
        fnCutsceneBlocker()
    
    -- |[Zombie]|
    elseif(sNinaForm == "Zombie") then

        -- |[Repeat Check]|
        local iSawZombieDriveOffInvaders = VM_GetVar("Root/Variables/NinaMod/MainQuest/iSawZombieDriveOffInvaders", "N")
        if(iSawZombieDriveOffInvaders == 1.0) then return end
        
        -- |[Activation Check]|
        local iMetCountess = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMetCountess", "N")
        if(iMetCountess == 0.0) then return end

        -- |[Flag]|
        VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawZombieDriveOffInvaders", "N", 1.0)

        -- |[Dialogue]|
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Well, then.[SOFTBLOCK] Guess I just gotta beat some of them up.[SOFTBLOCK] Maybe taking down three will be enough?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Heh.[SOFTBLOCK] This might actually be fun.") ]])
        fnCutsceneBlocker()

    -- |[Human]|
    elseif(sNinaForm == "Human") then

        -- |[Repeat Check]|
        local iSawHumanDriveOffInvaders = VM_GetVar("Root/Variables/NinaMod/MainQuest/iSawHumanDriveOffInvaders", "N")
        local iMetCountess = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMetCountess", "N")
        if(iSawHumanDriveOffInvaders == 1.0 or iMetCountess == 0.0) then return end

        -- |[Flag]|
        VM_SetVar("Root/Variables/NinaMod/MainQuest/iSawHumanDriveOffInvaders", "N", 1.0)

        -- |[Dialogue]|
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] These must be the goons that are bothering the countess.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Nothing a couple arrows can't handle!") ]])
        fnCutsceneBlocker()

    end
end
