-- |[ ======================================== Triggers ======================================== ]|
--Called when the player steps into a trigger zone. Can be either a partial or a whole collision, based on
-- the provided flag. The name of the trigger stepped in is also provided to the script.

-- |[Arguments]|
--Argument Listing:
-- 0: sObjectName - Name of the trigger that was stepped in.
-- 1: iIsWholeCollision - 1 if the player is wholly within the trigger, 0 if it's a partial collision.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sObjectName = LM_GetScriptArgument(0)
local iIsWholeCollision = tonumber(LM_GetScriptArgument(1))

-- |[Variables]|
--If variables need to be resolved commonly, do so here.

-- |[ ======================================= Execution ======================================== ]|
-- |[Overhearing the Bandits]|
if(sObjectName == "Overhear") then

    -- |[Repeat Check]|
    local iOverheardThugs = VM_GetVar("Root/Variables/NinaMod/MainQuest/iOverheardThugs", "N")
    if(iOverheardThugs == 1.0) then return end

    -- |[Flag]|
    VM_SetVar("Root/Variables/NinaMod/MainQuest/iOverheardThugs", "N", 1.0)
    
    -- |[Spawning]|
    TA_Create("Leader")
        TA_SetProperty("Position", 8, 16)
        TA_SetProperty("Facing", gci_Face_East)
        fnSetCharacterGraphics("Root/Images/Sprites/CultistM/", false)
    DL_PopActiveObject()
    TA_Create("Warrior")
        TA_SetProperty("Position", 21, 16)
        TA_SetProperty("Facing", gci_Face_West)
        fnSetCharacterGraphics("Root/Images/Sprites/CultistM/", false)
    DL_PopActiveObject()
    
    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Crud.[SOFTBLOCK] This place is a ruin.[SOFTBLOCK] Probably won't have any food if nobody is living here...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] But maybe...[SOFTBLOCK] there will be some untouched goodies left behind?[SOFTBLOCK] If I can sell something in town I can buy some bread...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Maybe best not to get too optimistic.[SOFTBLOCK] It's anyone's guess if the place hasn't been stripped...[SOFTBLOCK] Wait...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Someone's here!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneMove("Nina", 13.25, 24.50, 2.50)
    fnCutsceneMove("Nina", 12.25, 24.50, 2.50)
    fnCutsceneFace("Nina", 1, 0)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Hope they didn't see me...") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Camera, Movement]|
    fnCutsceneMove("Leader",  14.25, 16.50)
    fnCutsceneMove("Warrior", 15.25, 16.50)
    fnCutsceneCamera("Position", gcfCamera_Cutscene_Speed, 15.75, 16.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Leader", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Warrior", "Neutral") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Well?[SOFTBLOCK] Have you found it?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Warrior:[E|Neutral] Take a look boss![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] This isn't it, you idiot![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Warrior:[E|Neutral] But you said it'd have writing on it, right?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] I said it'd have one symbol on it, you numbskull![SOFTBLOCK] How many symbols is this?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Warrior:[E|Neutral] ...[SOFTBLOCK] At least five?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] (My kingdom for underlings who can read!)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] No, you dolt.[SOFTBLOCK] This isn't it.[SOFTBLOCK] I don't even know what language this is![SOFTBLOCK] This is worthless![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Warrior:[E|Neutral] But boss, maybe we can sell it to a mage?[SOFTBLOCK] What if the text is valuable![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Hm, not a bad idea.[SOFTBLOCK] It could be a powerful spell...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Or it could be a recipe for cinnamon ice cream.[SOFTBLOCK] No way to know until we get it someplace civilized.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Leader:[E|Neutral] Hold on to it for now, but keep looking.[SOFTBLOCK] Check the basement, the chapel, everywhere![SOFTBLOCK] Find it![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Warrior:[E|Neutral] You got it, boss!") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Movement]|
    fnCutsceneCamera("Actor Name", gcfCamera_Cutscene_Speed, "Nina")
    fnCutsceneMove("Leader",  21.25, 16.50)
    fnCutsceneMove("Leader",  21.25, 11.50)
    fnCutsceneTeleport("Leader", -1.25, -1.50)
    fnCutsceneMove("Warrior", 21.25, 16.50)
    fnCutsceneMove("Warrior", 21.25, 11.50)
    fnCutsceneTeleport("Warrior", -1.25, -1.50)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Dialogue]|
    fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
    LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Phew, that was close.[SOFTBLOCK] What were those goons looking for?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] At least that confirms that this place hasn't been totally looted yet.[SOFTBLOCK] Maybe I can find something valuable before they do...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Might come to a fight, though.[SOFTBLOCK] They don't look like the kind, understanding types.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Brigands were a common sight in the forests.[SOFTBLOCK] They were the sort of people without the discipline to work, who just liked to hurt others.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Sometimes they were ex-soldiers, discarded by their lords when there were no wars to fight.[SOFTBLOCK] Sometimes they were just bullies who liked making others do the work for them.)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (I had never had a problem killing people to survive in the past.[SOFTBLOCK] If it's them or me, I always chose me...)[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (But taking them on was a risk.[SOFTBLOCK] I resolved to try to avoid them, or at least get the first shot....)") ]])
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()

-- |[ ========================================= Finale ========================================= ]|
-- |[The Big Reveal]|
elseif(sObjectName == "BigRevealS" or sObjectName == "BigRevealW" or sObjectName == "BigRevealE") then

    -- |[Repeat Check]|
    local iSeenBigReveal = VM_GetVar("Root/Variables/NinaMod/MainQuest/iSeenBigReveal", "N")
    if(iSeenBigReveal == 1.0) then return end

    -- |[Activation Check]|
    --Must have recovered the runestone first.
    local iRecoveredRunestone = VM_GetVar("Root/Variables/NinaMod/MainQuest/iRecoveredRunestone", "N")
    if(iRecoveredRunestone == 0.0) then return end
    
    -- |[Variables]|
    local iHasZombie    = VM_GetVar("Root/Variables/Global/Nina/iHasZombieForm", "N")
    local iHasMannequin = VM_GetVar("Root/Variables/Global/Nina/iHasMannequinForm", "N")
    
    -- |[Flag]|
    VM_SetVar("Root/Variables/NinaMod/MainQuest/iSeenBigReveal", "N", 1.0)

    -- |[Movement]|
    if(sObjectName == "BigRevealS") then
        fnCutsceneMove("Nina", 14.75, 16.50)
    elseif(sObjectName == "BigRevealW") then
        fnCutsceneMove("Nina", 12.25, 16.50)
        fnCutsceneMove("Nina", 14.75, 16.50)
    else
        fnCutsceneMove("Nina", 17.25, 16.50)
        fnCutsceneMove("Nina", 14.75, 16.50)
    end
    fnCutsceneFace("Nina", 0, -1)
    fnCutsceneBlocker()
    fnCutsceneWait(25)
    fnCutsceneBlocker()
    
    -- |[Mannequin Dialogue]|
    if(iHasMannequin == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Countess", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Hm, perhaps it is best to put the bedroom upstairs.[SOFTBLOCK] Will my mannequin need a room of her own?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Maybe she could share mine...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Oh and I must hire a locksmith to fix the locks on the doors...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Movement]|
        fnCutsceneFace("Countess", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMove("Countess", 14.75, 15.50, 0.25)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Dialogue]|
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] It -[SOFTBLOCK] it can't be.[SOFTBLOCK] I'm -[SOFTBLOCK] oh my![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] [SOUND|Nina|Kiss]It's real![SOFTBLOCK] You're a human![SOFTBLOCK] Oh my goodness, is it a miracle?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I -[SOFTBLOCK] oh my goodness, I am so sorry![SOFTBLOCK] I - [BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] It's...[SOFTBLOCK] all right. Master.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Now cut that out.[SOFTBLOCK] You don't need to call me master any more![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] But you never told me your name.[SOFTBLOCK] What should I call you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Yarla.[SOFTBLOCK] Yarla Sciawin.[SOFTBLOCK] Please, call me Yarla.[SOFTBLOCK] I insist.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] It is such a relief to know my gas wore off, somehow.[SOFTBLOCK] I apologize for everything I did, I truly thought it was permanent.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] It...[SOFTBLOCK] was...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Clearly it was not.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] But it was![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Oh, I suppose this is yours, isn't it?[SOFTBLOCK] This is what you sent me to get.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] That little runestone is of no import to me![SOFTBLOCK] What matters is that you are well![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] But that's just it.[SOFTBLOCK] As soon as I picked this little thing up...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] There was a bright light, and here I am.[SOFTBLOCK] Back to my fleshy self.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Oh, and I dealt with those thugs like you said.[SOFTBLOCK] I didn't kill them.[SOFTBLOCK] But only because you said so.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] And you recovered the stone.[SOFTBLOCK] So to reiterate.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] A random beggar sells that to me, for a single platina.[SOFTBLOCK] A worthless trinket I buy out of charity.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] But then bandits break into my estate, and the dead rise.[SOFTBLOCK] And you drive them off, only to be transformed by this stone?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Their leader said it was important.[SOFTBLOCK] Dark forces wanted it.[SOFTBLOCK] I don't know if he was right or wrong, but he knew about it.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] To leave all this to coincidence would be a fool's choice.[SOFTBLOCK] This rock hides a great power.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yeah.[SOFTBLOCK] It transformed me, and it can transform me back, too.[SOFTBLOCK] But it will only work on me.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Are you certain?[SOFTBLOCK] Have you tried it?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] No, but...[SOFTBLOCK] it's like I always knew.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Then the stone was merely finding its way to you.[SOFTBLOCK] I was just an intermediary, as was the brigand, and the beggar.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] And perhaps you, too, are another such intermediary.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Maybe.[SOFTBLOCK] Either way, I want to be done with it.[SOFTBLOCK] Stay out of fate's way, that's what I think.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Heroes get eaten.[SOFTBLOCK] My mom always said that.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I doubt we have much choice in the matter.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[SOFTBLOCK] You're probably right.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] W-[SOFTBLOCK]well then![SOFTBLOCK] So you'll...[SOFTBLOCK] be off on your adventures, then?[SOFTBLOCK] Taking the stone wherever it leads you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] No way![SOFTBLOCK] You're putting that thing in your gallery![SOFTBLOCK] I don't want it![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Mas-[SOFTBLOCK] Miss Yarla.[SOFTBLOCK] I was...[SOFTBLOCK] wondering if you...[SOFTBLOCK] maybe need a hunter?[SOFTBLOCK] A cook?[SOFTBLOCK] A guard?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I really could use a steady meal and a place to stay.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Of course I need all those things and more![SOFTBLOCK] You're -[SOFTBLOCK] hired![SOFTBLOCK] Yes, on the spot![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] Really?[SOFTBLOCK] Thank you, master![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] ...[SOFTBLOCK] You keep calling me that.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Sorry.[SOFTBLOCK] I just...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I...[BLOCK]") ]])

        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Want to serve you.\", " .. sDecisionScript .. ", \"Work\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Want to be your mannequin again.\",  " .. sDecisionScript .. ", \"Mannequin\") ")
        fnCutsceneBlocker()
    
    -- |[Zombie Dialogue]|
    elseif(iHasZombie == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Countess", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Maybe I ought to put my bedroom upstairs.[SOFTBLOCK] No windows, though.[SOFTBLOCK] I might have to knock out a wall...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I wonder if...[SOFTBLOCK] that lovely zombie will want to...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Movement]|
        fnCutsceneFace("Countess", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMove("Countess", 14.75, 15.50, 0.25)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Dialogue]|
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] It -[SOFTBLOCK] it can't be.[SOFTBLOCK] What happened?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Actually, I was hoping you'd know more.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Were you not a banshee?[SOFTBLOCK] I kissed you![SOFTBLOCK] How could I be fooled by a disguise, I touched you![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] It wasn't a disguise.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] It's -[SOFTBLOCK] it's fine.[SOFTBLOCK] Not like I secretly lusted after a dangerous, powerful woman who would sweep me off my feet...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Excuse me?[SOFTBLOCK] I am still the most dangerous person you will ever meet![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Even though I didn't even eat any of those guys.[SOFTBLOCK] They smelled horrible![SOFTBLOCK] I didn't want to get sick, and I was dead![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Luckily they spent most of their time traipsing through my courtyard, so at least the smell won't linger.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yeah.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Oh![SOFTBLOCK] I got your stone back![SOFTBLOCK] It's what did this![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] As soon as I touched it, whoosh.[SOFTBLOCK] Back to my living self.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I take it I no longer need to pretend to be a vampire, then, as I am in no danger of being eaten.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Not consumed, but eaten?[SOFTBLOCK] I'm not so sure!)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] ...[SOFTBLOCK] Were you not going to respond?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Damn it![SOFTBLOCK] I hate being alive![SOFTBLOCK] I just think what I want to say and don't actually say it, because humans are cowards![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] I'm definitely going to eat you, just not in the way you thought![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Ha ha![SOFTBLOCK] I love the enthusiasm.[SOFTBLOCK] But first, that stone.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I received it for a pittance from a beggar.[SOFTBLOCK] It had no obvious magical properties, but I felt like I ought to display it.[SOFTBLOCK] Perhaps to show my charitable nature.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] And no sooner does it get stolen than you appear, and return it to me, transformed by its magic.[SOFTBLOCK] A coincidence?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Probably not.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] It's like I always knew what it was, what it could do, and it wasn't until I touched it that I remembered.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] You and the stone are connected.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Do you think it's fate?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I don't believe in fate.[SOFTBLOCK] It is merely the will of something very powerful, enough that the conventional laws of our reality are to be cast aside.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Well it's yours again, so I don't have to worry about it anymore.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Oh?[SOFTBLOCK] You don't intend to stay?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Countess, I intend to claim you as my own![SOFTBLOCK] We're going to screw every night until you can barely walk![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] You see, you didn't need to be a zombie to be bold.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Actually, I did.[SOFTBLOCK] See, dying made me realize that we don't get a lot of second chances.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Any day could be our last day, so I'm going to seize it.[SOFTBLOCK] Take what I want, make it count.[SOFTBLOCK] And if I was never a zombie, I would never have learned that.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] And -[SOFTBLOCK] I can become a zombie again if I want![SOFTBLOCK] I just need to use the runestone![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] And do you want to be a zombie?[BLOCK]") ]])
    
        --Decision script is this script. It must be surrounded by quotes.
        local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
        fnCutsceneInstruction([[ WD_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"I'll stay human.\", " .. sDecisionScript .. ", \"Human\") ")
        fnCutsceneInstruction(" WD_SetProperty(\"Add Decision\", \"Let's get dead!\",  " .. sDecisionScript .. ", \"Zombie\") ")
        fnCutsceneBlocker()
    
    -- |[Human Dialogue]|
    else
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Countess", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Upstairs would be less drafty, but there aren't any windows.[SOFTBLOCK] Perhaps I will need to knock through a wall.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Will I need quarters for...") ]])
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Movement]|
        fnCutsceneFace("Countess", 0, 1)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        fnCutsceneMove("Countess", 14.75, 15.50, 0.25)
        fnCutsceneBlocker()
        fnCutsceneWait(25)
        fnCutsceneBlocker()
        
        -- |[Dialogue]|
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        LM_ExecuteScript(gzFunctionPaths.sSetPartyDialogue)
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I was just -[SOFTBLOCK] thinking of you, Miss Nina![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Really?[SOFTBLOCK] I -[SOFTBLOCK] I got your stone back![SOFTBLOCK] Had to beat up a couple guys but they weren't a real problem.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Did you happen to have more of that bread on you?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Here you are.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] *chomp snarf*[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Miss Nina.[SOFTBLOCK] I am currently in need of several services.[SOFTBLOCK] Hunter, guard...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] I thought you'd never ask.[SOFTBLOCK] Sure![SOFTBLOCK] A nice place to sleep and a steady source of platina is what I've wanted for years![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I was not finished.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Oh?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I am a noble.[SOFTBLOCK] I have certain...[SOFTBLOCK] desires.[SOFTBLOCK] Desires that must be hidden for political reasons.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Like getting fisted?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Oh ho I -[SOFTBLOCK] what is fisting?[SOFTBLOCK] I mean, yes, yes, carnal desires.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I was wondering if you would like to provide such services?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] I'd love to be your girlfriend![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Unofficially![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Why does it have to be unofficial?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] We nobles must produce heirs to pass on wealth, or end feuds between families.[SOFTBLOCK] Having a non-noble lover is bad politically.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] It is unfortunate, but...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] It being a secret makes it hotter![BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] You're brash and very forward.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] It's what you like, right?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Absolutely.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] And our next meal must be more than soggy bread.[SOFTBLOCK] But first we must get my things out of the basement.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] How are we going to do that?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I know enough magic to warp things if I place a beacon down first.[SOFTBLOCK] I can show you, it's actually quite easy.[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I was thinking the first thing we'd move would be the bed...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] Right away, countess!") ]])
	
        fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
        fnCutsceneBlocker()
        fnCutsceneWait(120)
        fnCutsceneBlocker()

        --Dialogue.
        fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
        fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Smirk") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] (And that's how I ended up halfway across the continent and joining a noble family, even if unofficially.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] (Yarla and I restored the estate, took in some serfs, and put together a village.[SOFTBLOCK] We kept the runestone on the pedestal in the gallery and I barely looked twice at it.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] (In the end, she produced two boys and two girls to serve as heirs.[SOFTBLOCK] She didn't care much for the noblemen, but politics were politics.[SOFTBLOCK] They called me mommy and I raised them alongside her, as far as I'm concerned, they were my children too.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (But despite my youthful good looks, I was two decades her senior.[SOFTBLOCK] It's just how it is when you're an elf.[SOFTBLOCK] I took sick after our second daughter was born.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Something compelled me to still live, though.[SOFTBLOCK] As I lay in bed, we discussed our options.[SOFTBLOCK] Yarla suggested something.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (She had perfected the gas that transformed its victim into a mannequin.[SOFTBLOCK] Our estate was well protected by her various traps.[SOFTBLOCK] And it would save me from the sickness that plagued me.[SOFTBLOCK] I accepted.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Ironically, she did not make the same choice when her own time came.[SOFTBLOCK] I had been turned into a being of resin, untiring and unaging.[SOFTBLOCK] She chose instead to die a natural death.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (She told me to watch over our children, and to come find her in the next life whenever I tired of this one.[SOFTBLOCK] And she passed away.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (I oversaw our children, grandchildren, and great-grandchildren.[SOFTBLOCK] Eventually our family lost favour in the courts, the money dried up, and the great grandchildren married off into other households.[SOFTBLOCK] I was left alone.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (The estate fell into disrepair, and I stayed with the treasure gallery.[SOFTBLOCK] These were the things that I had with Yarla, our last memories.[SOFTBLOCK] I stayed with them and the world forgot about me.)[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Until you found me of course.[SOFTBLOCK] But that was my story.[SOFTBLOCK] The end.)") ]])
        
        --Execute the cleaner script.
        fnCutsceneInstruction([[ LM_ExecuteScript(gsNinaRoot .. "System/300 Cleanup.lua") ]])
        fnCutsceneBlocker()
        
        --Return to the "Nowhere" map.
        fnCutsceneInstruction([[ AL_BeginTransitionTo("Nowhere", "FORCEPOS:10.0x2.0x0") ]])
        fnCutsceneBlocker()
    
    end

-- |[ =================================== Mannequin Choices ==================================== ]|
-- |[Work For Her]|
elseif(sObjectName == "Work") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I want to serve you![SOFTBLOCK] I pledge my loyalty to you![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Well, yes, but I refuse to take you as a serf.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] If not a serf, perhaps a knight?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Heh heh ha![SOFTBLOCK] A knight?[SOFTBLOCK] I'm afraid you don't know how the laws of the western provinces work, do you?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Countess, I barely know the laws of the eastern provinces.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] No, no.[SOFTBLOCK] We can take serfs to work for us on a contract basis, but we cannot issue noble titles.[SOFTBLOCK] That is the sole province of the king.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I strongly doubt he'd allow a mongrel into his court, especially for someone of low standing like me.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Mongrel!?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I'm sorry, it is an actual legal term.[SOFTBLOCK] You are not a pure human.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] You don't allow elves to hold titles here?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I don't make the laws, the king does.[SOFTBLOCK] I am sorry.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yeah, not your fault.[SOFTBLOCK] I've heard it's the same in the eastern kingdoms.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] So then, what can I do to enter your service?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] How about...[SOFTBLOCK] consort?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] That sounds great![SOFTBLOCK] I'll be a consort![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] What's a consort?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Official lover.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] C-[SOFTBLOCK]countess![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Call me Yarla, please.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yarla, I don't know if I can be your consort...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Why not?[SOFTBLOCK] Do you not find yourself attracted to me?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] It isn't that![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] But aren't nobles supposed to produce heirs?[SOFTBLOCK] Pass on their bloodlines?[SOFTBLOCK] I obviously can't do that.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Of course you cannot, but my father and mother both have their own consorts.[SOFTBLOCK] Secretly, mind you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] They love each other, but platonically.[SOFTBLOCK] They are good friends, but not lovers.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Criminy, nobles![SOFTBLOCK] Everything is complicated![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I may marry someone, or not, but I can have you as a consort, always.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] The politics of it is...[SOFTBLOCK] dicey.[SOFTBLOCK] But seeing as I don't have any subjects here anyway...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I suppose consort will just have to be the way it is.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] And you shall be my guard and hunter, of course.[SOFTBLOCK] But you already wanted those jobs, and I'd say you are more than capable.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] But first -[SOFTBLOCK] you shall help me move![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] How are we going to get your things out of the basement?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I know how to warp them by placing a beacon.[SOFTBLOCK] I'll show you.[SOFTBLOCK] It's actually quite easy with some practice.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] And I was thinking...[SOFTBLOCK] the first thing we should put in...[SOFTBLOCK] is the bed.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] What, for sex?[SOFTBLOCK] Because if you've never had sex in the forest, you haven't lived.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Oh ho, I can't wait to![SOFTBLOCK] But for now, please indulge me, Nina.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] As you say, Countess Yarla!") ]])
	
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(120)
	fnCutsceneBlocker()

    --Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Smirk") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] (And that's how I ended up halfway across the continent and joining a noble family, even if unofficially.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] (Yarla and I restored the estate, took in some serfs, and put together a village.[SOFTBLOCK] We kept the runestone on the pedestal in the gallery and I barely looked twice at it.[SOFTBLOCK] Well, maybe once...[SOFTBLOCK] just to bring some spice to the bedroom.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] (Of course, she did have to produce heirs.[SOFTBLOCK] I wasn't jealous, because I knew she loved only me.[SOFTBLOCK] She told me the noblemen weren't as skilled as I was, anyway.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] (We had two sons and two daughters.[SOFTBLOCK] I raised them, they called me mother, they were just as much mine as hers.[SOFTBLOCK] I loved them dearly.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (But I took sick after our second daughter was born.[SOFTBLOCK] The doctors didn't know what it was.[SOFTBLOCK] Yarla was beside herself.[SOFTBLOCK] I was twenty years her senior even if I didn't look it.[SOFTBLOCK] I suppose it was the curse of being an elf.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (But she approached me and told me there was a way.[SOFTBLOCK] Mannequins didn't get sick.[SOFTBLOCK] I told her I couldn't, but she told me I must.[SOFTBLOCK] If not for her, then to not rob our beloved children of their mother.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (I took the runestone again and it worked.[SOFTBLOCK] I transformed back into a mannequin, and the mantra came right back.[SOFTBLOCK] It flooded me, but I didn't mind.[SOFTBLOCK] I loved Yarla, the mantra wasn't even necessary.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (But Yarla eventually grew old and would pass away.[SOFTBLOCK] We talked about her choices, and she chose to die.[SOFTBLOCK] We had both lived full, happy lives.[SOFTBLOCK] I had chosen differently, but we each made our decisions.[SOFTBLOCK] I would watch over our children for both our sakes.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (And then I watched over our grandchildren, and our great grandchildren.[SOFTBLOCK] Yarla had told me that, when I eventually died in whatever way a mannequin can die, I was to find her in the afterlife.[SOFTBLOCK] I told her I would, so we could be together again.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (But our line eventually lost favour in the courts.[SOFTBLOCK] The money ran out, and our descendents joined other houses or died of disease or war before leaving heirs.[SOFTBLOCK] Eventually, it was just me.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (We had moved the gallery of treasures into the basement, and I stood there with the things we had made together.[SOFTBLOCK] Standing guard, I suppose.[SOFTBLOCK] Never far from that runestone.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (The estate withered and collapsed, forgotten.[SOFTBLOCK] The stairs collapsed, the house fell, and we were forgotten.[SOFTBLOCK] Until you found me, that is.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (But that is my story.[SOFTBLOCK] The end.)") ]])
	
    --Execute the cleaner script.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsNinaRoot .. "System/300 Cleanup.lua") ]])
	fnCutsceneBlocker()
    
    --Return to the "Nowhere" map.
    fnCutsceneInstruction([[ AL_BeginTransitionTo("Nowhere", "FORCEPOS:10.0x2.0x0") ]])
    fnCutsceneBlocker()

-- |[Mannequin Again]|
elseif(sObjectName == "Mannequin") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")
    
    --Variables.
    local iToldOfLove = VM_GetVar("Root/Variables/NinaMod/MainQuest/iToldOfLove", "N")
	
	--Reboot the dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I want to be your mannequin again![SOFTBLOCK] Please, master![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I -[SOFTBLOCK] I forbid it![SOFTBLOCK] How could you want that?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] You showed me more kindness than anyone I've ever met before![SOFTBLOCK] You even told me to speak my mind to you![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] And the mantra takes all the anxiety away.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] But your family![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] I'll send them a letter.[SOFTBLOCK] The priest can read it to them.[SOFTBLOCK] If they visit -[SOFTBLOCK] we use the runestone again![BLOCK][CLEAR]") ]])
    
    --Read the love topic:
    if(iToldOfLove == 1.0) then
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] And your lover?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Um...[SOFTBLOCK] Well...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I told you I had fallen in love before...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Yes.[SOFTBLOCK] With a woman.[SOFTBLOCK] What of her?[SOFTBLOCK] Are you still together?[SOFTBLOCK] Does she love you back?[BLOCK][CLEAR]") ]])
        
    --Did not:
    else
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] And others?[SOFTBLOCK] Friends, lovers?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I did fall in love once...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] With who?[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] A beautiful woman...[BLOCK][CLEAR]") ]])
        fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Are you still together?[SOFTBLOCK] Does she love you back?[BLOCK][CLEAR]") ]])
    end
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I don't know if she does...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Then I cannot allow you to pledge yourself to me if - [BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] The woman was you, master.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] But [SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] M-[SOFTBLOCK]maybe it's not real love, but I feel it.[SOFTBLOCK] Do you feel that way to me?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] To fall in love after only a few moments...[SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Heavens, I'm every bit as flighty as my younger sister![SOFTBLOCK] She fell in love four times![SOFTBLOCK] Four![SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] So that's a yes?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Her relationships went poorly, I assure you![SOFTBLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] And are you smarter than she is?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Not if I make the same mistakes![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] What if I commit myself to you only for us to hate each other?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] Then we use the runestone![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Blast![SOFTBLOCK] I suppose I cannot talk you out of this.[SOFTBLOCK] Or myself.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] But, I have a condition.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] If I decide I want to return to human form, you will not forbid me from doing it.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] And if circumstances force us to part with it, then I will be afforded the chance to return to human form before we do.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] So long as I remain a mannequin, it will be by my choice.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I see.[SOFTBLOCK] Yes, I will agree to that.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Well, then, watch this!") ]])
    fnCutsceneBlocker()

    -- |[Execute Transformation]|
    --Wait a bit.
    fnCutsceneWait(30)
    fnCutsceneBlocker()

    --Flash the active character to white. Immediately after, execute the transformation.
    Cutscene_CreateEvent("Flash Nina White", "Actor")
        ActorEvent_SetProperty("Subject Name", "Nina")
        ActorEvent_SetProperty("Flashwhite Quickly")
    DL_PopActiveObject()
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ LM_ExecuteScript(gsNinaRoot .. "FormHandlers/Nina/Form_Mannequin.lua") ]])
    fnCutsceneWait(gci_Flashwhite_Ticks_Total)
    fnCutsceneBlocker()

    --Now wait a little bit.
    fnCutsceneWait(30)
    fnCutsceneBlocker()

    --Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Ngh...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] What a sight to behold.[SOFTBLOCK] Nina?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I am a mannequin.[SOFTBLOCK] Loyalty undying.[SOFTBLOCK] Serve the master.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] T-[SOFTBLOCK]talk to me like you were a moment ago![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Must.[SOFTBLOCK] I?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] This is how I prefer to be.[SOFTBLOCK] My thoughts are simple.[SOFTBLOCK] You are my whole world.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I see.[SOFTBLOCK] What if I need you to give me advice?[SOFTBLOCK] Guide me?[SOFTBLOCK] B-[SOFTBLOCK]be my partner in life?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I am.[SOFTBLOCK] I will guide you, master.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Faithfully, until the end of time.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I...[SOFTBLOCK] I accept, then.[SOFTBLOCK] Mannequin, be my lover, my partner, my guardian, and my servant.[SOFTBLOCK] I order you to.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I order you to love me with all your heart![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[SOFTBLOCK] I love you with all my heart.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Oh I'm fluttering![SOFTBLOCK] We -[SOFTBLOCK] help me warp my bed into the upstairs room![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.") ]])
    fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(120)
	fnCutsceneBlocker()

    --Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (And that's how I ended up a mannequin and lover of Countess Yarla.[SOFTBLOCK] Incredible story, right?)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (We lived together happily for many years.[SOFTBLOCK] I helped her with everything, watched over her as she slept, and gave her what advice I could.[SOFTBLOCK] I was certainly no statesman, but my moral guidance mattered to her.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (The time eventually came for her to produce an heir.[SOFTBLOCK] She had two boys and two girls with several noblemen.[SOFTBLOCK] She did not care for those men, but politics was politics.[SOFTBLOCK] It did not affect me in the slightest, for I knew she only loved me.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (I helped raise the children.[SOFTBLOCK] They saw me as their auntie, and in a way, I suppose I was.[SOFTBLOCK] They grew up just as wise as their mother.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (And when the time came, master gave me as a gift to her favoured daughter.[SOFTBLOCK] I would outlive them all, of course.[SOFTBLOCK] Master could have become a partirhuman of her choice, but she chose not to.[SOFTBLOCK] She had no regrets, had lived a full life, and told me to come find her in the next life.[SOFTBLOCK] I told her I would.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (The runestone stayed with us.[SOFTBLOCK] I only chose to be human again a few times, and never for long.[SOFTBLOCK] To show my inheritors what I truly was.[SOFTBLOCK] A mannequin who chose to be that way.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (But, one of master's great grandsons inherited me, and produced no heir.[SOFTBLOCK] He died in a campaign far from the estate while I was charged with watching over it.[SOFTBLOCK] The estate fell into disrepair.[SOFTBLOCK] I learned that the family had fallen out of favour.[SOFTBLOCK] The money ran out.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (And so with no one to call master, I stayed in the decaying ruin with the stone.[SOFTBLOCK] We were in the basement then, and the process of time collapsed the roof over us.[SOFTBLOCK] Eventually, we were forgotten.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Until you found me, I suppose.[SOFTBLOCK] But that is my story.[SOFTBLOCK] The end.)") ]])
	
    --Execute the cleaner script.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsNinaRoot .. "System/300 Cleanup.lua") ]])
	fnCutsceneBlocker()
    
    --Return to the "Nowhere" map.
    fnCutsceneInstruction([[ AL_BeginTransitionTo("Nowhere", "FORCEPOS:10.0x2.0x0") ]])
    fnCutsceneBlocker()
    
-- |[ ===================================== Zombie Choices ===================================== ]|
elseif(sObjectName == "Human") then

	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

    --Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] Human is fine, for now![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] But maybe later...[SOFTBLOCK] if you like the cold touch...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] It is you that I want.[SOFTBLOCK] Dead or alive.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] And yet I do not even know your name.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Nina.[SOFTBLOCK] Nina Avarro, of Tserro.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Yarla Sciawin of Jeffespeir.[SOFTBLOCK] Countess Yarla Sciawin, that is.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] So, Yarla.[SOFTBLOCK] Would you have me, officially, as your guard, and hunter?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Guard, hunter, and consort.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] What's a consort?[SOFTBLOCK] Is that like a diplomat?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Lover.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] Oh, so fuck-buddy![SOFTBLOCK] Not married, but you're still getting railed every night![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] I don't have a lot of practice with my tongue but I swear I'll learn![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Being dead has brought an unending passion to you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] I swear you'll never find yourself lying awake at night.[SOFTBLOCK] You'll be too tired![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Well then, my consort, our first task is to reassemble my bedroom.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I can place beacons to warp my things out of the basement.[SOFTBLOCK] Come, I'll show you how.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Great![SOFTBLOCK] And then I'll rustle up something for dinner![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] I was thinking...[SOFTBLOCK] shellfish.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] ...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Excuse me?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] The clitoris is kind of like a clam.[SOFTBLOCK] You know?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Ha ha ha oh![SOFTBLOCK] Oh my, and I'll hire you as jester as well![BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] As you say, countess!") ]])
    fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(120)
	fnCutsceneBlocker()

    --Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (And that's how I ended up lover of Countess Yarla.[SOFTBLOCK] It's all true, I swear.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (I actually wound up spending some of my time as an elf, and some of it as a zombie.[SOFTBLOCK] I even spent a few years as an alraune after I convinced the local coven to have me.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Yarla and I lived happily together.[SOFTBLOCK] I helped her with everything, watched over her as she slept, and brutally murdered her enemies.[SOFTBLOCK] Turns out nobles get attacked by assassins a lot, but that drops off once you eat a few of them.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (I became a zombie again just to try out eating people.[SOFTBLOCK] I don't recommend it, it's all hype.[SOFTBLOCK] The meat is like chicken but doesn't have that sort of taste the crispy parts get.[SOFTBLOCK] Not worth the effort.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Now, Yarla was a noble, and that meant she had to pass on the bloodline.[SOFTBLOCK] She had two boys and two girls across four noblemen.[SOFTBLOCK] They declined my threesome offers.[SOFTBLOCK] Can't imagine why.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (I helped raise the children.[SOFTBLOCK] They called me auntie, but that was just because of the politics of it.[SOFTBLOCK] They saw me as their mother, and really, I was.[SOFTBLOCK] I was there even when the countess couldn't be.[SOFTBLOCK] They grew up strong and wise.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (But Yarla did not want to become undead, or a plant, or a statue, or anything of the sort.[SOFTBLOCK] She eventually grew old.[SOFTBLOCK] I eventually became a zombie full time, to ward off my encroaching age.[SOFTBLOCK] Disease did not touch me, but it did eventually claim her.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Instead, I stayed on and watched over our children, and grandchildren, and great-grandchildren.[SOFTBLOCK] I took on an aura of mystery to the public, a wise old elf said to be thousands of years old.[SOFTBLOCK] I kept the whole zombie thing a secret.[SOFTBLOCK] Take frequent baths, that's my tip.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (The runestone stayed with us, but I never cast a second glance at it.[SOFTBLOCK] I suppose I could have become a human or plant at any time I wanted, but I just never wanted to.[SOFTBLOCK] With Yarla gone, there was no point.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Eventually, the family married off to other houses, or the youngest ones died in wars or plagues.[SOFTBLOCK] I had seen several generations through, and while Yarla's bloodline continues elsewhere, it left my care.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (We moved the treasures into the basement, and I made a tomb for myself there.[SOFTBLOCK] I could spend as much time as I liked asleep, or as asleep as the dead get.[SOFTBLOCK] You just sort of go to sleep and wake up whenever you like, even a century later.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (I took to sleeping for a few years at a time, just keeping an eye on our treasures.[SOFTBLOCK] They were our legacy, and the estate collapsed and was forgotten.[SOFTBLOCK] They stayed with me down in that hole. I felt like I was keeping Yarla's memory that way.[SOFTBLOCK] The things we shared stayed with me.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (And that's about it for my story with that runestone.[SOFTBLOCK] The end.)") ]])
	
    --Execute the cleaner script.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsNinaRoot .. "System/300 Cleanup.lua") ]])
	fnCutsceneBlocker()
    
    --Return to the "Nowhere" map.
    fnCutsceneInstruction([[ AL_BeginTransitionTo("Nowhere", "FORCEPOS:10.0x2.0x0") ]])
    fnCutsceneBlocker()

elseif(sObjectName == "Zombie") then
	
	--Clean up remaining scene parts.
	WD_SetProperty("Hide")

    -- |[Execute Transformation]|
    --Wait a bit.
    fnCutsceneWait(30)
    fnCutsceneBlocker()

    --Flash the active character to white. Immediately after, execute the transformation.
    Cutscene_CreateEvent("Flash Nina White", "Actor")
        ActorEvent_SetProperty("Subject Name", "Nina")
        ActorEvent_SetProperty("Flashwhite Quickly")
    DL_PopActiveObject()
    fnCutsceneWait(5)
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ LM_ExecuteScript(gsNinaRoot .. "FormHandlers/Nina/Form_Zombie.lua") ]])
    fnCutsceneWait(gci_Flashwhite_Ticks_Total)
    fnCutsceneBlocker()

    --Now wait a little bit.
    fnCutsceneWait(30)
    fnCutsceneBlocker()

    --Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Countess", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] Asked and answered, countess![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Now hold on just a moment, miss...[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Cripes, I didn't tell you my name?[SOFTBLOCK] Nina.[SOFTBLOCK] Nina Avarro, of Tserro.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Yarla Sciawin of Jeffespeir.[SOFTBLOCK] Countess Yarla Sciawin, that is.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] Never heard of you![SOFTBLOCK] No offense.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] None taken.[SOFTBLOCK] But Nina...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Are you sure about this?[SOFTBLOCK] Becoming a consort to a noble house requires much of you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Like what?[SOFTBLOCK] Do I have to dress all fancy?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] No eating people without my permission.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I'm not very good at eating people, actually.[SOFTBLOCK] I think it was just a thrill of the moment thing.[SOFTBLOCK] Why would people taste any better than a boar, you know?[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] So what else would a consort need to do?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Satisfy my sexual desires.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] When you make your hobby your job, you never work a day in your life![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Oh my![SOFTBLOCK] Powerful, beautiful, and passionate![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I would also like you to serve as a guard, given your skillset.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] Nobody will harm my lady![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Ah -[SOFTBLOCK] oh I feel like I'm going too fast here.[SOFTBLOCK] We just met and I've already declared you my consort.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] You didn't have much of a choice.[SOFTBLOCK] Nobody can resist my charms.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] By the way, why'd you pretend to be a vampire?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I literally thought you were going to eat me and said the first thing I could think of.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I hope you've learned not to judge an elf by the color of their skin, but rather by how much they want to finger you.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] So what do we do now?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I only just arrived.[SOFTBLOCK] We'll need to restore this estate, establish a village, get some serfs to work the land on contract...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Unless you meant right this second.[SOFTBLOCK] In which case, we need to get my bedroom assembled.[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Smirk] I absolutely love the way you think.") ]])
    fnCutsceneBlocker()
	
	fnCutsceneInstruction([[ AL_SetProperty("Activate Fade", 45, gci_Fade_Under_GUI, true, 0, 0, 0, 0, 0, 0, 0, 1) ]])
	fnCutsceneBlocker()
	fnCutsceneWait(120)
	fnCutsceneBlocker()

    --Dialogue.
	fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Major Sequence Fast", true) ]])
	fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (And that's how I ended up a zombie and lover of Countess Yarla.[SOFTBLOCK] It's all true, I swear.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (We lived together happily for many years.[SOFTBLOCK] I helped her with everything, watched over her as she slept, and brutally murdered her enemies.[SOFTBLOCK] Turns out nobles get attacked by assassins a lot, but that drops off once you eat a few of them.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Yes, I did eventually get to eat some people.[SOFTBLOCK] They taste a bit like chicken, except not as tender.[SOFTBLOCK] It's not very pleasant, and I think the only reason other zombies do it is because their brains are too rotted to care.[SOFTBLOCK] Not me, I stuck to what I could hunt from the forest.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Now, Yarla was a noble, and that meant she had to pass on the bloodline.[SOFTBLOCK] She had two boys and two girls across four noblemen.[SOFTBLOCK] They declined my threesome offers.[SOFTBLOCK] Can't imagine why.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (I helped raise the children.[SOFTBLOCK] They called me auntie, but that was just because of the politics of it.[SOFTBLOCK] They saw me as their mother, and really, I was.[SOFTBLOCK] I was there even when the countess couldn't be.[SOFTBLOCK] They grew up strong and wise.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (But Yarla did not want to become a zombie like me, or anything else.[SOFTBLOCK] She eventually grew old while I stayed young and undead.[SOFTBLOCK] It broke my unbeating heart, but it was her choice.[SOFTBLOCK] She passed away peacefully, after telling me to come see her in the next life whenever I tired of this one.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Instead, I stayed on and watched over our children, and grandchildren, and great-grandchildren.[SOFTBLOCK] I took on an aura of mystery to the public, a wise old elf said to be thousands of years old.[SOFTBLOCK] I kept the whole zombie thing a secret.[SOFTBLOCK] Take frequent baths, that's my tip.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (The runestone stayed with us, but I never cast a second glance at it.[SOFTBLOCK] I suppose I could have become a human at any time I wanted, but I just never wanted to.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (Eventually, the family married off to other houses, or the youngest ones died in wars or plagues.[SOFTBLOCK] I had seen several generations through, and while Yarla's bloodline continues elsewhere, it left my care.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (We moved the treasures into the basement, and I made a tomb for myself there.[SOFTBLOCK] I could spend as much time as I liked asleep, or as asleep as the dead get.[SOFTBLOCK] You just sort of go to sleep and wake up whenever you like, even a century later.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (I took to sleeping for a few years at a time, just keeping an eye on our treasures.[SOFTBLOCK] They were our legacy, and the estate collapsed and was forgotten.[SOFTBLOCK] They stayed with me down in that hole. I felt like I was keeping Yarla's memory that way.[SOFTBLOCK] The things we shared stayed with me.)[BLOCK][CLEAR]") ]])
	fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (And that's about it for my story with that runestone.[SOFTBLOCK] The end.)") ]])
	
    --Execute the cleaner script.
	fnCutsceneInstruction([[ LM_ExecuteScript(gsNinaRoot .. "System/300 Cleanup.lua") ]])
	fnCutsceneBlocker()
    
    --Return to the "Nowhere" map.
    fnCutsceneInstruction([[ AL_BeginTransitionTo("Nowhere", "FORCEPOS:10.0x2.0x0") ]])
    fnCutsceneBlocker()
end
