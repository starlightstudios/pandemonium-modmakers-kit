-- |[ ========================================= Harrow ========================================= ]|
-- |[Description]|
--Deals terrifying damage equal to 1.20 of user's attack power over 3 turns.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectBansheeHarrow == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Terrifying, 3, 1.20)
    zEffectStruct.sDisplayName = "Harrow"
    zEffectStruct.sIcon = "Mei|Rend"
    
    --Store
    gzEffectBansheeHarrow = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefAbility = gzEffectBansheeHarrow

--Call the standard handler. It will nil off the gzRefAbility.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
