-- |[ ========================================= Harrow ========================================= ]|
-- |[Description]|
--Deals terrifying damage equal to 2.00 of user's attack power over 5 turns.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectBansheeHarrowCrit == nil) then
    
    --Base, overrides.
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Terrifying, 5, 2.00)
    zEffectStruct.sDisplayName = "Harrow"
    zEffectStruct.sIcon = "Mei|Rend"
    
    --Store
    gzEffectBansheeHarrowCrit = zEffectStruct
    
end

-- |[Set and Execute]|
--Standardized Naming
gzRefAbility = gzEffectBansheeHarrowCrit

--Call the standard handler. It will nil off the gzRefAbility.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
