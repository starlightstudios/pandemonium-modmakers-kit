-- |[ ========================================= Guard ========================================== ]|
-- |[Description]|
--Increases protection and evade.

-- |[Arguments]|
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--These variables are used to auto-generate the properties of the ability when the ability is a
-- "standard" case. If you want to deviate from the standard, set things manually.
--Because the struct is global, it needs to have a unique name.
if(gzNinaCommonGuard == nil) then
    
    -- |[System]|
    --Setup.
    local zAbiStruct = {}

    --Display/System Variables
    zAbiStruct.sJobName      = "Common"
    zAbiStruct.sSkillName    = "Guard"
    zAbiStruct.sInternalName = zAbiStruct.sJobName .. "Internal"
    zAbiStruct.iJPUnlockCost = gciJP_Cost_Normal
    zAbiStruct.sIconBacking  = gsAbility_Backing_Buff
    zAbiStruct.sIconFrame    = gsAbility_Frame_Active
    zAbiStruct.sIconPath     = "Root/Images/AdventureUI/Abilities/Defend"
    zAbiStruct.sCPIcon       = "Null"
    zAbiStruct.iResponseType = gciAbility_ResponseStandard_DirectAction
    
    -- |[Description]|
    zAbiStruct.sDescriptionMarkdown = "+10 [Prt] for 1 [Turns].\n+30 [Evd] for 1 [Turns].\n\n\n\n\n[Costs]"
    zAbiStruct.sDescription = ""
    zAbiStruct.saImages = {}
    
    -- |[Simplified Description]|
    zAbiStruct.sSimpleDescMarkdown = "Keep your distance and protect yourself.\n\n\n\nIncreases [Prt](Protection) greatly.\nIncreases [Evd](Evade)."
    zAbiStruct.sSimpleDesc = ""
    zAbiStruct.saSimpleImages = {}

    -- |[Usability Variables]|
    zAbiStruct.bAlwaysAvailable = false  --Set to true to ignore all requirements.
    zAbiStruct.iRequiredMP = 0           --MP required to use the ability.
    zAbiStruct.iRequiredCP = 0           --CP required to use the ability.
    zAbiStruct.bRespectsCooldown = true  --If this ability has a cooldown, decrements it.
    zAbiStruct.iCooldown = 0             --Cooldown, in turns, before the ability is usable again. 1 prevents stacking of free actions, otherwise start at 2.
    zAbiStruct.bIsFreeAction = false     --Flags as a free action.
    zAbiStruct.iRequiredFreeActions = 0  --Free actions set this as 1, everything else as 0.
    zAbiStruct.bRespectActionCap = false --Characters with multiple free actions have a hard action cap. This flag checks that cap.

    --Targeting
    zAbiStruct.sTargetMacro = "Target Self"

    --Effects
    zAbiStruct.iCPGain = 1
    
    -- |[Prediction Package]|
    --Create basic package.
    zAbiStruct.zPredictionPackage = fnCreatePredictionPack()
    
    --Create an Effect module
    zAbiStruct.zPredictionPackage.zaEffectModules[1] = fnCreateEffectModule()
    zAbiStruct.zPredictionPackage.zaEffectModules[1].bIsBuff = true
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectType = gciDamageType_Piercing
    zAbiStruct.zPredictionPackage.zaEffectModules[1].iEffectStrength = 1000
    zAbiStruct.zPredictionPackage.zaEffectModules[1].sResultScript = "+10 [IMG1], +30 [IMG2] / 1[IMG3]"
    zAbiStruct.zPredictionPackage.zaEffectModules[1].saResultImages = {"Root/Images/AdventureUI/StatisticIcons/Protection", "Root/Images/AdventureUI/StatisticIcons/Evade", "Root/Images/AdventureUI/DebuffIcons/Clock"}
    
    -- |[Execution Ability Package]|
    zAbiStruct.zExecutionAbiPackage = fnConstructDefaultAbilityPackage("Buff")
    zAbiStruct.zExecutionAbiPackage.bDoesNoDamage = true
    zAbiStruct.zExecutionAbiPackage.bDoesNoStun = false
    zAbiStruct.zExecutionAbiPackage.bAlwaysHits = true
    zAbiStruct.zExecutionAbiPackage.bEffectAlwaysApplies = true
    
    -- |[Execution Effect Package]|
    --Basic package.
    zAbiStruct.zExecutionEffPackage = fnConstructDefaultEffectPackage()
    
    --Apply overrides.
    zAbiStruct.zExecutionEffPackage.sEffectPath     = fnResolvePath() .. "../Effects/Guard.lua"
    zAbiStruct.zExecutionEffPackage.sEffectCritPath = fnResolvePath() .. "../Effects/Guard.lua"
    zAbiStruct.zExecutionEffPackage.sApplyText      = "Guarding!"
    zAbiStruct.zExecutionEffPackage.sApplyTextCol   = "Color:Blue"
    zAbiStruct.zExecutionEffPackage.sApplySound     = "Combat\\|Impact_Buff"
    
    -- |[Run Markdown Handler]|
    fnMarkdownHandlerAbility(zAbiStruct)
    
    -- |[Finalize]|
    --Copy.
    gzNinaCommonGuard = zAbiStruct
end

--Because this is a prototype, we set the gzRefAbility to point to the unique structure. This way,
-- all the activity below points to the gzRefAbility's values and we don't need to change the structure's
-- name to create unique names for everything.
--The gsStandardAbilityPath handler also operates on gzRefAbility if it is called. You can use manual
-- overrides to make unique abilities.
gzRefAbility = gzNinaCommonGuard

--Execute.
LM_ExecuteScript(gsStandardAbilityPath, iSwitchType)
