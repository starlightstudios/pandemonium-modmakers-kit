-- |[ ================================== Enemy Pulse Response ================================== ]|
--When the function fnStandardEnemyPulse() is called, the game is checking which enemies should
-- ignore the player based on what form they are in. It handles the default code, and then checks
-- if any of the mods caught the results if an unhandled chapter is found. This script will be
-- called by the pulse.

-- |[Mod Activity Check]|
--If the mod is not active, don't do any of the below.
if(OM_IsModActive("Nina's Adventure") == false) then return end

--Get the chapter variable. Nina's Adventure is chapter 10.
local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")
if(iCurrentChapter == 10.0) then
    gbModHandledCall = true
    gbStopModExec = true
    gsModPlayerForm = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")
end
