-- |[ ===================================== Paragon Handler ==================================== ]|
--Called whenever an enemy spawns, flags whether or not that enemy should be a paragon.
--Paragons are tougher versions of the base enemy, with 10x stats and XP/JP/Platina. The program
-- will append "Paragon" to the appearance and enemy alias when spawning, the scripts do the rest.
--If no value is set, the default is to not become a paragon.
if(fnArgCheck(1) == false) then return end
local sGrouping = LM_GetScriptArgument(0)
local sSpawnName = LM_GetScriptArgument(1)

-- |[Special]|
--If the grouping starts with "ALWAYS", then this enemy is the "trigger" enemy for paragons to spawn.
local bDespawnIfUnready = false
if(string.sub(sGrouping, 1, 6) == "ALWAYS") then
    bDespawnIfUnready = true
    sGrouping = string.sub(sGrouping, 8)
end

-- |[Debug]|
--These flags can be set to always or never have paragons. The Never flag outweighs the Always flag.
local iAlwaysParagon = VM_GetVar("Root/Variables/Global/Paragons/iAlwaysParagon", "N")
local iNeverParagon  = VM_GetVar("Root/Variables/Global/Paragons/iNeverParagon", "N")
if(iNeverParagon == 1.0) then return end

-- |[ ===================================== Grouping Check ===================================== ]|
--Figure out which grouping it belongs to. The grouping is in "Root/Variables/Paragons/GROUPING/"
-- and has two variables: iKillCount and iHasDefeatedParagon.
local iIsEnabled          = VM_GetVar("Root/Variables/Paragons/"..sGrouping.."/iIsEnabled", "N")
local iKillCount          = VM_GetVar("Root/Variables/Paragons/"..sGrouping.."/iKillCount", "N")
local iKillsNeeded        = VM_GetVar("Root/Variables/Paragons/"..sGrouping.."/iKillsNeeded", "N")
local iHasDefeatedParagon = VM_GetVar("Root/Variables/Paragons/"..sGrouping.."/iHasDefeatedParagon", "N")

--If the enabled flag is false, the paragon never spawns. The player can turn them off this way.
if(iIsEnabled == 0.0 and iAlwaysParagon == 0.0) then
    
    --If this is the ALWAYS enemy, then it needs to despawn.
    if(bDespawnIfUnready) then
        AdvCombat_SetProperty("Set Paragon Value", gciAC_Paragon_Despawn)
    end
    return

--If the kill count is below the threshold, paragons do not spawn.
elseif(iKillCount < iKillsNeeded and iAlwaysParagon == 0.0) then
    
    --If this is the ALWAYS enemy, then it needs to despawn.
    if(bDespawnIfUnready) then
        AdvCombat_SetProperty("Set Paragon Value", gciAC_Paragon_Despawn)
    end
    return

--Kill count is above the threshold.
else

    --If the ALWAYS paragon has not been defeated yet, normal paragons do not spawn.
    if(iHasDefeatedParagon == 0.0 and iAlwaysParagon == 0.0) then

        --If we are the ALWAYS paragon, spawn:
        if(bDespawnIfUnready) then
            AdvCombat_SetProperty("Set Paragon Value", gciAC_Paragon_Yes)
        end
        return
    
    --The ALWAYS paragon was defeated, so normal enemies have a 10% chance of being paragons.
    -- This is rerolled every time the player rests.
    else
    
        --The ALWAYS paragon never respawns.
        if(bDespawnIfUnready) then
            AdvCombat_SetProperty("Set Paragon Value", gciAC_Paragon_Despawn)
            return
        end
        
        --If this flag is set, skip the rolls and just make the enemy a paragon.
        if(iAlwaysParagon == 1.0) then
            AdvCombat_SetProperty("Set Paragon Value", gciAC_Paragon_Yes)
            return
        end
    
        --Get the base paragon roll, this seeds the generator.
        local iParagonRoll = VM_GetVar("Root/Variables/Global/Paragons/iParagonRoll", "N")
        local iOffsetRoll = 0
    
        --Add all the letter values from the name to the offset roll.
        for i = 1, string.len(sSpawnName), 1 do
            iOffsetRoll = iOffsetRoll + string.byte(string.sub(sSpawnName, i, i))
        end
        local fRoll = STPerlin_Quickroll(iParagonRoll, iOffsetRoll)
    
        --The value is going to be between -1.0 and 1.0. There is a 20% chance of becoming a paragon, therefore, the value
        -- needs to be -0.60 or lower.
        if(fRoll <= -0.60) then
            AdvCombat_SetProperty("Set Paragon Value", gciAC_Paragon_Yes)
        end
    end
end
