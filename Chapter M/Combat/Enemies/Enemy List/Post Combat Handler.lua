-- |[ =================================== Post Combat Handler ================================== ]|
--Script called by combat whenever it ends. It passes in the combat state (victory, defeat, retreat, surrender)
-- and the name of the enemy in question.
--This is primarily used to track kills for paragon reasons.
if(fnArgCheck(1) == false) then return end
local iEndingType = LM_GetScriptArgument(0, "N")

--Debug.
Debug_PushPrint(false, "=========== Running post-combat handler ==========\n")

-- |[ ========================================= Victory ======================================== ]|
if(iEndingType == gciAC_Resolution_Victory) then
    
    -- |[Setup]|
    --Scan the graveyard and track all the groupings of the defeated enemies.
    local iGraveyardSize = AdvCombat_GetProperty("Entities in Graveyard")
    Debug_Print("Enemies in graveyard: " .. iGraveyardSize .. "\n")
    
    -- |[Scan]|
    --Iterate.
    for i = 0, iGraveyardSize-1, 1 do
        
        --Get ID and push.
        local iEntityID = AdvCombat_GetProperty("ID of Graveyard Entity", i)
        AdvCombat_SetProperty("Push Entity By ID", iEntityID)
        
            -- |[Paragon Handling]|
            --Get the associated paragon grouping.
            local sParagonGrouping = AdvCombatEntity_GetProperty("Paragon Grouping")
            
            --If the grouping starts with ALWAYS, this is the unique paragon.
            local bIsUniqueParagon = false
            if(string.sub(sParagonGrouping, 1, 6) == "ALWAYS") then
                bIsUniqueParagon = true
                sParagonGrouping = string.sub(sParagonGrouping, 8)
            end
            
            --Locate the grouping and get its values.
            local iKillCount          = VM_GetVar("Root/Variables/Paragons/"..sParagonGrouping.."/iKillCount", "N")
            local iHasDefeatedParagon = VM_GetVar("Root/Variables/Paragons/"..sParagonGrouping.."/iHasDefeatedParagon", "N")
            
            --Increment kills by 1.
            VM_SetVar("Root/Variables/Paragons/"..sParagonGrouping.."/iKillCount", "N", iKillCount + 1)
        
            --If this was the unique paragon, mark it as killed.
            if(bIsUniqueParagon) then
                VM_SetVar("Root/Variables/Paragons/"..sParagonGrouping.."/iHasDefeatedParagon", "N", 1.0)
            end
        
            --Statistics:
            Debug_Print("==> Enemy ID: " .. iEntityID .. "\n")
            Debug_Print("    Enemy Display Name: " .. AdvCombatEntity_GetProperty("Display Name") .. "\n")
            Debug_Print("    Paragon Grouping: " .. sParagonGrouping .. "\n")
            Debug_Print("    Kills in this group: " .. iKillCount + 1 .. "\n")
            
            -- |[Subquest: Mannequin Beat the Bandits]|
            local sNinaForm = VM_GetVar("Root/Variables/Global/Nina/sForm", "S")
            if(sNinaForm == "Mannequin") then
            
                local sEnemyDisplayName = AdvCombatEntity_GetProperty("Display Name")
                local iMannequinBanditsDefeated = VM_GetVar("Root/Variables/NinaMod/MainQuest/iMannequinBanditsDefeated", "N")
                local iSawMannequinDriveOffInvaders = VM_GetVar("Root/Variables/NinaMod/MainQuest/iSawMannequinDriveOffInvaders", "N")
                if(iSawMannequinDriveOffInvaders == 1.0) then
            
                    --Check if the enemy's display name is any of the bandit names.
                    if(sEnemyDisplayName == "Brigand Warrior" or sEnemyDisplayName == "Brigand Thief" or sEnemyDisplayName == "Brigand Leader") then
                        
                        --Increment and save.
                        iMannequinBanditsDefeated = iMannequinBanditsDefeated + 1
                        VM_SetVar("Root/Variables/NinaMod/MainQuest/iMannequinBanditsDefeated", "N", iMannequinBanditsDefeated)
            
                        --If this is the third bandit, play a cutscene.
                        if(iMannequinBanditsDefeated == 3.0) then
                            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 4, "Thief", "Neutral") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Warrior", "Neutral") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thief:[E|Neutral] Help![SOFTBLOCK] The wax elf is kicking our asses![BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Flee or die, humans.[BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Warrior:[E|Neutral] Fleeing sounds good, we're doing that![BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Take your wounded and get out.[SOFTBLOCK] Dally, and I will kill you.[BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thief:[E|Neutral] Come on, let's go![SOFTBLOCK] Go![BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] The bandits are defeated.[SOFTBLOCK] They will run for their lives.[SOFTBLOCK] The master's will be done.[BLOCK][CLEAR]") ]])
                            
                            --Variables.
                            local iVanquishedUndead = VM_GetVar("Root/Variables/NinaMod/MainQuest/iVanquishedUndead", "N")
                            
                            --Has not dealt with the undead yet.
                            if(iVanquishedUndead == 0.0) then
                                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
                                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I must destroy the undead, as master has bidden me.[BLOCK][CLEAR]") ]])
                                
                            --Dealt with the undead.
                            else
                                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
                                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
                                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I am a mannequin.[SOFTBLOCK] Loyalty undying.[SOFTBLOCK] Serve the master.[BLOCK][CLEAR]") ]])
                                fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I must return to the master.[SOFTBLOCK] The master will have new tasks for me.") ]])
                            end
                            fnCutsceneBlocker()
                        end
                    end
                end
            
            -- |[Subquest: Zombie Beat The Bandits]|
            elseif(sNinaForm == "Zombie") then
            
                --Variables.
                local sEnemyDisplayName = AdvCombatEntity_GetProperty("Display Name")
                local iZombieBanditsDefeated = VM_GetVar("Root/Variables/NinaMod/MainQuest/iZombieBanditsDefeated", "N")
                local iSawZombieDriveOffInvaders = VM_GetVar("Root/Variables/NinaMod/MainQuest/iSawZombieDriveOffInvaders", "N")
                
                --Quest is active:
                if(iSawZombieDriveOffInvaders == 1.0) then
            
                    --Check if the enemy's display name is any of the bandit names.
                    if(sEnemyDisplayName == "Brigand Warrior" or sEnemyDisplayName == "Brigand Thief" or sEnemyDisplayName == "Brigand Leader") then
                        
                        --Increment and save.
                        iZombieBanditsDefeated = iZombieBanditsDefeated + 1
                        VM_SetVar("Root/Variables/NinaMod/MainQuest/iZombieBanditsDefeated", "N", iZombieBanditsDefeated)
            
                        --If this is the third bandit, play a cutscene.
                        if(iZombieBanditsDefeated == 3.0) then
                            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Thief", "Neutral") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Warrior", "Neutral") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thief:[E|Neutral] Help![SOFTBLOCK] She's kicking our asses![BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] By the stars I am so hungry...[SOFTBLOCK] C'mere...[BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Warrior:[E|Neutral] Heeeeelp![SOFTBLOCK] Don't eat me, I don't want to die![BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Warrior:[E|Neutral] Please please please don't eat me, I'll do anything![BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Shut up.[BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Warrior:[E|Neutral] I'll worship the ground you walk on![SOFTBLOCK] I'll give you foot massages![BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] I said, shut up![BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Warrior:[E|Neutral] I'm begging you, anything, anything![BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] Ugh![SOFTBLOCK] I'm not even a little hungry anymore![SOFTBLOCK] You sicken me![BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thief:[E|Neutral] Really?[SOFTBLOCK] You're not gonna eat us?[BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] I don't know, I've never been hungry enough to eat a bug![SOFTBLOCK] That's what you are![SOFTBLOCK] Now get out of here![BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Warrior:[E|Neutral] Ruuuuun![BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Well that's those losers taken care of.[SOFTBLOCK] I don't even want a nibble now...[BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Happy] But maybe I can get a pick from the vampiress...") ]])
                            fnCutsceneBlocker()
                        end
                    end
                end
                
            -- |[Subquest: Human Beat The Bandits]|
            elseif(sNinaForm == "Human") then
            
                --Variables.
                local sEnemyDisplayName = AdvCombatEntity_GetProperty("Display Name")
                local iHumanBanditsDefeated = VM_GetVar("Root/Variables/NinaMod/MainQuest/iHumanBanditsDefeated", "N")
                local iSawHumanDriveOffInvaders = VM_GetVar("Root/Variables/NinaMod/MainQuest/iSawHumanDriveOffInvaders", "N")
                
                --Quest is active:
                if(iSawHumanDriveOffInvaders == 1.0) then
            
                    --Check if the enemy's display name is any of the bandit names.
                    if(sEnemyDisplayName == "Brigand Warrior" or sEnemyDisplayName == "Brigand Thief" or sEnemyDisplayName == "Brigand Leader") then
                        
                        --Increment and save.
                        iHumanBanditsDefeated = iHumanBanditsDefeated + 1
                        VM_SetVar("Root/Variables/NinaMod/MainQuest/iHumanBanditsDefeated", "N", iHumanBanditsDefeated)
            
                        --If this is the third bandit, play a cutscene.
                        if(iHumanBanditsDefeated == 3.0) then
                            fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 3, "Thief", "Neutral") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 5, "Warrior", "Neutral") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thief:[E|Neutral] Help![SOFTBLOCK] We're losing and it's barely a contest![BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] That is, indeed, true.[SOFTBLOCK] You guys suck.[BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Warrior:[E|Neutral] Heeeeelp![SOFTBLOCK] Ruuunnn![BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Upset] How embarassing.[SOFTBLOCK] Thugs who can't even fight.[SOFTBLOCK] Get lost![BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Thief:[E|Neutral] Aieee![BLOCK][CLEAR]") ]])
                            fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Well that's them taken care of.[SOFTBLOCK] Better go tell the countess about how it went.") ]])
                            fnCutsceneBlocker()
                        end
                    end
                end
            end
        DL_PopActiveObject()
    end
end

-- |[Debug]|
Debug_PopPrint("============ Post Combat Handler Ends ============\n")
