-- |[ ================================= Enemy Creation Response ================================ ]|
--When the function fnStandardEnemyHandler() is called, it checks the list of enemy prototypes and
-- creates an enemy based on the name. This function will be called to see if this mod needs to
-- populate enemy data.
--The global zEntry needs to be populated.

-- |[Mod Activity Check]|
--If the mod is not active, don't do any of the below.
if(OM_IsModActive("Nina's Adventure") == false) then return end

-- |[Argument Check]|
if(fnArgCheck(1) == false) then return end

--Argument resolve.
local psActualName = LM_GetScriptArgument(0)

-- |[Error Check]|
if(gczaNinaAdventureStats == nil) then return end

-- |[Scan]|
--Scan for a match.
for i = 1, #gczaNinaAdventureStats, 1 do
    if(gczaNinaAdventureStats[i].sActualName == psActualName) then
        zEntry = gczaNinaAdventureStats[i]
    end
end
