-- |[ ================================= Enemy Statistics Chart ================================= ]|
--This chart lists off all enemy properties in one handy spot. Complicated enemies may need additional
-- setup, which is why they have their own files.
if(OM_IsModActive("Nina's Adventure") == false) then return end

--Setup.
gzStatArray = {}
gzPortraitArray = {}

-- |[ ====================================== Enemy Listing ===================================== ]|
--Notes: Resistances are difference from standard, not actual values. The standard resistance is gciNormalResist.
-- Extra abilities and tags are added later.
--The "Attack" column is the basic attack this entity uses. Place "Null" to have no attacks or to manually specify
-- additional attacks later.

-- |[Chart]|
--     Actual Name       | Display Name      | Attack ||||| Lv | Health | Attack | Ini | Acc | Evd | Stun ||||| Pr | Sl | St | Pc | Fl | Fr | Sh | Cr | Ob | Bl | Ps | Cr | Tr ||||| Plt | Exp | JP |  Drop             | Chnc
fnAdd("Brigand Warrior",  "Brigand Warrior",  "Sword",      1,      66,      16,   20,   20,   20,   100,     4,   1,   1,   1,   3,   3,   0,   0,   0,  -5,  -5,   0, -12,      3,    8,  25, "Null",                 0)
fnAdd("Brigand Thief",    "Brigand Thief",    "Sword",      3,      50,      14,   40,   20,   60,    70,     0,  -1,  -1,  -1,   3,   3,   0,   0,   0,  -5,  -5,   0, -12,      3,   13,  25, "Null",                 0)
fnAdd("Brigand Leader",   "Brigand Leader",   "Sword",      3,     145,      30,   20,   20,   30,   100,     6,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,   0,     12,   22,  25, "Null",                 0)
fnAdd("Skeleton Archer",  "Skeleton Archer",  "Sword",      3,     145,      30,   20,   20,   30,   100,     0,   2, -15,   2,   0,   0,   0,  -8,  -8,  20,  20,   0,1000,     12,   22,  25, "Null",                 0)
fnAdd("Skeleton Warrior", "Skeleton Warrior", "Sword",      3,     145,      30,   20,   20,   30,   100,     6,   2, -15,   2,   0,   0,   0,  -8,  -8,  20,  20,   0,1000,     12,   22,  25, "Null",                 0)
fnAdd("Jelli",            "Jelli",            "Null",       3,      50,      30,   40,   20,   10,    50,     0,   3,   4,   3,   0,  -5,   0,   0,   0,   8,   8,   8,  -2,      7,    8,  25, "Null",                 0)
fnAdd("Sproutpecker",     "Sprout Pecker",    "Claw",       5,     120,      44,   60,    0,   20,   100,     0,  -1,   1,   1,  -5,   2,   4,   0,   0,   3,   3,   3,  -4,     20,   17,  25, "Mordreen Gem",         5)
fnAdd("Sproutling",       "Sproutling",       "Claw",       3,      20,      35,   70,   30,   20,    50,     0,  -1,   1,   1,  -5,   2,   4,   0,   0,   3,   3,   3,  -4,      5,   12,  25, "Null",                 0)

-- |[KO Count Modifiers]|
--None yet.

-- |[ =============================== Special Abilities and Tags =============================== ]|
--Enemies that have more than the standard attack or have special tags get them added here.

-- |[ ====================== Tag Application ===================== ]|
fnTag("Jelli",            {})
fnTag("Sproutpecker",     {{"Blind Effect +",  5}, {"Blind Apply +",   5}} )
fnTag("Sproutling",       {})

-- |[ ========================= Abilities ======================== ]|
-- |[Common Abilities]|
--Strike
--Pierce
--Flame
--Freeze
--Shock
--Crusade
--Obscure
--Bleed
--Poison
--Corrode
--Terrify

-- |[Chapter-Specific Abilities]|
--Sproutpecker.
local sPeckPath = gsRoot .. "Combat/Enemies/Abilities/Chapter 1 Enemies/Peck.lua"
fnAbility("Sprout Pecker",   {"Enemy|Peck", sPeckPath, 50})

-- |[ ========================================= Topics ========================================= ]|
--fnTopic("Sevavi Guardian",{})

-- |[ ================================= Portrait Offset Lookup ================================= ]|
-- |[Listing]|
--             Portrait Path                                  | Turn Portrait Path                                      | Actor 
fnAddPortrait("Root/Images/Portraits/Combat/Brigand_Leader",   "Root/Images/AdventureUI/TurnPortraits/Leader",           "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Brigand_Thief",    "Root/Images/AdventureUI/TurnPortraits/Thief",            "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Brigand_Warrior",  "Root/Images/AdventureUI/TurnPortraits/Warrior",          "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Skeleton_Archer",  "Root/Images/AdventureUI/TurnPortraits/SkelArch",         "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Skeleton_Warrior", "Root/Images/AdventureUI/TurnPortraits/SkelWarr",         "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Jelli",            "Root/Images/AdventureUI/TurnPortraits/Jelli",            "Null")
fnAddPortrait("Root/Images/Portraits/Combat/SproutPecker",     "Root/Images/AdventureUI/TurnPortraits/Sproutpecker",     "Null")
fnAddPortrait("Root/Images/Portraits/Combat/Sproutling",       "Root/Images/AdventureUI/TurnPortraits/Sproutling",       "Null")

-- |[Mapping]|
--Mainline
fnSetRemap("Brigand Warrior",  "Root/Images/Portraits/Combat/Brigand_Warrior")
fnSetRemap("Brigand Thief",    "Root/Images/Portraits/Combat/Brigand_Thief")
fnSetRemap("Brigand Leader",   "Root/Images/Portraits/Combat/Brigand_Leader")
fnSetRemap("Skeleton Archer",  "Root/Images/Portraits/Combat/Skeleton_Archer")
fnSetRemap("Skeleton Warrior", "Root/Images/Portraits/Combat/Skeleton_Warrior")
fnSetRemap("Jelli",            "Root/Images/Portraits/Combat/Jelli")
fnSetRemap("Sproutpecker",     "Root/Images/Portraits/Combat/SproutPecker")
fnSetRemap("Sproutling",       "Root/Images/Portraits/Combat/Sproutling")

-- |[ ======================================== Paragons ======================================== ]|
--Paragons have the same display abilities as the base case, but 10x the stats and 10x the rewards, and +3 all resistances.
-- Their setup must be done after all other entries are added.
local iTotal = #gzStatArray
for i = 1, iTotal, 1 do
    fnCreateParagon(i)
end

-- |[ ========================================== Debug ========================================= ]|
--Store all the data in chapter-local arrays.
gczaNinaAdventureStats = gzStatArray
gczaChapterMPortraitLookups = gzPortraitArray

--Print all stats to the console.
if(false) then
    io.write("Listing Enemy information:\n")
    for i = 1, #gczaChapterMStats, 1 do
        io.write(string.format("%2i: %s - %i %i %i %i %i\n", i, gczaNinaAdventureStats[i].sActualName, gczaNinaAdventureStats[i].iHealth, gczaNinaAdventureStats[i].iAtk, gczaNinaAdventureStats[i].iIni, gczaNinaAdventureStats[i].iAcc, gczaNinaAdventureStats[i].iEvd))
    end
end
