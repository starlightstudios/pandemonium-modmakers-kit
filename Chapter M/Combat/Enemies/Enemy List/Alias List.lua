-- |[ ============================= Nina's Adventure Enemy Aliases ============================= ]|
--Called at chapter boot. Creates a set of aliases for enemy names.
local sBasePath = fnResolvePath()
AdvCombat_SetProperty("Clear Enemy Aliases")

--Run the stat chart algorithm. This populates gczaChapter2Stats with all enemy properties.
-- If you need more specialized setup, that can be done in the enemy script.
LM_ExecuteScript(sBasePath .. "Enemy Stat Chart.lua")

-- |[ =================================== Creation Function ==================================== ]|
local function fnCreateAlias(psSLFName, psAliasName, bHasParagon, psPath)
    
    --If the path is "AUTO", store that directly.
    local sUsePath = psPath .. ".lua"
    if(psPath == "AUTO") then sUsePath = "AUTO" end
    AdvCombat_SetProperty("Create Enemy Path", psAliasName, sUsePath)
    AdvCombat_SetProperty("Create Enemy Alias", psSLFName, psAliasName)
    
    --If needed, make a paragon version.
    if(bHasParagon) then
        
        --If the path is "AUTO", use that without additions:
        local sUsePath = psPath .. " Paragon.lua"
        if(psPath == "AUTO") then sUsePath = "AUTO" end
        
        AdvCombat_SetProperty("Create Enemy Path", psAliasName .. " Paragon", sUsePath)
        AdvCombat_SetProperty("Create Enemy Alias", psSLFName .. " Paragon", psAliasName .. " Paragon")
    end
end

-- |[ ====================================== Alias Listing ===================================== ]|
-- |[Auto-Set Cases]|
--All of these enemies do not require special handlers and can be auto-set.
local saAutoList = {"Brigand Warrior", "Brigand Thief", "Brigand Leader", "Skeleton Archer", "Skeleton Warrior", "Sproutling", "Sproutpecker", "Jelli"}
for i = 1, #saAutoList, 1 do
    fnCreateAlias(saAutoList[i], saAutoList[i], true, "AUTO")
end

-- |[Manual Set Cases]|
--For enemies that require special variables when being created, handle that here.
