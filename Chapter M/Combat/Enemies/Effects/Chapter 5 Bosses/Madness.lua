-- |[ ========================================= Madness ======================================== ]|
-- |[Description]|
--Deals damage over time equal to 1.20x attack power over 3 turns. Damage type is provided as
-- an argument.

-- |[Notes]|
--None yet.

--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ======================================== Creation ======================================== ]|
--Called when the Effect is initialized. This may be at combat start, or it may be when the matching
-- ability is used. This is *not* called for a passive effect for the Status UI!
--Local variables, if any, should be set up here. The Active Object is the newly created AdvCombatEffect.
if(iSwitchType == gciEffect_Create) then
    
    --Resolve images and tags from type.
    local sDisplayName = "DoT Bleed"
    local sAbilityIcon = "GenBleed"
    local saTagList = {}
    
    --Switch case:
    if(giMadnessEffectType == gciDamageType_Slashing) then
        sDisplayName = "DoT Slash"
        sAbilityIcon = "GenBleed"
        saTagList[1] = {"Slashing DoT", 1}
        
    elseif(giMadnessEffectType == gciDamageType_Striking) then
        sDisplayName = "DoT Strike"
        sAbilityIcon = "GenStrike"
        saTagList[1] = {"Striking DoT", 1}
        
    elseif(giMadnessEffectType == gciDamageType_Piercing) then
        sDisplayName = "DoT Pierce"
        sAbilityIcon = "GenPierce"
        saTagList[1] = {"Piercing DoT", 1}
        
    elseif(giMadnessEffectType == gciDamageType_Flaming) then
        sDisplayName = "DoT Flame"
        sAbilityIcon = "GenFlame"
        saTagList[1] = {"Flaming DoT", 1}
        
    elseif(giMadnessEffectType == gciDamageType_Freezing) then
        sDisplayName = "DoT Freeze"
        sAbilityIcon = "GenFreeze"
        saTagList[1] = {"Freezing DoT", 1}
        
    elseif(giMadnessEffectType == gciDamageType_Shocking) then
        sDisplayName = "DoT Shock"
        sAbilityIcon = "GenShock"
        saTagList[1] = {"Shocking DoT", 1}
        
    elseif(giMadnessEffectType == gciDamageType_Crusading) then
        sDisplayName = "DoT Crusade"
        sAbilityIcon = "GenLight"
        saTagList[1] = {"Crusading DoT", 1}
        
    elseif(giMadnessEffectType == gciDamageType_Obscuring) then
        sDisplayName = "DoT Obscure"
        sAbilityIcon = "GenDark"
        saTagList[1] = {"Obscuring DoT", 1}
        
    elseif(giMadnessEffectType == gciDamageType_Bleeding) then
        sDisplayName = "DoT Bleed"
        sAbilityIcon = "GenBleed"
        saTagList[1] = {"Bleeding DoT", 1}
        
    elseif(giMadnessEffectType == gciDamageType_Poisoning) then
        sDisplayName = "DoT Poison"
        sAbilityIcon = "GenPoison"
        saTagList[1] = {"Poisoning DoT", 1}
        
    elseif(giMadnessEffectType == gciDamageType_Corroding) then
        sDisplayName = "DoT Corrode"
        sAbilityIcon = "GenCorrode"
        saTagList[1] = {"Corroding DoT", 1}
        
    elseif(giMadnessEffectType == gciDamageType_Terrifying) then
        sDisplayName = "DoT Terrify"
        sAbilityIcon = "GenTerrify"
        saTagList[1] = {"Terrifying DoT", 1}
    end
    
    --Create.
    AdvCombatEffect_SetProperty("Script", LM_GetCallStack(0))
    local iUniqueID = RO_GetID()
    LM_ExecuteScript(gsStandardDoTPath, gciEffect_Create, sDisplayName, iUniqueID, 3, sAbilityIcon, saTagList[1][1], saTagList[1][2])
    
    --Store effect type.
    DL_AddPath("Root/Variables/Combat/" .. iUniqueID .. "/")
    VM_SetVar("Root/Variables/Combat/"  .. iUniqueID .. "/iEffectType", "N", giMadnessEffectType)
    
-- |[ ====================================== Apply Stats ======================================= ]|
--Called when the given Effect needs to apply its stats to a CombatStatistics package. This may be
-- when it is created, when the status UI needs it, or any number of other locations. If the flag is
-- gciEffect_UnApplyStats, then the effects are being removed because the effect expired.
--The AdvCombatEffect is the Active Effect. The second argument passed in will be the ID of the target.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then
    
    --Get ID of target.
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    if(iTargetID < 1) then return end
    
    --Variables.
    local iUniqueID = RO_GetID()
    local iResistIndex = gciStatIndex_Resist_Poison
    local sTypeIndicator = "Poisoning"
    
    --Resolve type based on effect type.
    local iEffectType = VM_GetVar("Root/Variables/Combat/"  .. iUniqueID .. "/iEffectType", "N")
    
    --Index is based on type:
    iResistIndex = gciStatIndex_Resist_Start + iEffectType + gciDamageOffsetToResistances
    
    --Switch strings:
    if(iEffectType == gciDamageType_Slashing) then
        sTypeIndicator = "Slashing"
        
    elseif(iEffectType == gciDamageType_Striking) then
        sTypeIndicator = "Striking"
        
    elseif(iEffectType == gciDamageType_Piercing) then
        sTypeIndicator = "Piercing"
        
    elseif(iEffectType == gciDamageType_Flaming) then
        sTypeIndicator = "Flaming"
        
    elseif(iEffectType == gciDamageType_Freezing) then
        sTypeIndicator = "Freezing"
        
    elseif(iEffectType == gciDamageType_Shocking) then
        sTypeIndicator = "Shocking"
        
    elseif(iEffectType == gciDamageType_Crusading) then
        sTypeIndicator = "Crusading"
        
    elseif(iEffectType == gciDamageType_Obscuring) then
        sTypeIndicator = "Obscuring"
        
    elseif(iEffectType == gciDamageType_Bleeding) then
        sTypeIndicator = "Bleeding"
        
    elseif(iEffectType == gciDamageType_Poisoning) then
        sTypeIndicator = "Poisoning"
        
    elseif(iEffectType == gciDamageType_Corroding) then
        sTypeIndicator = "Corroding"
        
    elseif(iEffectType == gciDamageType_Terrifying) then
        sTypeIndicator = "Terrifying"
    end
    
    --Subroutine.
    LM_ExecuteScript(gsStandardDoTPath, iSwitchType, iTargetID, iResistIndex, 0.40, sTypeIndicator)

-- |[ ================================== Combat: Turn Begins =================================== ]|
--Called when the turn begins after turn order is resolved. This is called after Abilities have
-- run their updates. If an Effect is created by an ability during its update, it does *not* run its
-- turn-begin code until the next turn.
elseif(iSwitchType == gciAbility_BeginTurn) then

-- |[ ============================ Combat: Character Action Begins ============================= ]|
--Called when the character that possesses this Effect begins their turn.
elseif(iSwitchType == gciAbility_BeginAction) then
    
    --Variables.
    local iUniqueID = RO_GetID()
    local sTypeIndicator = "Poisoning"
    local sAnimation = "Poison"
    
    --Resolve type based on effect type.
    local iEffectType = VM_GetVar("Root/Variables/Combat/"  .. iUniqueID .. "/iEffectType", "N")
    
    --Switch strings:
    if(iEffectType == gciDamageType_Slashing) then
        sTypeIndicator = "Slashing"
        sAnimation = "Sword Slash"
        
    elseif(iEffectType == gciDamageType_Striking) then
        sTypeIndicator = "Striking"
        sAnimation = "Strike"
        
    elseif(iEffectType == gciDamageType_Piercing) then
        sTypeIndicator = "Piercing"
        sAnimation = "Pierce"
        
    elseif(iEffectType == gciDamageType_Flaming) then
        sTypeIndicator = "Flaming"
        sAnimation = "Flames"
        
    elseif(iEffectType == gciDamageType_Freezing) then
        sTypeIndicator = "Freezing"
        sAnimation = "Freeze"
        
    elseif(iEffectType == gciDamageType_Shocking) then
        sTypeIndicator = "Shocking"
        sAnimation = "Shock"
        
    elseif(iEffectType == gciDamageType_Crusading) then
        sTypeIndicator = "Crusading"
        sAnimation = "Light"
        
    elseif(iEffectType == gciDamageType_Obscuring) then
        sTypeIndicator = "Obscuring"
        sAnimation = "Shadow A"
        
    elseif(iEffectType == gciDamageType_Bleeding) then
        sTypeIndicator = "Bleeding"
        sAnimation = "Bleed"
        
    elseif(iEffectType == gciDamageType_Poisoning) then
        sTypeIndicator = "Poisoning"
        sAnimation = "Poison"
        
    elseif(iEffectType == gciDamageType_Corroding) then
        sTypeIndicator = "Corroding"
        sAnimation = "Corrode"
        
    elseif(iEffectType == gciDamageType_Terrifying) then
        sTypeIndicator = "Terrifying"
        sAnimation = "Terrify"
    end

    --Execute.
    LM_ExecuteScript(gsStandardDoTPath, gciAbility_BeginAction, sTypeIndicator, sAnimation)

-- |[ ============================= Combat: Character Free Action ============================== ]|
--Called after a character executes a free action.
elseif(iSwitchType == gciEffect_BeginFreeAction) then

-- |[ ============================= Combat: Character Action Ends ============================== ]|
--Called after a character executes any action. Is not called if the action was a free action 
-- (that's gciEffect_BeginFreeAction above).
elseif(iSwitchType == gciEffect_PostAction) then
    LM_ExecuteScript(gsStandardDoTPath, gciEffect_PostAction)

-- |[ =================================== Combat: Turn Ends ==================================== ]|
--Called after the turn ends, before turn order for the next turn is resolved.
elseif(iSwitchType == gciEffect_TurnEnds) then

-- |[ ================================== Combat: Combat Ends =================================== ]|
--Called after combat ends. The passed in flag dictates if Victory/Defeat occurred. This occurs
-- before Doctor Bag/XP/JP etc is awarded.
elseif(iSwitchType == gciEffect_CombatEnds) then

end
