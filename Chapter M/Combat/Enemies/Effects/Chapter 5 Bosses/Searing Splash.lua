-- |[ ===================================== Searing Splash ===================================== ]|
-- |[Description]|
--Backloaded DoT, does most of its damage in its last two turns.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ===================================== Prefabrication ===================================== ]|
--Creates a fast-access version of the ability.
if(gzEffectCH5BossSearingSplash == nil) then
    
    --Base
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Corroding, 10, 9.00)
    
    --System/Display
    zEffectStruct.sDisplayName = "Searing Splash"
    zEffectStruct.sIcon = "GenCorrode"
    
    --Turn Damage Factors
    zEffectStruct.iDamageFactorDescriptionFlag = gciDoTBackload
    zEffectStruct.faTurnFactors = {0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 4.000, 4.000}
    
    --Store
    gzEffectCH5BossSearingSplash = zEffectStruct
    
end

--Standardized Naming
gzRefAbility = gzEffectCH5BossSearingSplash

--Call the standard handler. It will nil off the gzRefAbility.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
