-- |[ ==================================== Prismatic Shield ==================================== ]|
-- |[Description]|
--Increase all resists by 30 for 5 turns.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ==================================== Local Variables ===================================== ]|
-- |[System]|
--This effect uses a standardized StatMod script.
local sDisplayName = "Prismatic Shield"
local iDuration = 6
local bIsBuff = true
local sIcon = "Root/Images/AdventureUI/Abilities/GenBuff"
local sStatString = "ResSls|Flat|30 ResStk|Flat|30 ResPrc|Flat|30 ResFlm|Flat|30 ResFrz|Flat|30 ResShk|Flat|30 ResCru|Flat|30 ResObs|Flat|30 ResBld|Flat|30 ResPsn|Flat|30 ResCrd|Flat|30 ResTfy|Flat|30"

--Description can be a variable number of lines.
local saDescription = {}
saDescription[1] = "A rainbow aura shields the target."
saDescription[2] = "Increases all resists by 30."
saDescription[3] = ""
saDescription[4] = ""
saDescription[5] = ""
saDescription[6] = "+30 All Resists."

--Run the standardized builder.
gzaStatModStruct            = fnCreateStatmodEffectPrototype(sDisplayName, iDuration, bIsBuff, sIcon, sStatString)
gzaStatModStruct.saStrings  = fnStatmodBuildInspectorStrings(gzaStatModStruct, saDescription)
gzaStatModStruct.sShortText = fnStatmodBuildShortText(gzaStatModStruct)

-- |[Overrides]|
--Tags.
gzaStatModStruct.saTagList = {{"Prismatic Shield", 1}}

--Right string is obscenely long. Modify it.
gzaStatModStruct.saStrings[2] = "+30 All Resists /[Dur][Turns]"

-- |[ ======================================== Creation ======================================== ]|
--Standard call when effect is initialized.
if(iSwitchType == gciEffect_Create) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_Create)
    
-- |[ ================================== Apply/Unapply Stats =================================== ]|
--Calls the apply/unapply stat routines.
elseif((iSwitchType == gciEffect_ApplyStats or iSwitchType == gciEffect_UnApplyStats) and iArgumentsTotal >= 2) then

    -- |[Stat Handler]|
    local iTargetID = math.floor(tonumber(LM_GetScriptArgument(1, "N")))
    LM_ExecuteScript(gsStandardStatPath, iSwitchType, iTargetID)

    -- |[Short Text]|
    --The short text for this ability goes offscreen, so we set it manually.
    local iUniqueID = RO_GetID()

    --Set the short text to the finalized string.
    AdvCombatEffect_SetProperty("Short Text", iTargetID, "All Resists +30")
    VM_SetVar("Root/Variables/Combat/" .. iUniqueID .. "/sShortText", "S", "All Resists +30")

-- |[ ================================== Character Action Ends ================================= ]|
--Called after a character action ends to decrement the turn counter.
elseif(iSwitchType == gciEffect_PostAction) then
    LM_ExecuteScript(gsStandardStatPath, gciEffect_PostAction)
end
