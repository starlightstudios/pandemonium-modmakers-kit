-- |[ ========================================== Gouge ========================================= ]|
-- |[Description]|
--Deals bleeding damage equal to 0.99 of user's attack power over 3 turns.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectCH1BossGouge == nil) then
    
    --Base
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Bleeding, 3, 1.99)
    
    --System/Display
    zEffectStruct.sDisplayName = "Gouge"
    zEffectStruct.sIcon = "GenBleed"
    
    --Store
    gzEffectCH1BossGouge = zEffectStruct
    
end

--Standardized Naming
gzRefAbility = gzEffectCH1BossGouge

--Call the standard handler. It will nil off the gzRefAbility.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
