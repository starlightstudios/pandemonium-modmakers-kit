-- |[ ========================================== Guilt ========================================= ]|
-- |[Description]|
--Bleeds for little damage each turn. Causes double damage to be taken when the warden strikes.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[Prefabrication Variables]|
--Setup, apply overrides.
if(gzEffectCH1BossGuilt == nil) then
    
    --Base
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Bleeding, 4, 0.36)
    
    --System/Display
    zEffectStruct.sDisplayName = "Guilt"
    zEffectStruct.sIcon = "GenBleed"
    
    --Store
    gzEffectCH1BossGuilt = zEffectStruct
    
end

--Standardized Naming
gzRefAbility = gzEffectCH1BossGuilt

--Call the standard handler. It will nil off the gzRefAbility.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)