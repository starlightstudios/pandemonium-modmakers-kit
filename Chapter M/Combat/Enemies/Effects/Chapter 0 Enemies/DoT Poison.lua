-- |[ ======================================= DoT Poison ======================================= ]|
-- |[Description]|
--Deals poisoning damage equal to 0.60 attack power over 3 turns.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ===================================== Prefabrication ===================================== ]|
--Creates a fast-access version of the ability.
if(gzEffectCH1EnemyPoison == nil) then
    
    --Base
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Poisoning, 3, 0.60)
    
    --System/Display
    zEffectStruct.sDisplayName = "Poison"
    zEffectStruct.sIcon = "GenPoison"
    
    --Store
    gzEffectCH1EnemyPoison = zEffectStruct
    
end

--Standardized Naming
gzRefAbility = gzEffectCH1EnemyPoison

--Call the standard handler. It will nil off the gzRefAbility.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
