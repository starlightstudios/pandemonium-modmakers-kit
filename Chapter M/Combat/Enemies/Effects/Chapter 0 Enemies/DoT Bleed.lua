-- |[ ======================================= DoT Bleed ======================================== ]|
-- |[Description]|
--Deals bleeding damage equal to 0.60 attack power over 3 turns.

-- |[Arguments]|
--Argument handling.
local iArgumentsTotal = LM_GetNumOfArgs()
if(iArgumentsTotal < 1) then return end

--Zeroth argument determines what this script does.
local iSwitchType = LM_GetScriptArgument(0, "N")

-- |[ ===================================== Prefabrication ===================================== ]|
--Creates a fast-access version of the ability.
if(gzEffectCH1EnemyBleed == nil) then
    
    --Base
    local zEffectStruct = fnCreateDoTEffectPrototype(gciDamageType_Bleeding, 3, 0.60)
    
    --System/Display
    zEffectStruct.sDisplayName = "Bleed"
    zEffectStruct.sIcon = "GenBleed"
    
    --Store
    gzEffectCH1EnemyBleed = zEffectStruct
    
end

--Standardized Naming
gzRefAbility = gzEffectCH1EnemyBleed

--Call the standard handler. It will nil off the gzRefAbility.
LM_ExecuteScript(gsStandardEffectDotPath, iSwitchType)
