-- |[ =========================== Position Lookups, Nina's Adventure =========================== ]|
--This file is called with a path to an image. That information is then used to set the UI positions
-- for that image.

-- |[Mod Activity Check]|
--If the mod is not active, don't do any of the below.
if(OM_IsModActive("Nina's Adventure") == false) then return end

-- |[Arguments]|
if(fnArgCheck(1) == false) then return end
local sDLPath = LM_GetScriptArgument(0)

--Set flag.
gbStopModExec = true
gbModHandledCall = true

-- |[Strip]|
--Start at the end of the path and count backwards until a / is found. Use only the last part.
-- This allows paragons to use the same alignments.
local i = 0
while true do
    local p = string.find(sDLPath, "/", i+1)
    if p == nil then break end
    i = p
end

--Get the ending string.
sDLPath = string.sub(sDLPath, i+1)

-- |[Scanning]|
if(sDLPath == "Brigand_Leader") then
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      204,  92)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -132, -22)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 251,  81)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Enemy,       0,  66)

elseif(sDLPath == "Brigand_Thief") then
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      193,  65)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -153, -97)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 237,  41)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Enemy,       0,  21)

elseif(sDLPath == "Brigand_Warrior") then
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      177, 125)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,     -163, -22)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 225,  98)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Enemy,       0,  80)

elseif(sDLPath == "Skeleton_Archer") then
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      240, 116)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,      -98, -48)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 282,  91)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Enemy,       0,  69)

elseif(sDLPath == "Skeleton_Warrior") then
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Base,      270, 108)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Ally,      -66, -75)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Inspector, 310,  81)
    AdvCombatEntity_SetProperty("UI Render Position", gci_ACE_RenderSlot_Combat_Enemy,       0,  69)

-- |[Not Found]|
--Set the found portrait flag to false.
else
    gbStopModExec = false
    gbModHandledCall = false
end 
