-- |[ ===================================== Build Form List ==================================== ]|
--When the debug list builds a list of forms, this script will be called to append on the end.

-- |[Mod Activity Check]|
--If the mod is not active, don't do any of the below.
if(OM_IsModActive("Nina's Adventure") == false) then return end

--Get chapter variable. Do nothing if this mod is not the active "chapter".
local iCurrentChapter = VM_GetVar("Root/Variables/Global/ChapterComplete/iCurrentChapter", "N")
if(iCurrentChapter ~= 10) then return end

-- |[Setup]|
--String prefix.
local sPre = "../Chapter M/FormHandlers/"

-- |[Nina]|
--List.
local saNinaList = {sPre .. "Nina/Form_Human", sPre .. "Nina/Form_Zombie", sPre .. "Nina/Form_Mannequin"}

-- |[Set]|
--Send this to the global set.
saListList[1] = saNinaList