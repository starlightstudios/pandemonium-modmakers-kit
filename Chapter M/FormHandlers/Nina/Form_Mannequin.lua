-- |[ ===================================== Nina Mannequin ===================================== ]|
--Script called when Nina shapeshifts into a mannequin at a save point.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Nina/sForm", "S", "Mannequin")
VM_SetVar("Root/Variables/Global/Nina/iHasMannequinForm", "N", 1.0)

-- |[World]|
--Any enemies on the map of this type should now ignore the party.
AL_PulseIgnore("Mannequin")

-- |[Combat Statistics]|
--Change Mei's class to Bee. This automatically changes appearance.
AdvCombat_SetProperty("Push Party Member", "Nina")
    AdvCombatEntity_SetProperty("Active Job", "Mannequin")
DL_PopActiveObject()
