-- |[ ======================================= Nina Human ======================================= ]|
--Script called when Nina shapeshifts into a human at a save point.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Nina/sForm", "S", "Human")

-- |[World]|
--Any enemies on the map of this type should now ignore the party.
AL_PulseIgnore("Human")

-- |[Combat Statistics]|
--Change Mei's class to Bee. This automatically changes appearance.
AdvCombat_SetProperty("Push Party Member", "Nina")
    AdvCombatEntity_SetProperty("Active Job", "Archer")
DL_PopActiveObject()
