-- |[ ====================================== Nina Zombie ======================================= ]|
--Script called when Nina shapeshifts into a zombie at a save point.

-- |[Script Variables]|
--Mark this as the current form.
VM_SetVar("Root/Variables/Global/Nina/sForm", "S", "Zombie")
VM_SetVar("Root/Variables/Global/Nina/iHasZombieForm", "N", 1.0)

-- |[World]|
--Any enemies on the map of this type should now ignore the party.
AL_PulseIgnore("Zombie")

-- |[Combat Statistics]|
--Change Mei's class to Bee. This automatically changes appearance.
AdvCombat_SetProperty("Push Party Member", "Nina")
    AdvCombatEntity_SetProperty("Active Job", "Banshee")
DL_PopActiveObject()
