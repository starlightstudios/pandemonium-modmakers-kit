-- |[ ===================================== Items for Nina ===================================== ]|
--Items that only Nina can use. Mostly weapons, some items.
if(gsNinaRoot == nil) then return end

-- |[ ========================================= Charts ========================================= ]|
--For equipment which can be made as part of a chart, construct it here.
if(gzNinaItemChart == nil) then
    
    --Setup. All functions operate on this chart.
    gzActiveChart = {}
    
    --Make aliases for the functions. This shortens the chart for readability.
    local fnAdd            = fnAddItemToChart
    local fnAddWpn         = fnAddWeaponToChart
    local fnAddAbility     = fnAddAbilityToChart
    local fnSetDescription = fnSetDescriptionOnChart

    -- |[Weapons Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22 | Damage Type           | Stats Array
    fnAddWpn("Cypress Bow",                    200,          1,     false,    gciWeaponCode, "NinaBow",  gciDamageType_Piercing, {gciStatIndex_Attack,  20})
    fnAddWpn("Yew Bow",                        400,          2,     false,    gciWeaponCode, "NinaBow",  gciDamageType_Piercing, {gciStatIndex_Attack,  42, gciStatIndex_Accuracy, 10})

    -- |[Armors Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Hunter's Clothes",                  100,          0,     false,     gciArmorCode, "ArmGenLight",  {gciStatIndex_Protection, 3, gciStatIndex_Initiative, 20})

    -- |[Accessories Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Bow Sight",                         300,          0,     false, gciAccessoryCode, "ItemScope",     {gciStatIndex_Accuracy, 15})
    fnAdd("Hip Quiver",                        100,          0,     false, gciAccessoryCode, "ItemScope",     {gciStatIndex_Initiative, 30})
    fnAdd("Leather Cloak",                     100,          0,     false, gciAccessoryCode, "AccCapeBrown",  {gciStatIndex_Protection, 1, gciStatIndex_Evade, 10})

    -- |[Combat Items Chart]|
    --     Actual Name               | Cash Value | Gem Slots | Key Item |     Eqp Code    |  Symbol22      | Stats Array
    fnAdd("Flash Powder",                      150,          0,     false,      gciItemCode, "ItemFlashbang",      {})
    fnAddAbility("Flash Powder", gsNinaRoot .. "Combat/Party/Items/Abilities/Flash Powder.lua")

    -- |[Descriptions]|
    --Weapons
    fnSetDescription("Cypress Bow", "A bow hewn from cypress wood and enhanced with simple magic. Strong, durable, and very accurate.\nDeals piercing damage.")
    fnSetDescription("Yew Bow", "A bow hewn from yew wood and enhanced with simple magic. Holds even more tensile energy.\nDeals piercing damage.")

    --Armors
    fnSetDescription("Hunter's Clothes", "Nina's day-to-day clothing. Light, breezy, unlikely to get caught on a branch, and blends well with the terrain.")

    --Accessories
    fnSetDescription("Bow Sight",     "A sight attached to the bow that gives a rough indication of the expected drop under full extension.\nIncreases shot accuracy.")
    fnSetDescription("Hip Quiver",    "A quiver meant to fit on the hip. Stores arrows for quick firing. Increases combat initiative.")
    fnSetDescription("Leather Cloak", "A thick cloak made from stitched leathers, this cloak will keep you warm and maybe glance a blow.\nImproves evade and protection.")

    --Combat Items
    fnSetDescription("Flash Powder",  "Finely ground powder that creates a blinding light when thrown. Blinds enemies in battle.")

    -- |[Store]|
    --Store the chart under a unique name.
    gzNinaItemChart = gzActiveChart
    gzActiveChart = nil

end

-- |[ ==================================== Item Is On Chart ==================================== ]|
--If the item is on the prefabricated chart, we can create it algorithmically.
for i = 1, #gzNinaItemChart, 1 do
    if(gzNinaItemChart[i].sName == gsItemName) then

        --Quick-access copy.
        local zItem = gzNinaItemChart[i]

        --Create item.
        AdInv_CreateItem(gsItemName)
        
            -- |[System]|
            AdItem_SetProperty("Value", zItem.iValue)
            AdItem_SetProperty("Gem Slots", zItem.iGemSlots)
            AdItem_SetProperty("Is Key Item", zItem.bIsKeyItem)
            
            -- |[Display]|
            AdItem_SetProperty("Rendering Image", "Root/Images/AdventureUI/Symbols22/" .. zItem.sSymbol22)
            AdItem_SetProperty("Description", zItem.sDescription)
            
            -- |[Usability]|
            AdItem_SetProperty("Is Equipment", true)
            AdItem_SetProperty("Add Equippable Character", "Nina")
            
            -- |[Stat Array]|
            for p = 1, #zItem.iaStatArray, 2 do
                AdItem_SetProperty("Statistic", zItem.iaStatArray[p+0], zItem.iaStatArray[p+1])
            end
            
            -- |[Usability / Description]|
            --Weapons:
            if(zItem.iUsability == gciWeaponCode) then
                
                --Type.
                AdItem_SetProperty("Type", "Elven Bow")
                
                --Equipment slots.
                AdItem_SetProperty("Add Equippable Slot", "Weapon")
                AdItem_SetProperty("Add Equippable Slot", "Weapon Backup A")
                AdItem_SetProperty("Add Equippable Slot", "Weapon Backup B")
                
                --Animations and Sound
                AdItem_SetProperty("Equipment Attack Animation", "Pierce")
                AdItem_SetProperty("Equipment Attack Sound",     "Combat\\|Impact_Pierce")
                AdItem_SetProperty("Equipment Critical Sound",   "Combat\\|Impact_Pierce_Crit")
                
                --Damage type.
                AdItem_SetProperty("Damage Type", gciEquipment_DamageType_Base, zItem.iDamageType + gciDamageOffsetToResistances, 1.0)
                AdItem_SetProperty("Add Tag", zItem.sSecondaryType, 1)
                fnStandardizeDescription(gcs_UI_DescriptionStd_Weapon, gci_UI_AdItem_Max_Description_Lines)
            
            --Armors:
            elseif(zItem.iUsability == gciArmorCode) then
                AdItem_SetProperty("Type", "Armor (Nina)")
                AdItem_SetProperty("Add Equippable Slot", "Body")
                fnStandardizeDescription(gcs_UI_DescriptionStd_Armor, gci_UI_AdItem_Max_Description_Lines)
            
            --Accessories:
            elseif(zItem.iUsability == gciAccessoryCode) then
                AdItem_SetProperty("Type", "Accessory (Nina)")
                AdItem_SetProperty("Add Equippable Slot", "Accessory A")
                AdItem_SetProperty("Add Equippable Slot", "Accessory B")
                fnStandardizeDescription(gcs_UI_DescriptionStd_Accessory, gci_UI_AdItem_Max_Description_Lines)
            
            --Combat Items:
            elseif(zItem.iUsability == gciItemCode) then
                AdItem_SetProperty("Type", "Combat Item (Nina)")
                AdItem_SetProperty("Add Equippable Slot", "Item A")
                AdItem_SetProperty("Add Equippable Slot", "Item B")
                AdItem_SetProperty("Ability Path", zItem.sAbilityPath)
                fnStandardizeDescription(gcs_UI_DescriptionStd_CombatItem, gci_UI_AdItem_Max_Description_Lines)
            
            end
        DL_PopActiveObject()

        --Finish up.
        gbFoundItem = true
        return
    end
end

-- |[ ====================================== Unique Items ====================================== ]|
--Items that do not fit on the chart are created here. These are items that deviate from the normal
-- creation pattern for some reason.

--No items yet.
