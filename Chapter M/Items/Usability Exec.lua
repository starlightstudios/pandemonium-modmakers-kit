-- |[ ================================== Usability Execution =================================== ]|
--When a base-game item is created, this script will be called with the name of the item. This script
-- can then add usability cases for characters created in the mod.
--The item's name is the global gsItemName. There are no arguments.

-- |[Mod Activity Check]|
--If the mod is not active, don't do any of the below.
if(OM_IsModActive("Nina's Adventure") == false) then return end

-- |[Nina Items]|
--List of items that Nina can use from the base game.
local saNinaItemList = {"Healing Tincture", "Swamp Boots"}

--Scan the list for a match.
for i = 1, #saNinaItemList, 1 do
    if(saNinaItemList[i] == gsItemName) then
        AdItem_SetProperty("Add Equippable Character", "Nina")
        break
    end
end
