-- |[ ====================================== Item Listing ====================================== ]|
--Same as the base game's item listing, but uses the local item files. Called after the base game
-- tries (and fails) to find an item.
--Note that this file does not accept arguments. It assumes that the globals are already set.
--If the item is found, gbFoundItem should be set to true.

-- |[ =================================== Subscript Iteration ================================== ]|
--Global flag, set to false. Stop execution as soon as one of the subscripts sets it to true.
local sBasePath = fnResolvePath()

--Call this function if the item is being placed in the inventory.
if(AdInv_GetProperty("Is Registering To Shop") == false) then
    fnCheckItemAchievements(gsItemName)
end
        
-- |[Call Subscripts]|
--Variables.
local saFileList = {}

--List of subscripts.
table.insert(saFileList, "Nina Items")

--Iterate across the file list.
--io.write("Scanning file list for " .. gsItemName .. "\n")
for i = 1, #saFileList, 1 do
    --io.write(" Checking " .. i .. "\n")
	LM_ExecuteScript(sBasePath .. saFileList[i] .. ".lua")
	
	--End execution. Print debug if needed.
	if(gbFoundItem == true) then
        gbStopModExec = true
        gbModHandledCall = true
		return
	end
end

-- |[Error Case]|
--Item was not found. The base script handles errors.
