-- |[ ====================================== Nina's Hello ====================================== ]|
--Entry point for dialogue with this NPC. The script should be run with the NPC atop the Activity Stack.
--Note: Nina talks to herself as a sample, because there's no other party members. Deal with it.

-- |[Music]|
--Change music to the character's theme, if they have one.
glLevelMusic = AL_GetProperty("Music")
--AL_SetProperty("Music", "IzunasTheme")
    
-- |[Dialogue]|
fnCutsceneInstruction([[ WD_SetProperty("Show") ]])
fnCutsceneInstruction([[ WD_SetProperty("Major Sequence", true) ]])
fnCutsceneInstruction([[ WD_SetProperty("Actor In Slot", 1, "Nina", "Neutral") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] (I do so love talking to myself as a code sample.)") ]])
