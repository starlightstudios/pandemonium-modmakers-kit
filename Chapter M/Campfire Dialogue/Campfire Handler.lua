-- |[ ==================================== Campfire Handler ==================================== ]|
-- |[Mod Activity Check]|
--If the mod is not active, don't do any of the below.
if(OM_IsModActive("Nina's Adventure") == false) then return end

--Run.
local zaDim = {0.0, 0.0, 0.0, 0.0, 180.0, 180.0}

--Add.
if(gsPartyLeaderName == "Nina") then
    AM_SetProperty("Chat Member", "Nina", gsNinaRoot .. "Campfire Dialogue/Nina/Campfire.lua", zaDim[1], zaDim[2], zaDim[3], zaDim[4], zaDim[5] + zaDim[3], zaDim[6] + zaDim[4], "Root/Images/AdventureUI/ChatIcons/NinaHuman")
end

