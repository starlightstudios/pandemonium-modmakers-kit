-- |[ ====================================== Build Topics ====================================== ]|
--When the chapter boots, builds topics for characters who can reply to them. Each character gets a folder.
local sBasePath = fnResolvePath()
local saList = {"Countess"}

-- |[Execute]|
--Now set as necessary.
for i = 1, #saList, 1 do
	LM_ExecuteScript(sBasePath .. saList[i] .. "/000 Build Listing.lua")
end
