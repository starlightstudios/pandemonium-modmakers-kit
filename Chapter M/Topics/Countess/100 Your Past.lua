-- |[ ================================== Countess: Your Past =================================== ]|
--Asking the Countess about her past.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Countess")

-- |[Dialogue]|
--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Master, I want to know why you are here.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I was wondering that myself, actually.[SOFTBLOCK] I should have followed father's advice and married some lunkheaded nobleman.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Ride him for a while, produce an heir or two.[SOFTBLOCK] Take up breeding racehorses or some other distraction.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] But then, this is terribly exciting, isn't it?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I suppose I haven't answered your question, have I?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I'm the third daughter of the Sciawin family of Jeffespeir.[SOFTBLOCK] My father is on the city council.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Unless something happens to my older siblings, I'm not getting the choice spot.[SOFTBLOCK] But my uncle died recently, the old bastard.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I was going through his papers and found a deed to this estate.[SOFTBLOCK] Apparently our family had just forgotten about it eighty years ago.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] The rest of the land was swallowed by the Stabkovich family, not that they use it for anything but firewood.[SOFTBLOCK] But we technically still own the estate.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] So I've decided to refurbish the place and make it my own.[SOFTBLOCK] And no sooner do I arrive than the problems start piling up...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Does that satisfy you, mannequin?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[SOFTBLOCK] I am satisfied.[BLOCK][CLEAR]") ]])
    
    --Topic unlock.
    WD_SetProperty("Unlock Topic", "Your Uncle", 1)
    
end
