-- |[ ================================== Countess: Your Uncle ================================== ]|
--Asking the Countess about her uncle.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Countess")

-- |[Dialogue]|
--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Master, please tell me about your uncle.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] You mean the one whose tragic passing led me to this place?[SOFTBLOCK] I didn't know him too well.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Mother said he spent more time with us when we were little.[SOFTBLOCK] I don't remember.[SOFTBLOCK] I would have been three I think.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I've heard he was a prejudiced taffer, and that's a direct quote.[SOFTBLOCK] Really hated demons, called them filth.[SOFTBLOCK] He had a strange way of showing it though.[SOFTBLOCK] You know what he told my father once?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] 'We can't drown them all, so they may as well be allowed to own a business'.[SOFTBLOCK] I suppose it's practical but it boggles the mind, doesn't it?[SOFTBLOCK] He'd have been the first to hold a pogram and blame some random imp for a disease or bad harvest.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I believe he was the second son of the family, so he would have taken our council seat if father had died or abdicated.[SOFTBLOCK] Never had any children of his own, though, so my siblings would still be the heirs.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Can you believe no woman would want to marry such a blowhard?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I can.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] We agree, and I didn't even need to order you to![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[SOFTBLOCK] Master...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Yes?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Nothing.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Mannequin, I order you to say what you were about to say.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[SOFTBLOCK] I was going to ask you if you intend to marry.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Well yes, producing heirs is what we nobles are supposed to do.[SOFTBLOCK] It's also how we pass down wealth and end political squabbles.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Marrying for love is what the common people do.[SOFTBLOCK] If we don't get married, it's either because there's a political reason, or because our family has nothing to offer another.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Would you like to marry for love, master?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] If I ever felt love, maybe I'd change my mind.[SOFTBLOCK] But I have not.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ... Not yet?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Not yet, of course.[SOFTBLOCK] There's always time.[BLOCK][CLEAR]") ]])
    
    --Topic unlock.
    WD_SetProperty("Unlock Topic", "Mannequin Love", 1)
end
