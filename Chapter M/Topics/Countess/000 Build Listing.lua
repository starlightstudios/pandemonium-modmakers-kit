-- |[ ===================================== Build Listing ====================================== ]|
--Builds a list of topics this NPC will respond to, and at what levels.
local sNPCName = "Countess"

--Constants
local gciAvailableAtStart = 1
local gciMustBeUnlocked = -1

--Topic listing.
fnConstructTopic("Your Past",      "Your Past",      gciAvailableAtStart, sNPCName, 0)
fnConstructTopic("Mannequin Past", "Mannequin Past", gciAvailableAtStart, sNPCName, 0)
fnConstructTopic("Your Uncle",     "Your Uncle",     gciMustBeUnlocked,   sNPCName, 0)
fnConstructTopic("Mannequin Love", "Mannequin Love", gciMustBeUnlocked,   sNPCName, 0)
