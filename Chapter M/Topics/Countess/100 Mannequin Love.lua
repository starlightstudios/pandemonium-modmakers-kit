-- |[ ================================ Countess: Mannequin Love ================================ ]|
--Countess asks Nina about her lovelife.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Countess")

-- |[Dialogue]|
--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
    
    --Flags.
    VM_SetVar("Root/Variables/NinaMod/MainQuest/iToldOfLove", "N", 1.0)
	
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Mannequin, have you ever loved someone?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] A man, a woman?[SOFTBLOCK] Who?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] A woman.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Hm, maybe it's best not to pry.[SOFTBLOCK] I'm starting to like you, despite you being effectively my slave.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] What if I become attached, only to have to send you to your death?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I would do it for you.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Ugh.[SOFTBLOCK] No, I will take your body, you belongings, you will.[SOFTBLOCK] But I will not take your dignity.[SOFTBLOCK] You may keep your past life's secrets.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] If I could return you to normal, mannequin, know that I would.[SOFTBLOCK] But the transformation is permanent.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] It was a trap meant to ensnare criminals![SOFTBLOCK] I didn't think the gas would be this potent![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] A perfectly kind, innocent person.[SOFTBLOCK] Loyal, strong, pretty.[SOFTBLOCK] She has a family, and maybe even lovers, and I turned her into my slave![BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] ...[SOFTBLOCK] Mannequin...[SOFTBLOCK] Just...[SOFTBLOCK] I'm sorry.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    
end
