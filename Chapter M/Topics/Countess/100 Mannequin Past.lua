-- |[ ================================ Countess: Mannequin Past ================================ ]|
--Countess asks Nina about her past.
if(not fnArgCheck(1)) then return end

--Arg resolve.
local sTopicName = LM_GetScriptArgument(0)

--Always flag to go back to the topics listing when this is over.
WD_SetProperty("Activate Topics After Dialogue", "Countess")

-- |[Dialogue]|
--Initial response is always "Hello".
if(sTopicName == "Hello") then
	
	--Common.
	WD_SetProperty("Major Sequence Fast", true)
	
    --Dialogue.
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Mannequin, who were you before we met?[SOFTBLOCK] In a way, I suppose who are you now?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I am Nina Avarro.[SOFTBLOCK] I was born in Tserro.[SOFTBLOCK] My parents were poor, and I received no education.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I suppose in a place like that, public education would be in a sorry state.[SOFTBLOCK] Was it part of the Sturnheim orbit back then?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] No.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Did your life change when it was conquered?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] No.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I see.[SOFTBLOCK] So what did you do?[SOFTBLOCK] What was your job?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I hunted wild animals for food, and sometimes joined in bounty hunts if the soldiers posted one.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I would sell the excess kills for money, and strip hides and bones for what little I could.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] And you've been doing that this whole time?[SOFTBLOCK] How old are you?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes.[SOFTBLOCK] I am 45 years old, master.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] 45![SOFTBLOCK] Old enough to be my mother![SOFTBLOCK] I suppose elves don't show it.[SOFTBLOCK] Not that you have a face anymore.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Then how did you come to this place?[SOFTBLOCK] Tserro is at least four months ride if you can afford a horse.[SOFTBLOCK] You likely couldn't.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] ...[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Master...[SOFTBLOCK] where am I?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] My estate.[SOFTBLOCK] We are five days by boat south down the coast from Jeffespeir.[SOFTBLOCK] After that, about two days on foot through the woods east.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] You didn't know?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] It should not have been possible to cross that distance on foot in two days of hunt.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] You say you were out hunting for two days, and you're telling me that you came from Tserro to here?[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Impossible.[SOFTBLOCK] And yet you are not lying.[SOFTBLOCK] I don't think you can.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] I am not lying, master.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] ...[SOFTBLOCK] But how?[SOFTBLOCK] I suppose I won't get any answers from you.[SOFTBLOCK] I'll have to look into this.[BLOCK][CLEAR]") ]])
    fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] Perhaps that magical education will actually be worth more than soggy bread after all...[BLOCK][CLEAR]") ]])
    
end
