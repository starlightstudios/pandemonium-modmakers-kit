-- |[ ==================================== Countess: Goodbye =================================== ]|
--Script that fires when the player says goodbye to end the dialogue. Not all NPCs actually do anything here! This
-- script's existence is also optional.
WD_SetProperty("Major Sequence Fast", true)

--Standard.
fnCutsceneInstruction([[ WD_SetProperty("Append", "Countess:[E|Neutral] I'd say that's enough for the moment.[SOFTBLOCK] Mannequin, see to your tasks.[BLOCK][CLEAR]") ]])
fnCutsceneInstruction([[ WD_SetProperty("Append", "Nina:[E|Neutral] Yes, master.[SOFTBLOCK] Your will be done.") ]])
