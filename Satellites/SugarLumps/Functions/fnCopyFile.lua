-- |[ ======================================= fnCopyFile ======================================= ]|
--Copies a file with the given filename into the datafiles directory of your mod. Deletes the old
-- file implicitly.
function fnCopyFile(psFileName)
    
    --Argument check.
    if(psFileName == nil) then return end
    
    --Pathnames.
    local sStartPath = "Output/" .. psFileName .. ".slf"
    local sFinalPath = "../../Chapter M/Datafiles/" .. psFileName .. ".slf"
    
    --Verify the file exists by attempting to open it.
    local fHandle = io.open(sStartPath, "r")
    if(fHandle == nil) then return end
        
    --Close the file.
    io.close(fHandle)
    
    --Delete the old one and move the new one.
    os.remove(sFinalPath)
    os.rename(sStartPath, sFinalPath)
end
