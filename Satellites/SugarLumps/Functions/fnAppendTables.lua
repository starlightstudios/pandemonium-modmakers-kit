-- |[ ===================================== fnAppendTables ===================================== ]|
--Given two tables, appends the contents of saTable2 onto the end of saTable1. Returns the modified saTable1.
function fnAppendTables(saTable1, saTable2)
    
    --Argument check.
    if(saTable1 == nil) then return nil end
    if(saTable2 == nil) then return saTable1 end
    
    --Copy.
	local iArrayLen2 = #saTable2
	for i = 1, iArrayLen2, 1 do
		table.insert(saTable1, saTable2[i])
	end

    --Finish.
	return saTable1
end
