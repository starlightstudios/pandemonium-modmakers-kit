-- |[ ====================================== fnResolvePath ===================================== ]|
--Resolves and returns the directory of the currently executing script.
fnResolvePath = function()
	local sStartPath = debug.getinfo(2, "S").source
	local iLen = string.len(sStartPath)

	--Determine where the last slash is
	local sLetter = ""
	for i = iLen, 1, -1 do
		sLetter = string.sub(sStartPath, i, i)
		if(sLetter == "/" or sLetter == "\\") then
			iLen = i
			break
		end
	end

	local sFinishPath = ""
	for i = 2, iLen, 1 do

		sLetter = string.sub(sStartPath, i, i)
		sFinishPath = sFinishPath .. sLetter
	end

	return sFinishPath
end