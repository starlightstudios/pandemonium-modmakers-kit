-- |[ ======================================== fnExecAll ======================================= ]|
--Given a path and pattern, goes into all folders within a given directory. Executes the file in 
-- sFilePattern in that folder, if it exists.
--This is used to execute, for example, all ZRouting.lua files in a set of directories.
function fnExecAll(sCurrentPath, sFilePattern)

	--Scan
	--io.write("Opening filesystem...")
	FS_Open(sCurrentPath, false)
	--io.write("done!\n")

	--For each entry, appends the sFilePattern and try to run it.
	-- Ignore non-directories.  Directories end in '/'.
	sPath = FS_Iterate()
	while (sPath ~= "NULL") do

		--Check the last byte for '/'
		local iLength = string.len(sPath)
		if(string.byte(sPath, iLength) == string.byte("/")) then

			--Append the sFilePattern on the end and call it
			--io.write("Exec " .. sPath .. sFilePattern .. "\n")
			LM_ExecuteScript(sPath .. sFilePattern)

		end

		sPath = FS_Iterate()

	end

	--Clean
	--io.write("Closing filesystem...")
	FS_Close()
	--io.write("done!\n")

end
