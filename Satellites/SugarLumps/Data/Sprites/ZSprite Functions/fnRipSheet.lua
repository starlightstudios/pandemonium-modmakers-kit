-- |[ ======================================= fnRipSheet ======================================= ]|
--Rips a standardized sprite sheet format, ripping four or eight directions of movement and two optional
-- common special frames. This handles most of the sprites in the game.
function fnRipSheet(sBaseName, sImagePath, bIsEightDir, bIsWide, bHasRunAnim, bHasCrouch, bHasWound)
	
	-- |[Arg Check]|
	if(sBaseName == nil) then return end
	if(sImagePath == nil) then return end
	if(bIsEightDir == nil) then return end
	if(bIsWide == nil) then return end
	if(bHasRunAnim == nil) then return end
	if(bHasCrouch == nil) then bHasCrouch = false end
	if(bHasWound == nil) then bHasWound = false end
	
	-- |[Constants]|
	local cfStartX = 0
	local cfStartY = 40
	local cfSizeX = 32
	local cfSizeY = 40
	local saSets =   {"South", "North", "West", "East"}
	local iaFrames = {      4,       4,      4,      4}
	if(bIsEightDir) then
		saSets =   {"South", "North", "West", "East", "NE", "NW", "SW", "SE"}
		iaFrames = {      4,       4,      4,      4,    4,    4,    4,    4}
	end
	
	-- |[Wide Case]|
	if(bIsWide ~= nil and bIsWide == true) then
		cfStartX = 0
		cfStartY = 0
		cfSizeX = 50
		cfSizeY = 59
	end
	
	-- |[Walking Animation]|
	local i = 1
	while(saSets[i] ~= nil) do
		
		--Iterate.
		for p = 1, iaFrames[i], 1 do
			
			--Name.
			local sUseName = sBaseName .. "|" .. saSets[i] .. "|" .. (p-1)
			
			--Position.
			local fRipX = cfStartX + (cfSizeX * (p-1))
			local fRipY = cfStartY + (cfSizeY * (i-1))
			
			--Rip.
			ImageLump_Rip(sUseName, sImagePath, fRipX + gfOffsetX, fRipY + gfOffsetY, cfSizeX, cfSizeY, 0)
			
		end
	
		--Next.
		i = i + 1
	end
	
    -- |[Running Animation]|
	--Only used on some sprites, mostly party members.
	if(bHasRunAnim ~= nil and bHasRunAnim == true) then
		
		--Iterate again.
		i = 1
		while(saSets[i] ~= nil) do
			
			--Iterate.
			for p = 1, iaFrames[i], 1 do
				
				--Name.
				local sUseName = sBaseName .. "|" .. saSets[i] .. "|Run" .. (p-1)
				
				--Position.
				local fRipX = cfStartX + (cfSizeX * (p-1)) + (cfSizeX * 4)
				local fRipY = cfStartY + (cfSizeY * (i-1))
				
				--Rip.
				ImageLump_Rip(sUseName, sImagePath, fRipX + gfOffsetX, fRipY + gfOffsetY, cfSizeX, cfSizeY, 0)
				
			end
		
			--Next.
			i = i + 1
		end
	end
    
    -- |[Crouch]|
    --Very common special frame. Optional.
    if(bHasCrouch == true) then
        
        --Position
        local fX = cfSizeX * 1.0
        local fY = cfSizeY * 0.0
        
        --Rip.
        local sUseName = "Spcl|" .. sBaseName .. "|Crouch"
        ImageLump_Rip(sUseName, sImagePath, fX, fY, cfSizeX, cfSizeY, 0)
        
    end
    
    -- |[Wounded]|
    --Very common special frame. Optional. Enemies need this.
    if(bHasWound == true) then
        
        --Position
        local fX = cfSizeX * 2.0
        local fY = cfSizeY * 0.0
        
        --Rip.
        local sUseName = "Spcl|" .. sBaseName .. "|Wounded"
        ImageLump_Rip(sUseName, sImagePath, fX, fY, cfSizeX, cfSizeY, 0)
        
    end
end