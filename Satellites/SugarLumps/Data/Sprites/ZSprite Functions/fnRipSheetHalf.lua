-- |[ ===================================== fnRipSheetHalf ===================================== ]|
--Special form of fnRipSheet(). Only rips the west/east frames. Used for animals.
function fnRipSheetHalf(sBaseName, sImagePath)
	
	-- |[Arg Check]|
	if(sBaseName == nil) then return end
	if(sImagePath == nil) then return end
	
	-- |[Constants]|
	local cfStartX = 0
	local cfStartY = 40
	local cfSizeX = 32
	local cfSizeY = 40
	local saSets =   {"West", "East"}
	local iaFrames = {     4,      4}
	
	-- |[Rip]|
	local i = 1
	while(saSets[i] ~= nil) do
		
		--Iterate.
		for p = 1, iaFrames[i], 1 do
			
			--Name.
			local sUseName = sBaseName .. "|" .. saSets[i] .. "|" .. (p-1)
			
			--Position.
			local fRipX = cfStartX + (cfSizeX * (p-1))
			local fRipY = cfStartY + (cfSizeY * (i-1))
			
			--Rip.
			ImageLump_Rip(sUseName, sImagePath, fRipX + gfOffsetX, fRipY + gfOffsetY, cfSizeX, cfSizeY, 0)
			
		end
	
		--Next.
		i = i + 1
	end
end