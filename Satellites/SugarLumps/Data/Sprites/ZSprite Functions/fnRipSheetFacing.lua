-- |[ ==================================== fnRipSheetFacing ==================================== ]|
--Rips a sheet, but not the walking/running animations, just the four facing directions. This is used
-- for NPCs who never move, in order to conserve memory/artist effort.
function fnRipSheetFacing(sBaseName, sImagePath, iXStart, iYStart)
	
	-- |[Arg Check]|
	if(sBaseName  == nil) then return end
	if(sImagePath == nil) then return end
	if(iXStart    == nil) then return end
	
	-- |[Constants]|
	local cfStartX = iXStart
	local cfStartY = iYStart
	local cfSizeX = 32
	local cfSizeY = 40
	local saSets =   {"South", "North", "West", "East"}
	local iaFrames = {      1,       1,      1,      1}
	
	-- |[Rip]|
	local i = 1
	while(saSets[i] ~= nil) do
		
		--Iterate.
		for p = 1, iaFrames[i], 1 do
			
			--Name.
			local sUseName = sBaseName .. "|" .. saSets[i] .. "|" .. (p-1)
			
			--Position.
			local fRipX = cfStartX + (cfSizeX * (p-1))
			local fRipY = cfStartY + (cfSizeY * (i-1))
			
			--Rip.
			ImageLump_Rip(sUseName, sImagePath, fRipX + gfOffsetX, fRipY + gfOffsetY, cfSizeX, cfSizeY, 0)
			
		end
	
		--Next.
		i = i + 1
	end
end