-- |[ ========================================= Sprites ======================================== ]|
--Routing file for spritesheets.
local sBasePath = fnResolvePath()

--Setup
SLF_Open("Output/Sprites.slf")
ImageLump_SetCompression(1)

-- |[ ======================================= Functions ======================================== ]|
--Offset variables used by the ripping functions. Used if you want to have multiple sheets or otherwise
-- need to do something unique with the image.
gfOffsetX = 0
gfOffsetY = 0

--Build temporary functions used exclusively by sprites.
LM_ExecuteScript(sBasePath .. "ZSprite Functions/fnRipSheet.lua")
LM_ExecuteScript(sBasePath .. "ZSprite Functions/fnRipSheetFacing.lua")
LM_ExecuteScript(sBasePath .. "ZSprite Functions/fnRipSheetHalf.lua")

-- |[ ===================================== Subdirectories ===================================== ]|
--If a subdirectory has more sprites, call it here.

-- |[ ========================================== Nina ========================================== ]|
--Protagonist of the starter kit.
fnRipSheet("Nina_Human",     sBasePath .. "Nina/Nina_Human.png",     true, false, true, true, true)
fnRipSheet("Nina_Zombie",    sBasePath .. "Nina/Nina_Zombie.png",    true, false, true, true, true)
fnRipSheet("Nina_Mannequin", sBasePath .. "Nina/Nina_Mannequin.png", true, false, true, true, true)

-- |[ ================================= 4-Direction Characters ================================= ]|
fnRipSheet("Countess", sBasePath .. "Countess/Countess.png", false, false, false, false, true)

-- |[ ======================================== Finish Up ======================================= ]|
--Finish
SLF_Close()

--Clean up global functions/variables.
gfOffsetX = nil
gfOffsetY = nil
fnRipSheet = nil
fnRipSheetHalf = nil
fnRipSheetFacing = nil
