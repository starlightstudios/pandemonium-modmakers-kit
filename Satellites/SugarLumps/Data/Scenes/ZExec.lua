-- |[ ==================================== Scene Compression =================================== ]|
--Executes scene compression.
Debug_PushPrint(false, "Beginning scene compression.\n")

--Variables.
local sCurrentPath = fnResolvePath()
local baCompressionFlags = {}

-- |[ ================================= List Build and Execute ================================= ]|
-- |[List Construction]|
--Build an ordered list.
local saEntryList = {}
--table.insert(saEntryList, {"Nina", "ZRouting.lua"})

-- |[Execution]|
--Run across all folders, execute.
for i = 1, #saEntryList, 1 do
    fnCompressFolder(sCurrentPath, saEntryList[i][1], saEntryList[i][2])
end

--Done
Debug_PopPrint("Finished portrait compression activities.\n")