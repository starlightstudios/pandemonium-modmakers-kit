-- |[ ============================== Image Compression Execution =============================== ]|
--Execs image compression for your project.
Debug_PushPrint(false, "Beginning image compression\n")

--Debug flags.
local sCurrentPath = fnResolvePath()

-- |[Functions / Constants]|
--Causes images to compress at 1/4th size. Greatly reduces file size and image quality.
gbUseLowDefinition = false

--Disables/Enables trimming of the edges of an image. If false, transparent edge pixels are purged.
-- ImageLump_SetBlockTrimmingFlag(true)

--Disables/Enables compressing images by lines. Each line is scanned to remove the transparent edges.
-- This takes extra time but can reduce file sizes. If a line-compressed image is larger than an
-- uncompressed image (the case for no transparencies) the uncompressed data is used.
ImageLump_SetAllowLineCompression(true)

--Global flag. Put this as a flag during compression to ignore the three transparent colors.
gciNoTransparencies = 16

-- |[ ==================================== Folder Execution ==================================== ]|
-- |[Execution]|
--Storage. Stores which folders got compressed. This information is useful for building low-def
-- versions of files if necessary.
local baCompressionFlags = {}

--Auto-exec the folders.
table.insert(baCompressionFlags, fnCompressFolder(sCurrentPath, "Scenes",  "ZRouting"))
table.insert(baCompressionFlags, fnCompressFolder(sCurrentPath, "Sprites", "ZRouting"))
table.insert(baCompressionFlags, fnCompressFolder(sCurrentPath, "UI",      "ZRouting"))

-- |[ ======================================= Subfolders ======================================= ]|
--If a folder does not have a single .slf file produced as its output, this runs to do cause those
-- folders to run their compressors as well.
LM_ExecuteScript(sCurrentPath .. "Portraits/ZExec.lua")

-- |[ ======================================== Finish Up ======================================= ]|
--Debug.
Debug_PopPrint("Finished image compression activities!\n")