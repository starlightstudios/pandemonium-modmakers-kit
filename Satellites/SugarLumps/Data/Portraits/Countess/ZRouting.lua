-- |[ =================================== Countess Portraits =================================== ]|
--Used for the modmaker's starter pack.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Countess.slf")
else
	SLF_Open("Output/PortraitsLD_Countess.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ======================================== Emotes ========================================== ]|
-- |[Single Emote]|
ImageLump_Rip("Por|Countess|Neutral", sBasePath .. "Countess_Emote.png", gciHDX0, gciHDY0, gciWid0, gciHei0, gciNoTransparencies)

-- |[ ======================================== Combat ========================================== ]|
--No combat portraits.

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
