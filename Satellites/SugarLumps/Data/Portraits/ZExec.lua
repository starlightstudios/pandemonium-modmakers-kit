-- |[ ================================= Portraits Compression ================================== ]|
--Executes portrait compression.
Debug_PushPrint(false, "Beginning portrait compression.\n")

--Variables.
local sCurrentPath = fnResolvePath()
local baCompressionFlags = {}

-- |[ ==================================== Sizing Constants ==================================== ]|
-- |[Normal Emote Grid]|
--Size constants used for the "normal" emote grids. Most characters use these.
gciHDX0 =    3
gciHDX1 =  703
gciHDX2 = 1403
gciHDX3 = 2103

gciHDY0 =    3
gciHDY1 =  771
gciHDY2 = 1539
gciHDY3 = 2307
gciHDY4 = 3075

gciWid0 = 694
gciWid1 = 694
gciWid2 = 694
gciWid3 = 694

gciHei0 = 762
gciHei1 = 762
gciHei2 = 762
gciHei3 = 762

-- |[Wide Emote Grid]|
--Size constants used for "wide" emote grids. These are usually enemy portraits.
gciWideX0 =    3
gciWideX1 = 1401
gciWideX2 = 2799
gciWideX3 = 4197

gciWideY0 =    3
gciWideY1 =  827
gciWideY2 = 1651

gciWideWid = 1392
gciWideHei =  818

-- |[ ================================= List Build and Execute ================================= ]|
-- |[List Construction]|
--Build an ordered list.
local saEntryList = {}
table.insert(saEntryList, {"Countess", "ZRouting"})
table.insert(saEntryList, {"Nina",     "ZRouting"})
table.insert(saEntryList, {"Enemies",  "ZRouting"})

-- |[Execution]|
--Run across all folders, execute.
for i = 1, #saEntryList, 1 do
    fnCompressFolder(sCurrentPath, saEntryList[i][1], saEntryList[i][2])
end

--Done
Debug_PopPrint("Finished portrait compression activities.\n")
