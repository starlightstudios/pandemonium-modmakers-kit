-- |[ ===================================== Nina Portraits ===================================== ]|
--Used for the modmaker's starter pack.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Nina.slf")
else
	SLF_Open("Output/PortraitsLD_Nina.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ======================================== Emotes ========================================== ]|
-- |[Automated Ripper Function]|
--Ripper designed for Christine's emote pattern.
local fnAutoRip = function(sBaseName, sFormName, sPath)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Neutral", sPath, gciHDX0, gciHDY0, gciWid0, gciHei0, gciNoTransparencies)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Smirk",   sPath, gciHDX0, gciHDY1, gciWid0, gciHei0, gciNoTransparencies)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Happy",   sPath, gciHDX1, gciHDY1, gciWid0, gciHei0, gciNoTransparencies)
    ImageLump_Rip("Por|" .. sBaseName .. sFormName .. "|Upset",   sPath, gciHDX2, gciHDY1, gciWid0, gciHei0, gciNoTransparencies)
end

-- |[General Purpose]|
fnAutoRip("Nina", "Human",  sBasePath .. "Nina_Human_Emote.png")
fnAutoRip("Nina", "Zombie", sBasePath .. "Nina_Zombie_Emote.png")

-- |[Single Emote]|
--Mannequin Nina only has the neutral emote.
ImageLump_Rip("Por|NinaMannequin|Neutral", sBasePath .. "Nina_Mannequin_Emotes.png", gciHDX0, gciHDY0, gciWid0, gciHei0, gciNoTransparencies)

-- |[ ======================================== Combat ========================================== ]|
--This is just a simple list of combat frames.
ImageLump_Rip("Party|Nina_Human",     sBasePath .. "Nina_Human_Combat.png",     0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Party|Nina_Zombie",    sBasePath .. "Nina_Zombie_Combat.png",    0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Party|Nina_Mannequin", sBasePath .. "Nina_Mannequin_Combat.png", 0, 0, -1, -1, gciNoTransparencies)

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
