-- |[ ==================================== Enemy Portraits ===================================== ]|
--Used for the modmaker's starter pack.
local sBasePath = fnResolvePath()

-- |[File Creation]|
--Creates portraits in 1/4 size for older machines, if true.
if(gbUseLowDefinition == false) then
	SLF_Open("Output/Portraits_Enemies.slf")
else
	SLF_Open("Output/PortraitsLD_Enemies.slf")
	ImageLump_SetScales(0.25, 0.25, 0, 0)
end

-- |[ ======================================== Emotes ========================================== ]|
-- |[Single Emote]|
--Enemies have single emotes each.
ImageLump_Rip("Por|Brigand_Leader|Neutral",  sBasePath .. "Enemy Emotes.png", gciHDX0, gciHDY0, gciWid0, gciHei0, gciNoTransparencies)
ImageLump_Rip("Por|Brigand_Thief|Neutral",   sBasePath .. "Enemy Emotes.png", gciHDX1, gciHDY0, gciWid0, gciHei0, gciNoTransparencies)
ImageLump_Rip("Por|Brigand_Warrior|Neutral", sBasePath .. "Enemy Emotes.png", gciHDX2, gciHDY0, gciWid0, gciHei0, gciNoTransparencies)

-- |[ ======================================== Combat ========================================== ]|
--This is just a simple list of combat frames.
ImageLump_Rip("Enemy|Brigand_Leader",   sBasePath .. "Bandit_Leader_Combat.png",    0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Enemy|Brigand_Thief",    sBasePath .. "Bandit_Thief_Combat.png",     0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Enemy|Brigand_Warrior",  sBasePath .. "Bandit_Warrior_Combat.png",   0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Enemy|Skeleton_Archer",  sBasePath .. "Skeleton_Archer_Combat.png",  0, 0, -1, -1, gciNoTransparencies)
ImageLump_Rip("Enemy|Skeleton_Warrior", sBasePath .. "Skeleton_Warrior_Combat.png", 0, 0, -1, -1, gciNoTransparencies)

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
