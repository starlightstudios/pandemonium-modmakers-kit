-- |[ ======================================== UI Images ======================================= ]|
--These images appear in spots on the UI, either pause menu, campfire, or combat.
local sBasePath = fnResolvePath()

-- |[File Creation]|
SLF_Open("Output/UI.slf")

-- |[ ===================================== Turn Portraits ===================================== ]|
--Appears in combat to denote the order characters are acting.

-- |[Ripper Function]|
--Automated, creates images named "PorSml|Alraune" and such. Uses tables.
local fnRipImages = function(sPrefix, sRipPath, saRipNames, iXValue, iYValue, iWid, iHei, sCharPrefix)
	
	--Arg check
	if(sPrefix    == nil) then return end
	if(sRipPath   == nil) then return end
	if(saRipNames == nil) then return end
	if(iXValue    == nil) then return end
	if(iYValue    == nil) then return end
	
	--sCharPrefix can be nil. It's only used for ripping the main characters.
	
	--Let 'er rip!
	local i = 0
	while(saRipNames[i+1] ~= nil) do
		
		--If the name is "SKIP", then don't rip here. This is used because slots are empty until later in some cases.
		if(saRipNames[i+1] == "SKIP") then
		else
		
			--Not using a character prefix:
			if(sCharPrefix == nil) then
				ImageLump_Rip(sPrefix .. saRipNames[i+1], sRipPath, iXValue + (iWid * i), iYValue, iWid, iHei, 0)
			
			--Character prefix. Makes names like "PorSml|Christine_Alraune"
			else
				ImageLump_Rip(sPrefix .. sCharPrefix .. saRipNames[i+1], sRipPath, iXValue + (iWid * i), iYValue, iWid, iHei, 0)
			end
			
		end
		
		--Next.
		i = i + 1
	end
end

-- |[Character Ripping]|
--Constants
local ciWid = 84
local ciHei = 54
local sRipPath = sBasePath .. "TurnPortraitsSheet.png"

--Nina's row.
local saRipNames = {"Human", "Mannequin", "Zombie"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 0, ciWid, ciHei, "Nina_")

-- |[Enemy Ripping]|
sRipPath = sBasePath .. "TurnPortraitsSheetEnemies.png"

--All enemies.
saRipNames = {"Leader", "Thief", "Warrior", "SkelArch", "SkelWarr"}
fnRipImages("PorSml|", sRipPath, saRipNames, ciWid * 0, ciHei * 0, ciWid, ciHei)

-- |[ ========================================== Faces ========================================= ]|
--These appear next to equipment to indicate which character is using it. All the faces
-- are on a single sheet, and the engine uses a subdivision.
ImageLump_SetBlockTrimmingFlag(true)
ImageLump_Rip("CharacterFaces", sBasePath .. "CharacterFaces.png", 0, 0, -1, -1, 0)
ImageLump_SetBlockTrimmingFlag(false)

-- |[ ======================================= Item Icons ======================================= ]|
ImageLump_Rip("BowIcon", sBasePath .. "BowIcon.png", 0, 0, -1, -1, 0)

-- |[ ===================================== Campfire Icons ===================================== ]|
ImageLump_SetBlockTrimmingFlag(true)
ImageLump_Rip("Campfire|Nina", sBasePath .. "CampfireIco.png", 0, 0, 192, 192, 0)
ImageLump_SetBlockTrimmingFlag(false)

-- |[ ======================================= Finish Up ======================================== ]|
--Close and write the SLF file.
SLF_Close()
