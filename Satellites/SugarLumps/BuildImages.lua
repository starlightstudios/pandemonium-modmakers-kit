-- |[ ====================================== Build Images ====================================== ]|
--Lua file which is executed if no other files were specified on the command line.

-- |[Baseline]|
--Run function scripts.
LM_ExecuteScript("Functions/fnAppendTables.lua")
LM_ExecuteScript("Functions/fnCompressFolder.lua")
LM_ExecuteScript("Functions/fnCopyFile.lua")
LM_ExecuteScript("Functions/fnExecAll.lua")
LM_ExecuteScript("Functions/fnResolvePath.lua")

-- |[Execute]|
--Execute the primary ripping file.
LM_ExecuteScript("Data/ZExec.lua")

-- |[ ==================================== Datafile Copying ==================================== ]|
--Copy the datafiles from the Output/ folder into the Chapter's Datafiles/ folder.

-- |[High-Res List]|
--This list is just literals with no LD cases.
local saList = {"Portraits_Countess", "Portraits_Nina", "Portraits_Enemies", "Sprites", "UI"}
for i = 1, #saList, 1 do
    fnCopyFile(saList[i])
end
